//
//  ClassifyModel.h
//  ReaderProject
//
//  Created by 李星星 on 2019/9/23.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ClassifyModel : BaseModel
@property(nonatomic,strong)NSString *author;//"author": 黛玉,
@property(nonatomic,strong)NSString *authorId;//"authorId": 10,
@property(nonatomic,strong)NSString *cover;//"cover": "https://cdn.cnbj0.fds.api.mi-img.com/atlas/664fa44a4c5bb43b9cc5a796ca511c20?thumb=1&w=240&h=320",
@property(nonatomic,strong)NSString *created;//"created": 1497399662,
@property(nonatomic,strong)NSString *enabled;//"enabled": 1,
@property(nonatomic,strong)NSString *extras;//"extras": "",
@property(nonatomic,strong)NSString *IDString;//"id": 10,
@property(nonatomic,strong)NSString *label;//"label": "玄幻",
@property(nonatomic,strong)NSString *tag;//"tag": ["热血", "青春", "校园"]
@property(nonatomic,strong)NSString *name;//"name": "我的贴身校花",
@property(nonatomic,strong)NSString *state;//"state": 1,
@property(nonatomic,strong)NSString *summary;//"summary": "一个普通的高中生，跟随自称是世界顶级杀手的无耻老头子学功夫，无意间又得到一瓶丹药，让他身怀数种异能，从此开始了美妙的生活...",
@property(nonatomic,strong)NSString *updated;//"updated": 1568057536,
@property(nonatomic,strong)NSString *words;//"words": 23777730
@property(nonatomic,strong)NSString *countchapter;//小说总章节

@property(nonatomic,strong)NSString *isRecommendModel;//是否为本地书架
@property(nonatomic,strong)NSString *isread;//本地书架书本是否已读
@end

NS_ASSUME_NONNULL_END
