//
//  TypeController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "TypeController.h"
#import "SearchTableViewCell.h"
#import "InfoController.h"
#import "ClassifyModel.h"
#import <SVProgressHUD.h>
#import <MJRefresh.h>
#import <UMAnalytics/MobClick.h>
@interface TypeController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property(strong,nonatomic)NSMutableArray * dataSourceArrM;

@property(nonatomic,assign)NSInteger page;

@end

@implementation TypeController

-(NSMutableArray *)dataSourceArrM{
    if (!_dataSourceArrM) {
        _dataSourceArrM = [ [NSMutableArray alloc] init];
    }
    return _dataSourceArrM;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.tableFooterView = [UIView new];
    [self.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SearchTableViewCell class])];
    self.listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        if (self.isFind && self.currentMonthlyModel) {
            [self requestData:[self.currentMonthlyModel.IDString integerValue] TypeString:@"monthnov" Type1String:@"monthly"];
        }else{
            [self requestData:[self.currentChannelModel.IDString integerValue] TypeString:@"classify" Type1String:@"classify"];
        }
    }];
    self.listTableView.mj_footer  = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        if (self.isFind && self.currentMonthlyModel) {
            [self requestData:[self.currentMonthlyModel.IDString integerValue] TypeString:@"monthnov" Type1String:@"monthly"];
        }else{
            [self requestData:[self.currentChannelModel.IDString integerValue] TypeString:@"classify" Type1String:@"classify"];
        }
    }];
    
    if (self.isFind && self.currentMonthlyModel) {
        self.page  = 1;
        [self requestData:[self.currentMonthlyModel.IDString integerValue] TypeString:@"monthnov" Type1String:@"monthly"];
        self.title = self.currentMonthlyModel.monthly;
        
    }else{
        self.page  = 1;
        [self requestData:[self.currentChannelModel.IDString integerValue] TypeString:@"classify" Type1String:@"classify"];
        self.title = self.currentChannelModel.type;
    }
    [MobClick setAutoPageEnabled:YES];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)requestData:(NSInteger)typeId TypeString:(NSString *)typeString Type1String:(NSString *)type1String{
    [SVProgressHUD show];
    [SVProgressHUD dismissWithDelay:3.0];
    [HttpManager fuckGetHttpManagerType:typeString Type1:type1String IDString:typeId page:self.page limit:10 requestSucced:^(id  _Nonnull responseObject) {
        [SVProgressHUD dismiss];
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
        if (self.page == 1) {
            [self.dataSourceArrM removeAllObjects];
        }
        NSArray *resultArr = responseObject[@"result"];
        for (NSDictionary *dic in resultArr) {
            ClassifyModel *model = [[ClassifyModel alloc] initWithDict:dic];
            model.IDString = dic[@"id"];
            [self.dataSourceArrM addObject:model];
        }
        [self.listTableView reloadData];
    } requestfailure:^(NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
    }];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSourceArrM.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchTableViewCell class]) forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.currentClassifyModel = self.dataSourceArrM[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    InfoController *ctrl = [[InfoController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    ctrl.currentClassfiyModel = self.dataSourceArrM[indexPath.row];
    [self.navigationController pushViewController:ctrl animated:YES];
}



@end
