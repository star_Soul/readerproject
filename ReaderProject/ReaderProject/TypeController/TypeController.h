//
//  TypeController.h
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "BaseViewController.h"
#import "ChannelModel.h"
#import "MonthlyModel.h"
NS_ASSUME_NONNULL_BEGIN
typedef NS_ENUM(NSInteger, TypeOfFlower) {
    xianyan,//    bolon
    guyan,//    helenkeller
    chuanyue,//    bolon
    haomen,//    helenkeller
    huanyan,//    bolon
    xiaoyuan,//    helenkeller
    dushi,//    bolon
    xuanhuan,//    helenkeller
    xianxia,//    bolon
    lingyi,//    helenkeller
    kehuan,//    bolon
    wuxia,//    helenkeller
};
@interface TypeController : BaseViewController
@property(nonatomic,assign)TypeOfFlower currentTypeOfFlower;

@property(nonatomic,strong)ChannelModel *currentChannelModel;

@property(nonatomic,assign)BOOL isFind;
@property(nonatomic,strong)MonthlyModel *currentMonthlyModel;
@end

NS_ASSUME_NONNULL_END
