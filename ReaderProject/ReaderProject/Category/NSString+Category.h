//
//  NSString+Category.h
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
NS_ASSUME_NONNULL_BEGIN

@interface NSString (Category)
- (CGSize )countTextSizeWithTextFont:(CGFloat)Font width:(CGFloat)width;
- (NSMutableAttributedString *)moneyHandle;

- (NSMutableAttributedString *)mAttStringWithChangeString:(NSString *)aString Attribute:(NSDictionary *)Dict  changIsFront:(BOOL)isFront;
- (BOOL)isEmpty;
- (BOOL) isEmail;
-(BOOL) isIdentityCard;
- (BOOL) isMobilePhone;
- (BOOL)hasString:(NSString *)subString;
+ (BOOL)isBankCard:(NSString*) cardNo;
- (BOOL) isUserNameString;
- (BOOL)isZuoji;
+ (BOOL) validatePassword:(NSString *)passWord;




- (NSRange)rangeByTrimmingLeftCharactersInSet:(NSCharacterSet *)characterSet;
- (NSRange)rangeByTrimmingRightCharactersInSet:(NSCharacterSet *)characterSet;

- (NSString *)stringByTrimmingLeftCharactersInSet:(NSCharacterSet *)characterSet;
- (NSString *)stringByTrimmingRightCharactersInSet:(NSCharacterSet *)characterSet;
//阿拉伯数字转中文格式
+(NSString *)translation:(NSString *)arebic;
+(NSString *)dateStr2pinyin;
//转换拼音
- (NSString *)transformToPinyin;

//是否包含语音解析的图标
- (BOOL)hasListenChar;




- (CGSize)getSizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
- (CGFloat)getHeightWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
- (CGFloat)getWidthWithFont:(UIFont *)font constrainedToSize:(CGSize)size;
-(BOOL)containsEmoji;

- (NSString *)emotionMonkeyName;

+ (NSString *)sizeDisplayWithByte:(CGFloat)sizeOfByte;

- (NSString *)trimWhitespace;


- (NSString *)checkNullString;




/**
 *  去掉首尾空字符串
 */
- (NSString *)replaceSpaceOfHeadTail;
- (NSString *)replaceUnicode;

/**
 获取缓存路径
 
 @return 将当前字符串拼接到cache目录后面
 */
- (NSString *)cacheDic;

/**
 获取document路径
 
 @return 将当前字符串拼接到document目录后面
 */
- (NSString *)docDic;

/**
 获取tmp路径
 
 @return 将当前字符串拼接到tmp目录后面
 */
- (NSString *)tmpDic;

+(NSString *)filterDataToString:(id)aString;
@end

NS_ASSUME_NONNULL_END
