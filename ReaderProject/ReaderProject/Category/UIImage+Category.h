//
//  UIImage+Category.h
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
NS_ASSUME_NONNULL_BEGIN

@interface UIImage (Category)
+(UIImage *)grayImage:(UIImage *)sourceImage;
/**
 * 传入图片的名称,返回一张可拉伸不变形的图片
 *
 * @param imageName 图片名称
 *
 * @return 可拉伸图片
 */
+ (UIImage *)resizableImageWithName:(NSString *)imageName;
//图片不被渲染
+ (UIImage *)imageWithOriginalRender:(NSString *)imageName;
- (UIImage *) imageWithTintColor:(UIColor *)tintColor;
- (UIImage *) imageWithGradientTintColor:(UIColor *)tintColor;

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size;
+ (UIImage *)imageWithColor:(UIColor *)color;
//图片上绘制文字 写一个UIImage的category
- (UIImage *)imageWithTitle:(NSString *)title fontSize:(CGFloat)fontSize;

+ (UIImage *)fullResolutionImageFromALAsset:(PHAsset *)asset;
+ (UIImage *)fullScreenImageALAsset:(PHAsset *)asset;

- (void)compressAndsaveImageSaveName:(NSString *)name;
//图片截取-----
-(UIImage *)cropImage:(UIImage *)image rect:(CGRect)cropRect;
-(UIImage*)scaledToSize:(CGSize)targetSize;
-(UIImage*)scaledToSize:(CGSize)targetSize highQuality:(BOOL)highQuality;
-(UIImage*)scaledToMaxSize:(CGSize )size;
//渐变图片绘制
+ (UIImage *)imageWithStartColor:(UIColor *)color endColor:(UIColor *)color size:(CGSize)size;
+(UIImage*)shotCutViewByView:(UIView *)aView;
//图片大小
+(CGSize)getImageSizeWithURL:(id)imageURL;

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+ (UIImage *)imageNamedFromMyBundle:(NSString *)name;
@end

NS_ASSUME_NONNULL_END
