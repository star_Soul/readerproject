//
//  UserAgreePopView.h
//  ReaderProject
//
//  Created by 李星星 on 2019/9/27.
//  Copyright © 2019 dingye. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UserAgreePopView : UIView
@property (retain, nonatomic) IBOutlet UILabel *UserAgreeLabel;
@property (retain, nonatomic) IBOutlet UIButton *agreeButton;

@end

NS_ASSUME_NONNULL_END
