//
//  UserAgreePopView.m
//  ReaderProject
//
//  Created by 李星星 on 2019/9/27.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "UserAgreePopView.h"

@implementation UserAgreePopView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
    [_UserAgreeLabel release];
    [_agreeButton release];
    [super dealloc];
}
@end
