//
//  ChangePwdController.m
//  ReaderProject
//
//  Created by 李星星 on 2019/9/30.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "ChangePwdController.h"
#import <objc/runtime.h>
#import <SVProgressHUD.h>
#import <UMAnalytics/MobClick.h>
@interface ChangePwdController ()
@property (retain, nonatomic) IBOutlet UITextField *phoneTextField;
@property (retain, nonatomic) IBOutlet UITextField *codeTextField;
@property (retain, nonatomic) IBOutlet UITextField *passwordTextField;
@property (retain, nonatomic) IBOutlet UITextField *againPasswordTextField;
@property (retain, nonatomic) IBOutlet UIButton *codeButton;
@property (retain, nonatomic) IBOutlet UIButton *registerButton;
@property(nonatomic,strong)NSTimer *timer;
@property(nonatomic,assign)NSInteger count;
@end

@implementation ChangePwdController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.title = @"重置密码";
    self.registerButton.layer.cornerRadius = 20;
    self.registerButton.clipsToBounds = YES;

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    [self.view addGestureRecognizer:tap];
    self.phoneTextField.clearButtonMode = UITextFieldViewModeAlways;
    self.view.backgroundColor = [UIColor whiteColor];
    
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(self.phoneTextField, ivar);
    placeholderLabel.textColor = UIColorFromRGB(0x808080);
    self.phoneTextField.textColor = UIColorFromRGB(0x808080);

    UILabel *pwdplaceholderLabel = object_getIvar(self.passwordTextField, ivar);
    pwdplaceholderLabel.textColor = UIColorFromRGB(0x808080);
    self.passwordTextField.textColor = UIColorFromRGB(0x808080);
    
    
    UILabel *againpwdplaceholderLabel = object_getIvar(self.againPasswordTextField, ivar);
    againpwdplaceholderLabel.textColor = UIColorFromRGB(0x808080);
    self.againPasswordTextField.textColor = UIColorFromRGB(0x808080);
    
    UILabel *codeplaceholderLabel = object_getIvar(self.codeTextField, ivar);
    codeplaceholderLabel.textColor = UIColorFromRGB(0x808080);
    self.codeTextField.textColor = UIColorFromRGB(0x808080);
    [MobClick setAutoPageEnabled:YES];
}
-(void)tapClick{
    [self.view endEditing:YES];
}
- (IBAction)codeButtonClick:(UIButton *)sender {
    if (self.phoneTextField.text.length == 11) {
           [MBProgressHUD showError:@"验证码已发送"];
           [HttpManager fuckHttpManagerRequestUrl:@"http://www.ljsb.top:5000/common/getcode" dic:@{@"phone":self.phoneTextField.text} Succed:^(id  _Nonnull responseObject) {
               self.codeButton.userInteractionEnabled = NO;
               self.count = 0;
               self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerClick) userInfo:nil repeats:YES];
               [[NSRunLoop currentRunLoop] addTimer:self.timer
                                            forMode:NSRunLoopCommonModes];
           } requestfailure:^(NSError * _Nonnull error) {
               
           }];
           
       }else{
           [MBProgressHUD showError:@"请输入正确的手机号"];
           return;
       }
    
}
- (IBAction)registerButtonClick:(UIButton *)sender {
    if (self.phoneTextField.text.length != 11) {
        [MBProgressHUD showError:@"请输入正确的手机号"];
        return;
    }else if (self.codeTextField.text.length != 6 ){
        [MBProgressHUD showError:@"验证码不正确"];
        return;
    }else if (self.passwordTextField.text.length < 6){
        [MBProgressHUD showError:@"请设置密码长度至少6位"];
        return;
    }else if (self.againPasswordTextField.text.length < 6){
        [MBProgressHUD showError:@"请设置密码长度至少6位"];
        return;
    }else{
        if ([self.passwordTextField.text isEqualToString:self.againPasswordTextField.text]) {
            [SVProgressHUD show];
            [SVProgressHUD dismissWithDelay:3.0];
            [HttpManager fuckHttpManagerRequestUrl:@"http://www.ljsb.top:5000/common/forgetpwd" dic:@{@"phone":self.phoneTextField.text,@"code":self.codeTextField.text,@"password":self.passwordTextField.text,@"password2":self.againPasswordTextField.text} Succed:^(id  _Nonnull responseObject) {
                [SVProgressHUD dismiss];
                PBUserInfo *info = [[PBUserInfo alloc] init];
                info.phone = self.phoneTextField.text;
                info.userPassword =  self.passwordTextField.text;
                info.sex = 1;
                info.authorization = responseObject[@"result"][@"analysis"];
                [[UserInfoManager manager] save: info];
                [self.navigationController popToRootViewControllerAnimated:YES];
            } requestfailure:^(NSError * _Nonnull error) {
                
            }];
        }else{
            [MBProgressHUD showError:@"两次密码不一致"];
        }
    }

    
}
-(void)timerClick{
    self.count ++;
    if (self.count == 60) {
        [self.timer invalidate];
        self.timer = nil;
        self.codeButton.userInteractionEnabled = YES;
        [self.codeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
        return;
    }
    [self.codeButton setTitle:[NSString stringWithFormat:@"%ldS",60 - self.count] forState:UIControlStateNormal];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)dealloc {
    [_phoneTextField release];
    [_codeTextField release];
    [_passwordTextField release];
    [_againPasswordTextField release];
    [_codeButton release];
    [_registerButton release];
    [super dealloc];
}
@end
