//
//  EngineTools.m
//  ProfessionBarritser
//
//  Created by 松松的漫漫 on 2018/4/19.
//  Copyright © 2018年 LSS. All rights reserved.
//

#import "EngineTools.h"
#import <sys/utsname.h>
#import <CommonCrypto/CommonDigest.h>
#import <objc/runtime.h>
@implementation EngineTools
//富文本改变字体颜色
+ (NSMutableAttributedString *)changeFontAndColor:(UIFont *)font Color:(UIColor *)color TotalString:(NSString *)totalString SubString:(NSArray *)subArray {
    NSMutableAttributedString *attributedStr;
    NSString *origString;
    if ([totalString isKindOfClass:[NSAttributedString class]]) {
        attributedStr = [[NSMutableAttributedString alloc] initWithAttributedString:(NSAttributedString *)totalString];
        origString = attributedStr.string;
    } else {
        attributedStr = [[NSMutableAttributedString alloc] initWithString:totalString];
        origString = totalString;
    }
    for (NSString *rangeStr in subArray) {
        NSRange range = [origString rangeOfString:rangeStr options:NSBackwardsSearch];
        if (color) {
            [attributedStr addAttribute:NSForegroundColorAttributeName value:color range:range];
        }
        if (font) {
            [attributedStr addAttribute:NSFontAttributeName value:font range:range];
        }
    }
    return attributedStr;
}
#pragma mark - 富文本设置部分字体颜色
+ (NSMutableAttributedString *)setupAttributeString:(NSString *)text rangeText:(NSString *)rangeText textColor:(UIColor *)color WithFont:(UIFont *)font  {
    NSRange hightlightTextRange = [text rangeOfString:rangeText];
    NSMutableAttributedString *attributeStr = [[NSMutableAttributedString alloc] initWithString:text];
    if (hightlightTextRange.length > 0) {
        [attributeStr addAttribute:NSForegroundColorAttributeName
                             value:color
                             range:hightlightTextRange];
        [attributeStr addAttribute:NSFontAttributeName value:font range:hightlightTextRange];
        return attributeStr;
    }else {
        return [rangeText copy];
    }
}
+ (NSMutableAttributedString *)addLineWithString:(NSString *)string stringColor:(UIColor *)color
{
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:string];
    NSUInteger length = string.length;
    [attri addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlinePatternSolid | NSUnderlineStyleSingle) range:NSMakeRange(0, length)];
    
    [attri addAttribute:NSStrikethroughColorAttributeName value:color range:NSMakeRange(0, length)];
    
    return attri;
}

+ (NSMutableAttributedString *)addLineBottomWithString:(NSString *)string stringColor:(UIColor *)color
{
    NSMutableAttributedString *attri = [[NSMutableAttributedString alloc] initWithString:string];
    NSUInteger length = string.length;
    [attri addAttribute:NSUnderlineStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, length)];
    
    [attri addAttribute:NSStrikethroughColorAttributeName value:color range:NSMakeRange(0, length)];
    
    return attri;
}

+ (CGFloat)widthWithHeight:(CGFloat)height andFont:(UIFont *)font string:(NSString *)string{
    NSDictionary *attribute = @{NSFontAttributeName:font};
    CGSize  size = [string boundingRectWithSize:CGSizeMake(MAXFLOAT, height)  options:(NSStringDrawingUsesFontLeading|NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin)   attributes:attribute context:nil].size;
    return size.width;
}
+ (CGFloat )getHeigthValue:(NSString *)string font:(CGFloat)font width:(CGFloat)width
{
    NSStringDrawingOptions options =  NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
    
    CGRect rect = [string boundingRectWithSize:CGSizeMake(width,MAXFLOAT) options:options attributes:@{NSFontAttributeName:FONT_MEDIUM(font)} context:nil];
    
    CGFloat realHeight = ceilf(rect.size.height);
    return realHeight;
}

+ (BOOL)isMobilePhone:(NSString *)mobilePhone
{
    NSString *reg = @"^((13[0-9])|(15[^4,\\D])|(18[0,0-9])|(17[0,0-9]))\\d{8}$";
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES%@", reg];
    return [predicate evaluateWithObject:mobilePhone];
}
+ (UIAlertController *)alertViewControllTitle:(NSString *)title Message:(NSString *)msg cancelMessage:(NSString *)cancelMsg sureMessage:(NSString *)sureMsg selectResult:(void(^)(BOOL select))result
{
    
    UIAlertController *alerController = [UIAlertController alertControllerWithTitle:title message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancelAlert = [UIAlertAction actionWithTitle:cancelMsg style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        if (result)
        {
            result(NO);
        }
    }];
    
    UIAlertAction *sureAlert = [UIAlertAction actionWithTitle:sureMsg style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (result)
        {
            result(YES);
        }
    }];
    [alerController addAction:cancelAlert];
    [alerController addAction:sureAlert];
    return alerController;
}

+ (NSString *)getCreatTime
{
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *timeString = [formatter stringFromDate:date];
    NSString *classString = [NSString stringWithFormat:@"直播%@",timeString];
    return classString;
}

+ (NSString *)getTime:(NSInteger)times
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:times/1000];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"MM月dd日 HH:mm";
    NSString *timeString = [formatter stringFromDate:date];
    return timeString;
}
+ (NSString *)getYearTime:(NSInteger)times{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:times/1000];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"YYYY-MM-dd HH:mm";
    NSString *timeString = [formatter stringFromDate:date];
    return timeString;
}
+ (NSString *)getPayTime:(NSInteger)times
{
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:times/1000];
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"YYYY-MM-dd";
    NSString *timeString = [formatter stringFromDate:date];
    return timeString;
}

#pragma mark 实现搜索条背景透明化
+ (UIImage*)GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height{
    CGRect r= CGRectMake(0.0f, 0.0f, 1.0f, height);
    UIGraphicsBeginImageContext(r.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, r);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}
+ (BOOL )isIphoneX
{
    DLog(@"current device model ==%@",[self getDeviceModel]);
    if ([[self getDeviceModel] isEqualToString:@"iPhone X"])
    {
        return YES;
    }
    return NO;
}
+ (NSString *)getDeviceModel
{
    NSDictionary* deviceNamesByCode = nil;
    NSString* deviceName = nil;
    if (deviceName) {
        return deviceName;
    }
    deviceNamesByCode = @{
                          @"i386"      :@"Simulator",
                          @"iPod1,1"   :@"iPod Touch 1",      // (Original)
                          @"iPod2,1"   :@"iPod Touch 2",      // (Second Generation)
                          @"iPod3,1"   :@"iPod Touch 3",      // (Third Generation)
                          @"iPod4,1"   :@"iPod Touch 4",      // (Fourth Generation)
                          @"iPhone1,1" :@"iPhone 2G",          // (Original)
                          @"iPhone1,2" :@"iPhone 3G",          // (3G)
                          @"iPhone2,1" :@"iPhone 3GS",          // (3GS)
                          @"iPad1,1"   :@"iPad 1",            // (Original)
                          @"iPad2,1"   :@"iPad 2",          //
                          @"iPad3,1"   :@"iPad 3",            // (3rd Generation)
                          @"iPhone3,1" :@"iPhone 4",        //
                          @"iPhone4,1" :@"iPhone 4S",       //
                          @"iPhone5,1" :@"iPhone 5",        // (model A1428, AT&T/Canada)
                          @"iPhone5,2" :@"iPhone 5",        // (model A1429, everything else)
                          @"iPad3,4"   :@"iPad 3",            // (4th Generation)
                          @"iPad2,5"   :@"iPad Mini",       // (Original)
                          @"iPhone5,3" :@"iPhone 5c",       // (model A1456, A1532 | GSM)
                          @"iPhone5,4" :@"iPhone 5c",       // (model A1507, A1516, A1526 (China), A1529 | Global)
                          @"iPhone6,1" :@"iPhone 5s",       // (model A1433, A1533 | GSM)
                          @"iPhone6,2" :@"iPhone 5s",       // (model A1457, A1518, A1528 (China), A1530 | Global)
                          @"iPhone7,1" :@"iPhone 6 Plus",
                          @"iPhone7,2" :@"iPhone 6",
                          @"iPhone8,1" :@"iPhone 6s",
                          @"iPhone8,2" :@"iPhone 6s Plus",
                          @"iPhone8,4" :@"iPhone SE",
                          @"iPhone9,1" :@"iPhone 7",
                          @"iPhone9,3" :@"iPhone 7",
                          @"iPhone9,2" :@"iPhone 7 Plus",
                          @"iPhone9,4" :@"iPhone 7 Plus",
                          @"iPhone10,1" :@"iPhone 8",
                          @"iPhone10,4" :@"iPhone 8",
                          @"iPhone10,2" :@"iPhone 8 Plus",
                          @"iPhone10,5" :@"iPhone 8 Plus",
                          @"iPhone10,3" :@"iPhone X",
                          @"iPhone10,6" :@"iPhone X",
                          @"iPad4,1"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Wifi
                          @"iPad4,2"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Cellular
                          @"iPad4,3"   :@"iPad Air",        // 5th Generation iPad (iPad Air) - Cellular
                          @"iPad4,4"   :@"iPad Mini 2",       // (2nd Generation iPad Mini - Wifi)
                          @"iPad4,5"   :@"iPad Mini 2",        // (2nd Generation iPad Mini - Cellular)
                          @"iPad4,6"   :@"iPad Mini 2",        // (2nd Generation iPad Mini - Cellular)
                          @"iPad4,7"   :@"iPad Mini 3",        // (2nd Generation iPad Mini - Cellular)
                          @"iPad4,8"   :@"iPad Mini 3",        // (2nd Generation iPad Mini - Cellular)
                          @"iPad4,9"   :@"iPad Mini 3",        // (2nd Generation iPad Mini - Cellular)
                          @"iPad5,1"   :@"iPad Mini 4",
                          @"iPad5,2"   :@"iPad Mini 4",
                          @"iPad5,3"   :@"iPad Air 2",
                          @"iPad5,4"   :@"iPad Air 2",
                          @"iPad6,7"   :@"iPad Pro 12.9",
                          @"iPad6,8"   :@"iPad Pro 12.9",
                          @"iPad6,3"   :@"iPad Pro 9.7",
                          @"iPad6,4"   :@"iPad Pro 9.7",
                          @"iPad6,11"   :@"iPad 2017",
                          @"iPad6,12"   :@"iPad 2017",
                          @"iPad7,1"   :@"iPad Pro 2 12.9",
                          @"iPad7,2"   :@"iPad Pro 2 12.9",
                          @"iPad7,3"   :@"iPad Pro 10.5",
                          @"iPad7,4"   :@"iPad Pro 10.5"
                          };
    
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *code = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    deviceName = [deviceNamesByCode objectForKey:code];
    if (!deviceName) {
        deviceName = code;
        // Not found in database. At least guess main device type from string contents:
        //if ([code rangeOfString:@"iPod"].location != NSNotFound) {
        //    deviceName = @"iPod Touch";
        //} else if([code rangeOfString:@"iPad"].location != NSNotFound) {
        //    deviceName = @"iPad";
        //} else if([code rangeOfString:@"iPhone"].location != NSNotFound){
        //    deviceName = @"iPhone";
        //} else {
        //    deviceName = @"Simulator";
        //}
    }
    return deviceName;
}

//字典转json字符串
+ (NSString*)dictionaryToJson:(NSDictionary *)dic {
    if (dic == nil) {
        return @"";
    }
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}
//json字符串转字典
+(NSDictionary*)JSONStrToDictionary:(NSString*)jsonStr{
    
    //JSON字符串
    if (jsonStr == nil) {
        return nil;
    }
    if ([jsonStr isKindOfClass:[NSMutableDictionary class]]||[jsonStr isKindOfClass:[NSDictionary class]]) {
        return (NSDictionary*)jsonStr;
    }
    NSData *jsonData = [jsonStr dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        return nil;
    }
    return dic;
}

//数组转json字符串
+ (NSString *)arrayToJSONString:(NSArray *)array
{
    
    if (array.count == 0) {
        return @"";
    }
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:array options:NSJSONWritingPrettyPrinted error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}


//json字符串转为数组
+ (NSArray *)stringToJSON:(NSString *)jsonStr {
    if (jsonStr) {
        id tmp = [NSJSONSerialization JSONObjectWithData:[jsonStr dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments | NSJSONReadingMutableLeaves | NSJSONReadingMutableContainers error:nil];
        if (tmp) {
            if ([tmp isKindOfClass:[NSArray class]]) {
                return tmp;
            } else if([tmp isKindOfClass:[NSString class]]
                      || [tmp isKindOfClass:[NSDictionary class]]) {
                return [NSArray arrayWithObject:tmp];
            } else {
                return nil;
            }
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

/**
 *  将普通字符串转换成base64字符串
 *
 *  @param text 普通字符串
 *
 *  @return base64字符串
 */
+ (NSString *)base64StringFromText:(NSString *)text {
    
    NSData *data = [text dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64String = [data base64EncodedStringWithOptions:0];
    return base64String;
}
/**
 *  将base64字符串转换成普通字符串
 *
 *  @param base64 base64字符串
 *
 *  @return 普通字符串
 */
+ (NSString *)textFromBase64String:(NSString *)base64 {
    NSData *data = [[NSData alloc] initWithBase64EncodedString:base64 options:0];
    NSString *text = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    return text;
}

+ (id)cleanNullWithResponseObject:(id)responseObject
{
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        return [self cleanNullWithDictionary:responseObject];
    }else{
        return [self cleanNullWithArray:responseObject];
    }
}
+ (NSDictionary *)cleanNullWithDictionary:(NSDictionary *)dic
{
    NSMutableDictionary *dict = [dic mutableCopy];
    for (NSString *key in [dict allKeys]) {
        if ([dict[key] isKindOfClass:[NSNull class]]) {
            dict[key] = @"";
        }
        else if ([dict[key] isKindOfClass:[NSDictionary class]]){
            dict[key] = [self cleanNullWithDictionary:dict[key]];
            
        }else if ([dict[key] isKindOfClass:[NSArray class]]){
            dict[key] = [self cleanNullWithArray:dict[key]];
        }
    }
    return dict;
}
+ (NSArray *)cleanNullWithArray:(NSArray *)arr
{
    NSMutableArray *array = [arr mutableCopy];
    for (int i = 0; i < array.count; i++) {
        if ([array[i] isKindOfClass:[NSNull class]]) {
            array[i] = @"";
            
        }else if ([array[i] isKindOfClass:[NSArray class]]) {
            array[i] = [self cleanNullWithArray:array[i]];
            
        }else if ([array[i] isKindOfClass:[NSDictionary class]]) {
            array[i] = [self cleanNullWithDictionary:array[i]];
        }
    }
    return array;
}
//+ (NSURL *)getImageUrl:(NSString *)url
//{
//    NSString *imageUrl = [self imageUrl:url];
//    return [NSURL URLWithString:imageUrl];
//}
//+ (NSString *)imageUrl:(NSString *)url
//{
//    if (![url containsString:@"http"])
//    {
//        NSString *one = [url substringToIndex:1];
//        if ([one isEqualToString:@"/"])
//        {
//            url = [NSString stringWithFormat:@"%@%@",BASEURL,[url substringFromIndex:1]];
//        }
//        else
//        {
//            url = [NSString stringWithFormat:@"%@%@",BASEURL,url];
//        }
//    }
//    return url;
//}

+(NSString *)timeFormmatted:(NSInteger)totalSeconds
{
    NSInteger seconds = totalSeconds%60;
    NSInteger minutes = (totalSeconds/60)%60;
    NSInteger hours = totalSeconds/3600;
    
    return [NSString stringWithFormat:@"%02ld:%02ld:%02ld",hours,minutes,seconds];
}

+(void)deleteFilePath:(NSString*)path {
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
        
        NSError *error = nil;
        [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
        if (!error) {
            NSLog(@"mp4 删除成功");
        } else {
            NSLog(@"mp4 删除失败 error = %@", error);
        }
    }
    
}

+(UIViewController *)getCurrentVC {
    
    UIViewController *topRootViewController = [[UIApplication  sharedApplication] keyWindow].rootViewController;
    
    // 在这里加一个这个样式的循环
    while (topRootViewController.presentedViewController)
    {
        // 这里固定写法
        topRootViewController = topRootViewController.presentedViewController;
    }
    
    /*
     *  在此判断返回的视图是不是你的根视图--我的根视图是tabbar
     */
    if ([topRootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *mainTabBarVC = (UITabBarController *)topRootViewController;
        topRootViewController = [mainTabBarVC selectedViewController];
        topRootViewController = [topRootViewController.childViewControllers lastObject];
    }else{
        //导航堆栈
        topRootViewController =  topRootViewController.childViewControllers.lastObject;
    }
    
    return topRootViewController;
    
}

+ (NSString *)cleanEmptyString:(NSString *)string
{
    while ([string containsString:@" "])
    {
        string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    return string;
}
//截取掉最后一个/
+(NSString *)subLastCharStringWithStr:(NSString *)str{
    NSString *one = [str substringWithRange:NSMakeRange(str.length-1, 1)];
    if ([one isEqualToString:@"/"])
    {
        str = [str substringFromIndex:1];
    }
    return str;
}
//截取掉第一个"/"
+(NSString *)subFirstCharStringWithStr:(NSString *)str{
    NSString *one = [str substringWithRange:NSMakeRange(0, 1)];
    if ([one isEqualToString:@"/"])
    {
        str = [str substringFromIndex:1];
    }
    return str;
}

+ (NSString *)md5:(NSString *)string
{
//    const char *cStr = [string UTF8String];
//    unsigned char result[CC_MD5_DIGEST_LENGTH];
//
//    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
//
//    return [NSString stringWithFormat:
//            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
//            result[0], result[1], result[2], result[3], result[4], result[5], result[6], result[7],
//            result[8], result[9], result[10], result[11], result[12], result[13], result[14], result[15]
//            ];
    const char* input = [string UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(input, (CC_LONG)strlen(input), result);
    
    NSMutableString *digest = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [digest appendFormat:@"%02X", result[i]];
    }
    
    return digest;
}

+(void)radiusWithCell:(UITableViewCell *)cell andTableView:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath{
    if ([cell respondsToSelector:@selector(tintColor)]) {
        //        if (tableView == self.tableView) {
        CGFloat cornerRadius = 8.f;
        cell.backgroundColor = UIColor.clearColor;
        CAShapeLayer *layer = [[CAShapeLayer alloc] init];
        CGMutablePathRef pathRef = CGPathCreateMutable();
        CGRect bounds = CGRectInset(cell.bounds, 0, 0);
        BOOL addLine = NO;
        if (indexPath.row == 0 && indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
            CGPathAddRoundedRect(pathRef, nil, bounds, cornerRadius, cornerRadius);
        } else if (indexPath.row == 0) {
            CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds));
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds), CGRectGetMidX(bounds), CGRectGetMinY(bounds), cornerRadius);
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
            CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds));
            addLine = YES;
            
        } else if (indexPath.row == [tableView numberOfRowsInSection:indexPath.section]-1) {
            CGPathMoveToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMinY(bounds));
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMinX(bounds), CGRectGetMaxY(bounds), CGRectGetMidX(bounds), CGRectGetMaxY(bounds), cornerRadius);
            CGPathAddArcToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMaxY(bounds), CGRectGetMaxX(bounds), CGRectGetMidY(bounds), cornerRadius);
            CGPathAddLineToPoint(pathRef, nil, CGRectGetMaxX(bounds), CGRectGetMinY(bounds));
        } else {
            CGPathAddRect(pathRef, nil, bounds);
            addLine = YES;
        }
        layer.path = pathRef;
        CFRelease(pathRef);
        //颜色修改
        layer.fillColor = [UIColor colorWithWhite:1.f alpha:1.0f].CGColor;
        layer.strokeColor = DEFAULT_BG_COLORB1.CGColor;
        if (addLine == YES) {
            CALayer *lineLayer = [[CALayer alloc] init];
            CGFloat lineHeight = (1.f / [UIScreen mainScreen].scale);
            lineLayer.frame = CGRectMake(CGRectGetMinX(bounds)+10, bounds.size.height-lineHeight, bounds.size.width-10, lineHeight);
            lineLayer.backgroundColor = tableView.separatorColor.CGColor;
            [layer addSublayer:lineLayer];
        }
        UIView *testView = [[UIView alloc] initWithFrame:bounds];
        [testView.layer insertSublayer:layer atIndex:0];
        testView.backgroundColor = UIColor.clearColor;
        //        cell.backgroundView = testView;
        [cell.contentView insertSubview:testView atIndex:0];
    }
}

+ (UIImage *)createQRForString:(NSString *)qrString withSize:(CGFloat)size {
    // Need to convert the string to a UTF-8 encoded NSData object
    NSData *stringData = [qrString dataUsingEncoding:NSUTF8StringEncoding];
    // Create the filter
    CIFilter *qrFilter = [CIFilter filterWithName:@"CIQRCodeGenerator"];
    // Set the message content and error-correction level
    [qrFilter setValue:stringData forKey:@"inputMessage"];
    [qrFilter setValue:@"M" forKey:@"inputCorrectionLevel"];
    
    CGRect extent = CGRectIntegral(qrFilter.outputImage.extent);
    CGFloat scale = MIN(size/CGRectGetWidth(extent), size/CGRectGetHeight(extent));
    // create a bitmap image that we'll draw into a bitmap context at the desired size;
    size_t width = CGRectGetWidth(extent) * scale;
    size_t height = CGRectGetHeight(extent) * scale;
    CGColorSpaceRef cs = CGColorSpaceCreateDeviceGray();
    CGContextRef bitmapRef = CGBitmapContextCreate(nil, width, height, 8, 0, cs, (CGBitmapInfo)kCGImageAlphaNone);
    CIContext *context = [CIContext contextWithOptions:nil];
    CGImageRef bitmapImage = [context createCGImage:qrFilter.outputImage fromRect:extent];
    CGContextSetInterpolationQuality(bitmapRef, kCGInterpolationNone);
    CGContextScaleCTM(bitmapRef, scale, scale);
    CGContextDrawImage(bitmapRef, extent, bitmapImage);
    // Create an image with the contents of our bitmap
    CGImageRef scaledImage = CGBitmapContextCreateImage(bitmapRef);
    // Cleanup
    CGContextRelease(bitmapRef);
    CGImageRelease(bitmapImage);
    return [UIImage imageWithCGImage:scaledImage];
}

+ (NSString *)getTimeSpace:(NSInteger)space
{
    NSInteger minutes = (space / 60)%60;
    if (space/60 != 0) {
        minutes = minutes +1;
    }
    NSInteger hours = space / (60*60);
//    NSInteger day = space / (60*60*24);
//    NSInteger month = space / (60*60*24*30);
//    NSInteger yers = space /( 60*60*24*365);
    NSString *time = @"1分钟";;
//    if (yers >0)
//    {
//        time = [NSString stringWithFormat:@"%ld年%ld月%ld日%ld小时%ld分",yers,month,day,hours,minutes];
//    }
//    else if (month > 0)
//    {
//        time = [NSString stringWithFormat:@"%ld月%ld日%ld小时%ld分",month,day,hours,minutes];
//    }
//    else if (day > 0)
//    {
//        time = [NSString stringWithFormat:@"%ld日%ld小时%ld分",day,hours,minutes];
//    }
    if (hours > 0)
    {
        time = [NSString stringWithFormat:@"%ld小时%ld分",hours,minutes];
    }
    else if (minutes > 0)
    {
        time = [NSString stringWithFormat:@"%ld分",minutes];
    }
    return time;
}


//+ (NSString *)getTimeFreeSpace:(NSInteger)space
//{
//    NSInteger second = space % 60;
//    NSInteger minutes = (space / 60)%60;
//    NSInteger hours = (space / (60*60))%24;
//    NSInteger day = (space / (60*60*24))%30;
//    NSInteger month = (space / (60*60*24*30))%365;
//    NSInteger yers = space /( 60*60*24*365);
//    NSString *time = @"0";
//    if (yers >0)
//    {
//        time = [NSString stringWithFormat:@"%ld年%ld月%ld日%ld时%ld分%ld秒",yers,month,day,hours,minutes,second];
//    }
//    else if (month > 0)
//    {
//        time = [NSString stringWithFormat:@"%ld月%ld日%ld时%ld分%ld秒",month,day,hours,minutes,second];
//    }
//    else if (day > 0)
//    {
//        time = [NSString stringWithFormat:@"%ld日%ld时%ld分%ld秒",day,hours,minutes,second];
//    }
//    else if (hours > 0)
//    {
//        time = [NSString stringWithFormat:@"%ld时%ld分%ld秒",hours,minutes,second];
//    }
//    else if (minutes > 0)
//    {
//        time = [NSString stringWithFormat:@"%ld分%ld秒",minutes,second];
//    }
//    else if (second > 0)
//    {
//        time = [NSString stringWithFormat:@"00:%ld",second];
//    }
//    return time;
//}

+ (NSString *)getSpaceTime:(NSInteger)space
{
    NSInteger second = space % 60;
    NSInteger minutes = (space / 60)%60;
    NSInteger hours = (space / (60*60));
    NSString *time = @"0";
    if (hours > 0)
    {
        time = [NSString stringWithFormat:@"%ld:%ld:%ld",hours,minutes,second];
    }
    else if (minutes > 0)
    {
        time = [NSString stringWithFormat:@"%ld:%ld",minutes,second];
    }
    else if (second > 0)
    {
        time = [NSString stringWithFormat:@"00:%ld",second];
    }
    return time;
}
//model转化为字典
+(NSDictionary *)dicFromObject:(NSObject *)object{
    NSMutableDictionary *dic = [ [NSMutableDictionary alloc] init];
    unsigned int count;
    objc_property_t *propertyList = class_copyPropertyList([object class], &count);
    
    for (int i = 0; i < count; i++) {
        objc_property_t property = propertyList[i];
        const char *cName = property_getName(property);
        NSString *name = [NSString stringWithUTF8String:cName];
        NSObject *value = [object valueForKey:name];//valueForKey返回的数字和字符串都是对象
        
        if ([value isKindOfClass:[NSString class]] || [value isKindOfClass:[NSNumber class]]) {
            //string , bool, int ,NSinteger
            [dic setObject:value forKey:name];
            
        } else if ([value isKindOfClass:[NSArray class]] || [value isKindOfClass:[NSDictionary class]]) {
            //字典或字典
            [dic setObject:[self arrayOrDicWithObject:(NSArray*)value] forKey:name];
            
        } else if (value == nil) {
            //null
            //[dic setObject:[NSNull null] forKey:name];//这行可以注释掉?????
            
        } else {
            //model
            [dic setObject:[self dicFromObject:value] forKey:name];
        }
    }
    
    return [dic copy];
}
//将可能存在model数组转化为普通数组
+ (id)arrayOrDicWithObject:(id)origin {
    if ([origin isKindOfClass:[NSArray class]]) {
        //数组
        NSMutableArray *array = [ [NSMutableArray alloc] init];
        for (NSObject *object in origin) {
            if ([object isKindOfClass:[NSString class]] || [object isKindOfClass:[NSNumber class]]) {
                //string , bool, int ,NSinteger
                [array addObject:object];
                
            } else if ([object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]]) {
                //数组或字典
                [array addObject:[self arrayOrDicWithObject:(NSArray *)object]];
                
            } else {
                //model
                [array addObject:[self dicFromObject:object]];
            }
        }
        
        return [array copy];
        
    } else if ([origin isKindOfClass:[NSDictionary class]]) {
        //字典
        NSDictionary *originDic = (NSDictionary *)origin;
        NSMutableDictionary *dic = [ [NSMutableDictionary alloc] init];
        for (NSString *key in originDic.allKeys) {
            id object = [originDic objectForKey:key];
            
            if ([object isKindOfClass:[NSString class]] || [object isKindOfClass:[NSNumber class]]) {
                //string , bool, int ,NSinteger
                [dic setObject:object forKey:key];
                
            } else if ([object isKindOfClass:[NSArray class]] || [object isKindOfClass:[NSDictionary class]]) {
                //数组或字典
                [dic setObject:[self arrayOrDicWithObject:object] forKey:key];
                
            } else {
                //model
                [dic setObject:[self dicFromObject:object] forKey:key];
            }
        }
        
        return [dic copy];
    }
    
    return [NSNull null];
}

@end
