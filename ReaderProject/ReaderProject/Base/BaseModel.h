//
//  BaseModel.h
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@interface NSObject (Reflection)

- (void)reflectValuesFromDict:(NSDictionary *)dict;
- (void)reflectPropertiesToObject:(NSObject *)obj;
- (void)reflectPropertiesFromObject:(NSObject *)obj;

@end
@interface BaseModel : NSObject<NSCopying>

- (instancetype)initWithDict:(NSDictionary *)jsonDic;

- (void)objReflectFromDic:(NSDictionary *)jsonDic;

- (NSDictionary *)dictRepresentation;

@end

NS_ASSUME_NONNULL_END
