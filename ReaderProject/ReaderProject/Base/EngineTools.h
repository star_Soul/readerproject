//
//  EngineTools.h
//  ProfessionBarritser
//
//  Created by 松松的漫漫 on 2018/4/19.
//  Copyright © 2018年 LSS. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface EngineTools : NSObject
/**改变字体颜色*/
+ (NSMutableAttributedString *)changeFontAndColor:(UIFont *)font Color:(UIColor *)color TotalString:(NSString *)totalString SubString:(NSArray *)subArray;
#pragma mark - 富文本设置部分字体颜色
+ (NSMutableAttributedString *)setupAttributeString:(NSString *)text rangeText:(NSString *)rangeText textColor:(UIColor *)color WithFont:(UIFont *)font;
/**文字宽度*/
+ (CGFloat)widthWithHeight:(CGFloat)height andFont:(UIFont *)font string:(NSString *)string;
/**文字高度*/
+ (CGFloat )getHeigthValue:(NSString *)string font:(CGFloat)font width:(CGFloat)width;
/**文字中间加横线*/
+ (NSMutableAttributedString *)addLineWithString:(NSString *)string stringColor:(UIColor *)color;
/**文字下划线*/
+ (NSMutableAttributedString *)addLineBottomWithString:(NSString *)string stringColor:(UIColor *)color;
/** 时间格式MM月dd天 hh:mm */
+ (NSString *)getTime:(NSInteger)times;
/** 时间格式YYYY-MM-dd hh:mm */
+ (NSString *)getYearTime:(NSInteger)times;
/** 时间格式 YY-MM-dd */
+ (NSString *)getPayTime:(NSInteger)times;
/**判断是否是手机*/
+ (BOOL)isMobilePhone:(NSString *)mobilePhone;
/**新视频YYMMDDhhmmss*/
+ (NSString *)getCreatTime;
+ (UIAlertController *)alertViewControllTitle:(NSString *)title Message:(NSString *)msg cancelMessage:(NSString *)cancelMsg sureMessage:(NSString *)sureMsg selectResult:(void(^)(BOOL select))result;
/** 绘制背景色图片*/
+ (UIImage*)GetImageWithColor:(UIColor*)color andHeight:(CGFloat)height;
/**获取手机型号*/
+ (NSString *)getDeviceModel;
/**判断是否是X*/
+ (BOOL )isIphoneX;
//字典转json字符串
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;
//json字符串转字典
+(NSDictionary*)JSONStrToDictionary:(NSString*)jsonStr;
//数组转json字符串
+ (NSString *)arrayToJSONString:(NSArray *)array;
//json字符串转为数组
+ (NSArray *)stringToJSON:(NSString *)jsonStr;

/**
 *  将普通字符串转换成base64字符串
 *
 *  @param text 普通字符串
 *
 *  @return base64字符串
 */
+ (NSString *)base64StringFromText:(NSString *)text;
/**
 *  将base64字符串转换成普通字符串
 *
 *  @param base64 base64字符串
 *
 *  @return 普通字符串
 */
+ (NSString *)textFromBase64String:(NSString *)base64;

+ (id)cleanNullWithResponseObject:(id)responseObject;
+ (NSDictionary *)cleanNullWithDictionary:(NSDictionary *)dic;
+ (NSArray *)cleanNullWithArray:(NSArray *)arr;
+ (NSURL *)getImageUrl:(NSString *)url;
//+ (NSString *)imageUrl:(NSString *)url;
/**时间转换*/
+(NSString *)timeFormmatted:(NSInteger)totalSeconds;
+(void)deleteFilePath:(NSString*)path;

/**获取堆栈最上层控制器*/
+(UIViewController *)getCurrentVC;
+ (NSString *)cleanEmptyString:(NSString *)string;
//截取掉最后一个"/"
+(NSString *)subLastCharStringWithStr:(NSString *)str;
//截取掉第一个"/"
+(NSString *)subFirstCharStringWithStr:(NSString *)str;
+ (NSString *)md5:(NSString *)string;
//画cell的section圆弧
+(void)radiusWithCell:(UITableViewCell *)cell andTableView:(UITableView *)tableView andIndexPath:(NSIndexPath *)indexPath;
+ (UIImage *)createQRForString:(NSString *)qrString withSize:(CGFloat)size;
/**xx天xx小时xx分*/
+ (NSString *)getTimeSpace:(NSInteger)space;

/**xx:xx:xx:xx*/
+ (NSString *)getSpaceTime:(NSInteger)space;
//model转化为字典
+(NSDictionary *)dicFromObject:(NSObject *)object;
@end
