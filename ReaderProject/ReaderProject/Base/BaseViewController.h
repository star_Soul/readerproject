//
//  BaseViewController.h
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController
@property (nonatomic, weak)UIButton *back;

-(void)setLeftNavigationBarItemWithTitle:(NSString *)title selector:(SEL)selector;
-(void)setRightBtnWithTitle:(NSString *)title selector:(SEL)selector;
-(void)setRightBtnWithImage:(UIImage *)image selector:(SEL)selector;
- (void)setLineView;
@end

NS_ASSUME_NONNULL_END
