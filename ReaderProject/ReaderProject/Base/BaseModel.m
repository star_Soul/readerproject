//
//  BaseModel.m
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "BaseModel.h"
#import <objc/runtime.h>

@implementation NSObject (Reflection)
- (BOOL)containsKey:(NSString *)key withDict:(NSDictionary *)dict
{
    id value = [dict objectForKey:key];
    return value == nil ? NO : YES;
}

- (void)reflectValuesFromDict:(NSDictionary *)dict {
    Class class = [self class];
    
    unsigned int num_props;
    objc_property_t *prop_list;
    prop_list = class_copyPropertyList(class, &num_props);
    
    for(unsigned int i = 0; i < num_props; i++) {
        const char *cPropName = property_getName(prop_list[i]);
        NSString * propName = [NSString stringWithFormat:@"%s", cPropName];
        if ([self containsKey:propName withDict:dict]) {
            @try {
                [self setValue:dict[propName] forKey:propName];
            } @catch (NSException * e) {
                if ([[e name] isEqualToString:NSInvalidArgumentException]) {
                    // fix char-typed bool under 32-bit cpu
                    NSNumber* boolVal = [NSNumber numberWithBool:[dict[propName] boolValue]];
                    [self setValue:boolVal forKey:propName];
                }
            }
        }
    }
    free(prop_list);
}

- (void)reflectPropertiesToObject:(NSObject *)obj {
    Class class = [self class];
    Class targetClass = [obj class];
    
    unsigned int num_props;
    objc_property_t *prop_list;
    prop_list = class_copyPropertyList(class, &num_props);
    
    for(unsigned int i = 0; i < num_props; i++) {
        const char *cPropName = property_getName(prop_list[i]);
        NSString * propName = [NSString stringWithFormat:@"%s", cPropName];
        id value = [self valueForKey:propName];
        if ([value isKindOfClass:[NSNumber class]] && [value doubleValue] == 0) {
            continue;
        }
        if (value && value != [NSNull null] && class_getProperty(targetClass, cPropName) != NULL) {
            [obj setValue:value forKey:propName];
        }
    }
    free(prop_list);
}

- (void)reflectPropertiesFromObject:(NSObject *)obj {
    Class class = [self class];
    Class targetClass = [obj class];
    
    unsigned int num_props;
    objc_property_t *prop_list;
    prop_list = class_copyPropertyList(targetClass, &num_props);
    
    for(unsigned int i = 0; i < num_props; i++) {
        const char *cPropName = property_getName(prop_list[i]);
        NSString * propName = [NSString stringWithFormat:@"%s", cPropName];
        id value = [obj valueForKey:propName];
        if ([value isKindOfClass:[NSNumber class]] && [value doubleValue] == 0) {
            continue;
        }
        if (value && value != [NSNull null] && class_getProperty(class, cPropName) != NULL) {
            [self setValue:value forKey:propName];
        }
    }
    free(prop_list);
}

@end
@implementation BaseModel
- (id)copyWithZone:(NSZone *)zone {
    BaseModel *obj = [[self class] allocWithZone:zone];
    [obj reflectPropertiesFromObject:self];
    return obj;
}

- (instancetype)initWithDict:(NSDictionary *)jsonDic {
    self = [super init];
    if (self) {
        [self objReflectFromDic:jsonDic];
    }
    return self;
}

- (void)objReflectFromDic:(NSDictionary *)jsonDic {
    if (![jsonDic isKindOfClass:[NSDictionary class]]) {
        return;
    }
    for (NSString *key in [jsonDic allKeys]) {
        struct objc_property *property = class_getProperty([self class], [key UTF8String]);
        if (property == NULL) {
            continue;
        }
        
        id value = [jsonDic objectForKey:key];
        
        if (value == [NSNull null]) {
            continue;
        }
        
        if ([value isKindOfClass:[NSDictionary class]]) {
            char * classNameT = property_copyAttributeValue(property, "T");
            NSString *originClassName = [NSString stringWithUTF8String:classNameT];
            free(classNameT);
            
            NSString *className = [originClassName substringWithRange:NSMakeRange(2,originClassName.length-3)];
            NSDictionary *dic = (NSDictionary *)value;
            Class class = NSClassFromString(className);
            if (class) {
                id obj = [[class alloc] init];
                if ([obj isKindOfClass:[BaseModel class]]) {
                    [obj objReflectFromDic:dic];
                    [self setValue:obj forKey:key];
                } else if ([obj isKindOfClass:[NSDictionary class]]) {
                    [self setValue:value forKey:key];
                }
            }
        } else {
            if ([value isKindOfClass:[NSNumber class]]) {
                char * classNameT = property_copyAttributeValue(property, "T");
                NSString *originClassName = [NSString stringWithUTF8String:classNameT];
                free(classNameT);
                
                if (originClassName.length == 1) {
                    [self setValue:value forKeyPath:key];
                } else {
                    NSString *className = [originClassName substringWithRange:NSMakeRange(2,originClassName.length-3)];
                    if ([className isEqualToString:@"NSString"]) {
                        [self setValue:[NSString stringWithFormat:@"%@", value] forKey:key];
                    } else {
                        [self setValue:value forKeyPath:key];
                    }
                }
            } else {
                [self setValue:value forKeyPath:key];
            }
        }
    }
}

- (NSDictionary *)dictRepresentation {
    NSMutableSet *properties = [NSMutableSet new];
    [BaseModel propertyForClass:[self class] withSet:properties];
    
    NSMutableDictionary *dict = [ [NSMutableDictionary alloc] init];
    for (NSString *property in properties) {
        if ([property hasPrefix:@"__"]) {
            continue;
        }
        id value = [self valueForKey:property];
        if ([value isKindOfClass:[NSArray class]]) {
            if ([(NSArray *)value count] == 0) {
                continue;
            }
            NSMutableArray *array = [ [NSMutableArray alloc] init];
            for (NSObject *elem in value) {
                if ([elem isKindOfClass:[BaseModel class]]) {
                    NSDictionary *elemDict = [(BaseModel *)elem dictRepresentation];
                    [array addObject:elemDict];
                } else {
                    [array addObject:elem];
                }
            }
            [dict setObject:[array copy] forKey:property];
        } else if ([value isKindOfClass:[BaseModel class]]) {
            NSDictionary *subDict = [(BaseModel *)value dictRepresentation];
            [dict setObject:subDict forKey:property];
        } else if ([value isKindOfClass:[NSNumber class]]) {
            if ([(NSNumber *)value intValue] == 0) {
                value = @(0);
            } else if ([(NSNumber *)value intValue] == 1) {
                value = @(1);
            }
            [dict setObject:value forKey:property];
        } else if (value && value != [NSNull null]) {
            [dict setObject:value forKey:property];
        }
    }
    return [dict copy];
}

- (NSSet *)allPropertys {
    NSMutableSet* result = [NSMutableSet new];
    Class observed = [self class];
    while ([observed isSubclassOfClass:[self class]]) {
        [BaseModel propertyForClass:observed withSet:result];
        observed = [observed superclass];
    }
    return result;
}

+ (void)propertyForClass:(Class)class withSet:(NSMutableSet *)result {
    unsigned int num_props;
    objc_property_t *prop_list;
    prop_list = class_copyPropertyList(class, &num_props);
    
    for(unsigned int i = 0; i < num_props; i++) {
        NSString * propName = [NSString stringWithFormat:@"%s", property_getName(prop_list[i])];
        [result addObject: propName];
    }
    free(prop_list);
}
@end
