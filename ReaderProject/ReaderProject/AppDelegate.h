//
//  AppDelegate.h
//  ReaderProject
//
//  Created by dy on 2019/9/19.
//  Copyright © 2019 dingye. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WXApi.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate,WXApiDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

