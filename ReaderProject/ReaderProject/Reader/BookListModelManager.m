//
//  BookListModelManager.m
//  ReaderProject
//
//  Created by 李星星 on 2019/10/8.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BookListModelManager.h"

@implementation BookListModelManager
{
    //FMDatabase 没有考虑到多个线程同时操作数据库的情况
    NSLock *_lock;
    
}
//被static修饰，只会初始化一次， 并且会一直持有初始化的值
static BookListModelManager *manager = nil;
+ (BookListModelManager *)shareManager{
    //防止多个线程同时调用shareManager方法
    //@synchronized 一次只能允许一个线程访问关键字中的代码
    @synchronized(self){
        if (manager == nil) {
            manager = [[self alloc] init];
        }
    }
    return manager;
}
//重写init 进行一些必要的初始化操作
- (id)init{
    self = [super init];
    if (self) {
        _lock = [[NSLock alloc] init];
        //拼接数据库的路径
        //NSHomeDirectory() 程序的沙盒根目录
        _dbPath = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/BookListModelInfo.db"];
        NSLog(@"%@",_dbPath);
        //创建fmdb对象，并将路径传递过去
        _dataBase = [[FMDatabase alloc] initWithPath:_dbPath];
        //open dbPath中没有数据库文件，会创建并打开数据库；有文件，则直接打开
        //[_dataBase close];
        if ([_dataBase open]) {
            //创建表 blob 二进制对象类型
            NSString *createSql = @"create table if not exists BookListModelInfo(id integer primary key autoincrement,IDString text,num  integer,chapterId text,content text,created text,number text,state text,title text,updated text,words text,page integer,pageArr text)";
            //executeUpdate 增、删、改，创建表 的sql全用此方法
            //返回值为执行的结果 yes no
            BOOL isSuccessed  = [_dataBase executeUpdate:createSql];
            if (!isSuccessed) {
                //打印失败的信息
                NSLog(@"create error:%@",_dataBase.lastErrorMessage);
            }
        }
    }
    return self;
}
-(void)insertDataWithModel:(BookmarkModel *)model{
    [_lock lock];
    //    UIImage *image = model.headImage;
    //将图片转化成NSData
    //UIImagePNGRepresentation 将png格式的图片转化成NSData
    //    NSData *data = UIImagePNGRepresentation(image);
    //UIImageJPEGRepresentation(<#UIImage *image#>, CGFloat compressionQuality)
    //sqlite中 用?作为占位符
    NSString *insertSql = @"insert into BookListModelInfo(IDString,num,chapterId,content,created,number,state,title,updated,words,page,pageArr) values(?,?,?,?,?,?,?,?,?,?,?,?)";
    //executeUpdate 要求后面跟的参数必须是NSObject类型,否则会抛出EXC_BAD_ACCESS错误,fmdb会在将数据写入之前对数据进行自动转化
    BOOL isSuccessd =[_dataBase executeUpdate:insertSql,model.IDString,[NSString stringWithFormat:@"%ld",model.num],model.chapterId,model.content,model.created,model.number,model.state,model.title,model.updated,model.words,[NSString stringWithFormat:@"%ld",model.page],[self gs_jsonStringCompactFormatForNSArray:model.pageArr]];
    if (!isSuccessd) {
        NSLog(@"insert error:%@",_dataBase.lastErrorMessage);
    }
    [_lock unlock];
}
- (NSString *)gs_jsonStringCompactFormatForNSArray:(NSArray *)arrJson {

    if (![arrJson isKindOfClass:[NSArray class]] || ![NSJSONSerialization isValidJSONObject:arrJson]) {

        return nil;

    }
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:arrJson options:0 error:nil];

    NSString *strJson = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    return strJson;

}
-(void)deleteDataWithChapterId:(NSString *)chapterId IDString:(NSString *)IDString title:(NSString *)title page:(NSInteger)page{
    [_lock lock];
    NSString *deleteSql =[NSString stringWithFormat:@"delete from BookListModelInfo where chapterId = '%@' title = '%@' page = '%ld' and IDString = '%@' ",chapterId,title,page,IDString];
    BOOL isSuccessed =[_dataBase executeUpdate:deleteSql,time];
    if (!isSuccessed) {
        NSLog(@"delete error:%@",_dataBase.lastErrorMessage);
    }
    [_lock unlock];
}
-(void)deleteData{
    [_lock lock];
    NSString *deleteSql =[NSString stringWithFormat:@"delete from BookListModelInfo"] ;
    BOOL isSuccessed =[_dataBase executeUpdate:deleteSql,time];
    if (!isSuccessed) {
        NSLog(@"delete error:%@",_dataBase.lastErrorMessage);
    }
    [_lock unlock];
}
-(NSArray *)fetchAllUsers{
    [_lock lock];
    NSString *seleteSql =[NSString stringWithFormat:@"select * from BookListModelInfo"];
    //查询的sql语句用executeQuery
    //FMResultSet 查询结果的集合类
    FMResultSet *set =[_dataBase executeQuery:seleteSql];
    //next 从第一条数据开始，一直能取到最后一条，能取到当前的数据返回YES
    NSMutableArray *array = [ [NSMutableArray alloc] init];
    while ([set next]) {
        //根据字段名称，获取字段的值
        BookmarkModel *model = [[BookmarkModel alloc] init];
        //stringForColumn 获取字符串的值
        model.IDString = [set stringForColumn:@"IDString"];
        model.num = [[set stringForColumn:@"num"] integerValue];
        model.chapterId = [set stringForColumn:@"chapterId"];
        model.content = [set stringForColumn:@"content"];
        model.created = [set stringForColumn:@"created"];
        model.number = [set stringForColumn:@"number"];
        model.state = [set stringForColumn:@"state"];
        model.title = [set stringForColumn:@"title"];
        model.updated = [set stringForColumn:@"updated"];
        model.words = [set stringForColumn:@"words"];
        model.page = [[set stringForColumn:@"page"] integerValue];
        model.pageArr = [self jsonToObject:[set stringForColumn:@"pageArr"]];
        
        //获取NSData值
        [array addObject:model];
    }
    [_lock unlock];
    return array;
}
-(NSArray *)jsonToObject:(NSString *)json{
    //string转data
    NSData * jsonData = [json dataUsingEncoding:NSUTF8StringEncoding];
    //json解析
    NSArray * obj = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
    return obj;
}

-(BOOL)isDataExistsWithName:(NSString *)name IDString:(NSString *)IDString{
    NSString *selectSql = @"select * from BookListModelInfo where name = '%@' and IDString = '%@'";
    FMResultSet *set = [_dataBase executeQuery:selectSql,name,IDString];
    return [set next];
}
@end
