//
//  BottomSettingView.m
//  ReaderProject
//
//  Created by 李星星 on 2019/9/30.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BottomSettingView.h"

@interface BottomSettingView()
@property(nonatomic,strong)UIButton *listButton;

@property(nonatomic,strong)UIButton *lightModeButton;

@property(nonatomic,strong)UIButton *settingButton;

@end

@implementation BottomSettingView
-(UIButton *)listButton{
    if (!_listButton) {
        _listButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_listButton setTitle:@"目录" forState:UIControlStateNormal];
        [_listButton setImage:[UIImage imageNamed:@"目录-1"] forState:UIControlStateNormal];
    }
    return _listButton;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)dealloc {
    [_firstButton release];
    [_secondButton release];
    [_thirdButton release];
    [super dealloc];
}
@end
