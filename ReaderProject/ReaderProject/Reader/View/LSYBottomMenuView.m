//
//  LSYBottomMenuView.m
//  LSYReader
//
//  Created by Labanotation on 16/6/1.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYBottomMenuView.h"
#import "LSYMenuView.h"
@interface LSYBottomMenuView ()
@property (nonatomic,strong) LSYReadProgressView *progressView;
@property (nonatomic,strong) LSYThemeView *themeView;
@property (nonatomic,strong) UIButton *minSpacing;
@property (nonatomic,strong) UIButton *mediuSpacing;
@property (nonatomic,strong) UIButton *maxSpacing;
@property (nonatomic,strong) UIButton *catalog;
@property (nonatomic,strong) UISlider *slider;
@property (nonatomic,strong) UIButton *lastChapter;
@property (nonatomic,strong) UIButton *nextChapter;
@property (nonatomic,strong) UILabel *fontTitleLabel;
@property (nonatomic,strong) UIButton *increaseFont;
@property (nonatomic,strong) UIButton *decreaseFont;
@property (nonatomic,strong) UILabel *fontLabel;
@end
@implementation LSYBottomMenuView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(void)setup{
    [self setBackgroundColor:[UIColor colorWithRed:251 green:251 blue:247 alpha:0.8]];
    [self addSubview:self.slider];
    [self addSubview:self.catalog];
    [self addSubview:self.progressView];
    [self addSubview:self.lastChapter];
    [self addSubview:self.nextChapter];
    [self addSubview:self.fontTitleLabel];
    [self addSubview:self.increaseFont];
    [self addSubview:self.decreaseFont];
    [self addSubview:self.fontLabel];
    [self addSubview:self.themeView];
//    [self addObserver:self forKeyPath:@"readModel.chapter" options:NSKeyValueObservingOptionNew context:NULL];
//    [self addObserver:self forKeyPath:@"readModel.page" options:NSKeyValueObservingOptionNew context:NULL];
    [[LSYReadConfig shareInstance] addObserver:self forKeyPath:@"fontSize" options:NSKeyValueObservingOptionNew context:NULL];
}
-(UIButton *)catalog
{
    if (!_catalog) {
        _catalog = [LSYReadUtilites commonButtonSEL:@selector(showCatalog) target:self];
        [_catalog setImage:[UIImage imageNamed:@"reader_cover"] forState:UIControlStateNormal];
    }
    return _catalog;
}
-(LSYReadProgressView *)progressView
{
    if (!_progressView) {
        _progressView = [[LSYReadProgressView alloc] init];
        _progressView.hidden = YES;
        
    }
    return _progressView;
}
-(UISlider *)slider
{
    if (!_slider) {
        _slider = [[UISlider alloc] init];
        _slider.minimumValue = 0;
        _slider.maximumValue = 100;
        _slider.minimumTrackTintColor = RGB(244, 119, 34);
        _slider.maximumTrackTintColor = RGB(152, 152, 152);
        [_slider setThumbImage:[self thumbImage] forState:UIControlStateNormal];
        [_slider setThumbImage:[self thumbImage] forState:UIControlStateHighlighted];
        [_slider addTarget:self action:@selector(changeMsg:) forControlEvents:UIControlEventValueChanged];
        [_slider addObserver:self forKeyPath:@"highlighted" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:NULL];
        
    }
    return _slider;
}
-(UIButton *)nextChapter
{
    if (!_nextChapter) {
        _nextChapter = [LSYReadUtilites commonButtonSEL:@selector(jumpChapter:) target:self];
        [_nextChapter setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_nextChapter setTitle:@"" forState:UIControlStateNormal];
    }
    return _nextChapter;
}
-(UIButton *)lastChapter
{
    if (!_lastChapter) {
        _lastChapter = [LSYReadUtilites commonButtonSEL:@selector(jumpChapter:) target:self];
        [_lastChapter setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_lastChapter setTitle:@"亮度" forState:UIControlStateNormal];
        
    }
    return _lastChapter;
}
-(UILabel *)fontTitleLabel{
    if (!_fontTitleLabel) {
        _fontTitleLabel = [[UILabel alloc] init];
        _fontTitleLabel.text = @"字号";
        [_fontTitleLabel setTextColor:[UIColor blackColor]];
    }
    return _fontTitleLabel;
}
-(UIButton *)increaseFont
{
    if (!_increaseFont) {
        _increaseFont = [LSYReadUtilites commonButtonSEL:@selector(changeFont:) target:self];
        [_increaseFont setTitle:@"+" forState:UIControlStateNormal];
        [_increaseFont setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [_increaseFont.titleLabel setFont:[UIFont systemFontOfSize:17]];
        _increaseFont.layer.borderWidth = 1;
        _increaseFont.layer.borderColor = [UIColor blackColor].CGColor;
    }
    return _increaseFont;
}
-(UIButton *)decreaseFont
{
    if (!_decreaseFont) {
        _decreaseFont = [LSYReadUtilites commonButtonSEL:@selector(changeFont:) target:self];
        [_decreaseFont setTitle:@"-" forState:UIControlStateNormal];
        [_decreaseFont.titleLabel setFont:[UIFont systemFontOfSize:17]];
        [_decreaseFont setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        _decreaseFont.layer.borderWidth = 1;
        _decreaseFont.layer.borderColor = [UIColor blackColor].CGColor;
    }
    return _decreaseFont;
}
-(UILabel *)fontLabel
{
    if (!_fontLabel) {
        _fontLabel = [[UILabel alloc] init];
        _fontLabel.font = [UIFont systemFontOfSize:14];
        _fontLabel.textColor = [UIColor blackColor];
        _fontLabel.textAlignment = NSTextAlignmentCenter;
        _fontLabel.text = [NSString stringWithFormat:@"%d",(int)[LSYReadConfig shareInstance].fontSize];

    }
    return _fontLabel;
}
-(LSYThemeView *)themeView
{
    if (!_themeView) {
        _themeView = [[LSYThemeView alloc] init];
    }
    return _themeView;
}
#pragma mark - Button Click
-(void)jumpChapter:(UIButton *)sender
{
    if (sender == _nextChapter) {
        if ([self.delegate respondsToSelector:@selector(menuViewJumpChapter:page:)]) {
//            [self.delegate menuViewJumpChapter:(_readModel.chapter == _readModel.chapterCount-1)?_readModel.chapter:_readModel.chapter+1 page:0];
        }
    }
    else{
        if ([self.delegate respondsToSelector:@selector(menuViewJumpChapter:page:)]) {
//            [self.delegate menuViewJumpChapter:_readModel.chapter?_readModel.chapter-1:0 page:0];
        }
        
    }
}
-(void)changeFont:(UIButton *)sender
{

    if (sender == _increaseFont) {

        if (floor([LSYReadConfig shareInstance].fontSize) == floor(MaxFontSize)) {
            return;
        }
        [LSYReadConfig shareInstance].fontSize++;
    }
    else{
        if (floor([LSYReadConfig shareInstance].fontSize) == floor(MinFontSize)){
            return;
        }
        [LSYReadConfig shareInstance].fontSize--;
    }
    self.fontLabel.text = [NSString stringWithFormat:@"%.0f",[LSYReadConfig shareInstance].fontSize];
    if ([self.delegate respondsToSelector:@selector(menuViewFontSize:)]) {
        [self.delegate menuViewFontSize:self];
    }
}
#pragma mark showMsg

-(void)changeMsg:(UISlider *)sender
{
    //获取亮度
    
    [UIScreen mainScreen].brightness;

    //调整亮度
    [[UIScreen mainScreen] setBrightness:sender.value/100.00];
//    NSUInteger page =ceil((_readModel.chapterModel.pageCount-1)*sender.value/100.00);
//    if ([self.delegate respondsToSelector:@selector(menuViewJumpChapter:page:)]) {
//        [self.delegate menuViewJumpChapter:_readModel.chapter page:page];
//    }
    
    
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    
//    if ([keyPath isEqualToString:@"readModel.chapter"] || [keyPath isEqualToString:@"readModel.page"]) {
////        _slider.value = _readModel.page/((float)(_readModel.chapterModel.pageCount-1))*100;
////        [_progressView title:_readModel.chapterModel.title progress:[NSString stringWithFormat:@"%.1f%%",_slider.value]];
//    }
//    else if ([keyPath isEqualToString:@"fontSize"]){
//        _fontLabel.text = [NSString stringWithFormat:@"%d",(int)[LSYReadConfig shareInstance].fontSize];
//    }
//    else{
//        if (_slider.state == UIControlStateNormal) {
//            _progressView.hidden = YES;
//        }
//        else if(_slider.state == UIControlStateHighlighted){
//            _progressView.hidden = NO;
//        }
//    }
    
}
-(UIImage *)thumbImage
{
    CGRect rect = CGRectMake(0, 0, 15,15);
    UIBezierPath *path = [UIBezierPath bezierPath];
    path.lineWidth = 5;
    [path addArcWithCenter:CGPointMake(rect.size.width/2, rect.size.height/2) radius:7.5 startAngle:0 endAngle:2*M_PI clockwise:YES];
    
    UIImage *image = nil;
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    {
        [RGB(244, 119, 34) setFill];
        [path fill];
        image = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    return image;
}
-(void)showCatalog
{
    if ([self.delegate respondsToSelector:@selector(menuViewInvokeCatalog:)]) {
        [self.delegate menuViewInvokeCatalog:self];
    }
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    _slider.frame = CGRectMake(60, 20, ViewSize(self).width-100, 30);
    _lastChapter.frame = CGRectMake(5, 20, 40, 30);
    _nextChapter.frame = CGRectMake(DistanceFromLeftGuiden(_slider), 20, 40, 30);
    _decreaseFont.frame = CGRectMake(10 + 60, DistanceFromTopGuiden(_slider), (ViewSize(self).width-20 - 60)/3, 30);
    _fontLabel.frame = CGRectMake(DistanceFromLeftGuiden(_decreaseFont), DistanceFromTopGuiden(_slider), (ViewSize(self).width-20 - 60)/3,  30);
    _fontTitleLabel.frame = CGRectMake(10, DistanceFromTopGuiden(_slider),  60, 30);
    _increaseFont.frame = CGRectMake(DistanceFromLeftGuiden(_fontLabel) , DistanceFromTopGuiden(_slider),  (ViewSize(self).width-20 - 60)/3, 30);
    _themeView.frame = CGRectMake(0, DistanceFromTopGuiden(_increaseFont)+10, ViewSize(self).width, 40);
    _catalog.frame = CGRectMake(10, DistanceFromTopGuiden(_themeView), 30, 30);
    _progressView.frame = CGRectMake(60, -60, ViewSize(self).width-120, 50);
}
-(void)dealloc
{
    [_slider removeObserver:self forKeyPath:@"highlighted"];
    [self removeObserver:self forKeyPath:@"readModel.chapter"];
    [self removeObserver:self forKeyPath:@"readModel.page"];
    [[LSYReadConfig shareInstance] removeObserver:self forKeyPath:@"fontSize"];
}
@end
@interface LSYThemeView ()
@property (nonatomic,strong) UILabel *themeTitleLabel;
@property (nonatomic,strong) UIView *theme1;
@property (nonatomic,strong) UIView *theme2;
@property (nonatomic,strong) UIView *theme3;
@property (nonatomic,strong) UIView *theme4;
@property (nonatomic,strong) UIView *theme5;
@end
@implementation LSYThemeView
- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}
-(void)setup{
    [self setBackgroundColor:[UIColor clearColor]];
    [self addSubview:self.themeTitleLabel];
    [self addSubview:self.theme1];
    [self addSubview:self.theme2];
    [self addSubview:self.theme3];
    [self addSubview:self.theme4];
    [self addSubview:self.theme5];
}
-(UILabel *)themeTitleLabel{
    if (!_themeTitleLabel) {
        _themeTitleLabel = [[UILabel alloc] init];
        [_themeTitleLabel setText:@"背景"];
        [_themeTitleLabel setTextColor:[UIColor blackColor]];
    }
    return _themeTitleLabel;
}
-(UIView *)theme1
{
    if (!_theme1) {
        _theme1 = [[UIView alloc] init];
        _theme1.backgroundColor = [UIColor redColor];
        [_theme1 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeTheme:)]];
    }
    return _theme1;
}
-(UIView *)theme2
{
    if (!_theme2) {
        _theme2 = [[UIView alloc] init];
        _theme2.backgroundColor = RGB(0, 255, 255);
        [_theme2 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeTheme:)]];
    }
    return _theme2;
}
-(UIView *)theme3
{
    if (!_theme3) {
        _theme3 = [[UIView alloc] init];
        _theme3.backgroundColor = RGB(255, 255, 0);
        [_theme3 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeTheme:)]];
    }
    return _theme3;
}
-(UIView *)theme4
{
    if (!_theme4) {
        _theme4 = [[UIView alloc] init];
        _theme4.backgroundColor = RGB(243, 152, 0);
        [_theme4 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeTheme:)]];
    }
    return _theme4;
}
-(UIView *)theme5
{
    if (!_theme5) {
        _theme5 = [[UIView alloc] init];
        _theme5.backgroundColor = RGB(253, 249, 237);
        [_theme5 addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeTheme:)]];
    }
    return _theme5;
}
-(void)changeTheme:(UITapGestureRecognizer *)tap
{
    [[NSNotificationCenter defaultCenter] postNotificationName:LSYThemeNotification object:tap.view.backgroundColor];
}
-(void)layoutSubviews
{
    CGFloat width = 30;
    CGFloat spacing = (ViewSize(self).width-width*6 - 60)/5;
    _themeTitleLabel.frame = CGRectMake(10, 0, 60, 40);
    _theme1.frame = CGRectMake(spacing + 60, 5, width, width);
    _theme2.frame = CGRectMake(DistanceFromLeftGuiden(_theme1)+spacing, 5, width, width);
    _theme3.frame = CGRectMake(DistanceFromLeftGuiden(_theme2)+spacing, 5, width, width);
    _theme4.frame = CGRectMake(DistanceFromLeftGuiden(_theme3)+spacing, 5, width, width);
    _theme5.frame = CGRectMake(DistanceFromLeftGuiden(_theme4)+spacing, 5, width, width);
    _theme1.layer.cornerRadius =
    _theme2.layer.cornerRadius =
    _theme3.layer.cornerRadius =
    _theme4.layer.cornerRadius =
    _theme5.layer.cornerRadius = width / 2.0;
    _theme1.clipsToBounds =
    _theme2.clipsToBounds =
    _theme3.clipsToBounds =
    _theme4.clipsToBounds =
    _theme5.clipsToBounds = YES;
    _theme1.layer.borderColor =
    _theme2.layer.borderColor =
    _theme3.layer.borderColor =
    _theme4.layer.borderColor =
    _theme5.layer.borderColor = [UIColor blackColor].CGColor;
    _theme1.layer.borderWidth =
    _theme2.layer.borderWidth =
    _theme3.layer.borderWidth =
    _theme4.layer.borderWidth =
    _theme5.layer.borderWidth = 1.0;
    
}
@end
@interface LSYReadProgressView ()
@property (nonatomic,strong) UILabel *label;
@end
@implementation LSYReadProgressView
- (instancetype)init
{
    self = [super init];
    if (self) {
         [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.8]];
        [self addSubview:self.label];
    }
    return self;
}
-(UILabel *)label
{
    if (!_label) {
        _label = [[UILabel alloc] init];
        _label.font = [UIFont systemFontOfSize:[LSYReadConfig shareInstance].fontSize];
        _label.textColor = [UIColor whiteColor];
        _label.textAlignment = NSTextAlignmentCenter;
        _label.numberOfLines = 0;
    }
    return _label;
}
-(void)title:(NSString *)title progress:(NSString *)progress
{
    _label.text = [NSString stringWithFormat:@"%@\n%@",title,progress];
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    _label.frame = self.bounds;
}
@end
