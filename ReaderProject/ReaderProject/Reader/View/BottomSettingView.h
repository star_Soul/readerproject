//
//  BottomSettingView.h
//  ReaderProject
//
//  Created by 李星星 on 2019/9/30.
//  Copyright © 2019 dingye. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BottomSettingView : UIView
@property (retain, nonatomic) IBOutlet UIButton *firstButton;
@property (retain, nonatomic) IBOutlet UIButton *secondButton;
@property (retain, nonatomic) IBOutlet UIButton *thirdButton;

@end

NS_ASSUME_NONNULL_END
