//
//  LSYMenuView.m
//  LSYReader
//
//  Created by Labanotation on 16/6/1.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYMenuView.h"
#import "LSYTopMenuView.h"
#import "LSYBottomMenuView.h"
#define AnimationDelay 0.3f
#define TopViewHeight 64.0f
#define BottomViewHeight 80.0f
#define BottomSViewHeight 200.0f


@interface LSYMenuView ()<LSYMenuViewDelegate>

@end
@implementation LSYMenuView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
-(void)setup
{
    self.backgroundColor = [UIColor clearColor];
    [self addSubview:self.topView];
    [self addSubview:self.bottomSettingView];
    [self addSubview:self.bottomView];
    [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenSelf)]];
}
-(LSYTopMenuView *)topView
{
    if (!_topView) {
        _topView = [[LSYTopMenuView alloc] initWithFrame:CGRectMake(0, -TopViewHeight, ViewSize(self).width,TopViewHeight)];
        _topView.delegate = self;
        _topView.backgroundColor = [UIColor colorWithRed:251 green:251 blue:247 alpha:0.8];
    }
    return _topView;
}
-(LSYBottomMenuView *)bottomView
{
    if (!_bottomView) {
        _bottomView = [[LSYBottomMenuView alloc] init];
        _bottomView.delegate = self;
        _bottomView.hidden = YES;
    }
    return _bottomView;
}
-(BottomSettingView *)bottomSettingView{
    if (!_bottomSettingView) {
        _bottomSettingView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([BottomSettingView class]) owner:self options:nil].lastObject;
        [_bottomSettingView setBackgroundColor:[UIColor colorWithRed:251 green:251 blue:247 alpha:0.8]];
        [_bottomSettingView.thirdButton addTarget:self action:@selector(showBottomView) forControlEvents:UIControlEventTouchUpInside];
        _bottomSettingView.frame =CGRectMake(0, ViewSize(self).height, ViewSize(self).width,BottomViewHeight);
    }
    return _bottomSettingView;
}
-(void)setRecordModel:(LSYRecordModel *)recordModel
{
//    _recordModel = recordModel;
//    _bottomView.readModel = recordModel;
}
#pragma mark - LSYMenuViewDelegate

-(void)menuViewInvokeCatalog:(LSYBottomMenuView *)bottomMenu
{
    if ([self.delegate respondsToSelector:@selector(menuViewInvokeCatalog:)]) {
        [self.delegate menuViewInvokeCatalog:bottomMenu];
    }
}
-(void)menuViewJumpChapter:(NSUInteger)chapter page:(NSUInteger)page
{
    if ([self.delegate respondsToSelector:@selector(menuViewJumpChapter:page:)]) {
        [self.delegate menuViewJumpChapter:chapter page:page];
    }
}
-(void)menuViewFontSize:(LSYBottomMenuView *)bottomMenu
{
    if ([self.delegate respondsToSelector:@selector(menuViewFontSize:)]) {
        [self.delegate menuViewFontSize:bottomMenu];
    }
}
-(void)menuViewMark:(LSYTopMenuView *)topMenu
{
    if ([self.delegate respondsToSelector:@selector(menuViewMark:)]) {
        [self.delegate menuViewMark:topMenu];
    }
}
#pragma mark -
-(void)hiddenSelf
{
    [self hiddenAnimation:YES];
}
#pragma mark - 底部栏显示动画
-(void)showAnimation:(BOOL)animation
{
    self.hidden = NO;
    [UIView animateWithDuration:animation?AnimationDelay:0 animations:^{
        _topView.frame = CGRectMake(0, 0, ViewSize(self).width, TopViewHeight);
        _bottomSettingView.frame = CGRectMake(0, ViewSize(self).height-BottomViewHeight, ViewSize(self).width,BottomViewHeight);
    } completion:^(BOOL finished) {
        
    }];
    if ([self.delegate respondsToSelector:@selector(menuViewDidAppear:)]) {
        [self.delegate menuViewDidAppear:self];
    }
}
-(void)hiddenAnimation:(BOOL)animation
{
    [UIView animateWithDuration:animation?AnimationDelay:0 animations:^{
        _topView.frame = CGRectMake(0, -TopViewHeight, ViewSize(self).width, TopViewHeight);
         _bottomSettingView.frame = CGRectMake(0, ViewSize(self).height, ViewSize(self).width,BottomViewHeight);
        [self hiddenBottomView];
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];
    if ([self.delegate respondsToSelector:@selector(menuViewDidHidden:)]) {
        [self.delegate menuViewDidHidden:self];
    }
}
-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if (@available(iOS 11.0, *)){
        // do nothing.
        // looks the layout of view in iOS 11 is different with iOS 10.
    } else {
        _topView.frame = CGRectMake(0, -TopViewHeight, ViewSize(self).width,TopViewHeight);
        _bottomSettingView.frame = CGRectMake(0, ViewSize(self).height, ViewSize(self).width,BottomViewHeight);
        _bottomView.frame = CGRectMake(0, ViewSize(self).height, ViewSize(self).width,BottomSViewHeight);
        
    }
}
-(void)showBottomView{
    _bottomView.hidden = NO;
    [UIView animateWithDuration:AnimationDelay animations:^{

        _bottomView.frame = CGRectMake(0, ViewSize(self).height-BottomSViewHeight, ViewSize(self).width,BottomSViewHeight);
    } completion:^(BOOL finished) {
        
    }];
}
-(void)hiddenBottomView{
    _bottomView.hidden = YES;
    [UIView animateWithDuration:AnimationDelay animations:^{

        _bottomView.frame = CGRectMake(0, ViewSize(self).height, ViewSize(self).width,BottomSViewHeight);
    } completion:^(BOOL finished) {
        self.hidden = YES;
    }];

}
@end
