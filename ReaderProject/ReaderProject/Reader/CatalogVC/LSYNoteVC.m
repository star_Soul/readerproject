//
//  LSYNoteVC.m
//  LSYReader
//
//  Created by okwei on 16/6/2.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYNoteVC.h"
#import "LSYCatalogViewController.h"
#import "BookListModelManager.h"
static  NSString *noteCell = @"noteCell";
@interface LSYNoteVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tabView;

@property(nonatomic,strong)NSMutableArray *dataSourceArrM;
@end

@implementation LSYNoteVC
- (instancetype)init
{
    self = [super init];
    if (self) {
//        [self addObserver:self forKeyPath:@"readModel.notes" options:NSKeyValueObservingOptionNew context:NULL];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:RGB(253, 249, 237)];
    [self.view addSubview:self.tabView];
    [self requestData];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self requestData];
}
-(void)requestData{
    NSArray *arr = [[BookListModelManager shareManager] fetchAllUsers];
    self.dataSourceArrM = [[NSMutableArray alloc] initWithArray:arr];
    [self.tabView reloadData];
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    [_tabView reloadData];
//    [LSYReadModel updateLocalModel:_readModel url:_readModel.resource]; //本地保存
}
-(UITableView *)tabView
{
    if (!_tabView) {
        _tabView = [[UITableView alloc] init];
        _tabView.delegate = self;
        _tabView.dataSource = self;
        _tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tabView.backgroundColor = RGB(253, 249, 237);
    }
    return _tabView;
}
#pragma mark - UITableView Delagete DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return 1;
    return self.dataSourceArrM.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:noteCell];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:noteCell];
    }
    cell.backgroundColor = RGB(253, 249, 237);
    BookmarkModel *model = self.dataSourceArrM[indexPath.row];
    cell.textLabel.text = model.title;
//    cell.detailTextLabel.text = model.notes[indexPath.row].note;

    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  44.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if ([self.delegate respondsToSelector:@selector(catalog:didSelectChapter:)]) {
        BookmarkModel *model = self.dataSourceArrM[indexPath.row];
//
//        NSInteger chapter = selectedNoteModel.chapter;
//        NSInteger page = [_readModel getPageIndexByOffset:selectedNoteModel.location Chapter:chapter];
        [self.delegate catalog:nil didSelectChapter:model];
//        [self.delegate catalog:nil didSelectChapter:chapter page:page];
    }
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
-(UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}
-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
//    [_readModel.notes removeObjectAtIndex:indexPath.row];
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
}
-(void)dealloc
{
    [self removeObserver:self forKeyPath:@"readModel.notes"];
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _tabView.frame = CGRectMake(0, 0, ViewSize(self.view).width, ViewSize(self.view).height);
}
@end
