//
//  LSYNoteVC.h
//  LSYReader
//
//  Created by okwei on 16/6/2.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClassifyModel.h"
@protocol LSYCatalogViewControllerDelegate;
@interface LSYNoteVC : UIViewController
@property (nonatomic,strong) ClassifyModel *bookClassifyModel;
@property (nonatomic,weak) id<LSYCatalogViewControllerDelegate>delegate;
@end
