//
//  LSYChapterVC.m
//  LSYReader
//
//  Created by okwei on 16/6/2.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYChapterVC.h"
#import "LSYCatalogViewController.h"
#import <SVProgressHUD.h>
#import <MJRefresh.h>
#import "BookListModel.h"
static  NSString *chapterCell = @"chapterCell";
@interface LSYChapterVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong) UITableView *tabView;
@property (nonatomic) NSUInteger readChapter;

@property(nonatomic,strong)NSMutableArray *secondLevelModelArrM;
@property(nonatomic,assign)NSInteger page;
@end

@implementation LSYChapterVC
-(NSMutableArray *)secondLevelModelArrM{
    if (!_secondLevelModelArrM) {
        _secondLevelModelArrM = [[NSMutableArray alloc] init];
    }
    return _secondLevelModelArrM;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view setBackgroundColor:RGB(253, 249, 237)];
    
    [self.view addSubview:self.tabView];
//    [self addObserver:self forKeyPath:@"readModel.record.chapter" options:NSKeyValueObservingOptionNew context:NULL];
    self.page = 1;
    [self requestData];
    self.tabView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self requestData];
    }];
    self.tabView.mj_footer  = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self requestData];
    }];
}
-(void)requestData{
    [SVProgressHUD show];
    [SVProgressHUD dismissWithDelay:3.0];
    
    [HttpManager fuckGetHttpManagerType:@"chapter" Type1:@"book" IDString:[self.bookClassifyModel.IDString intValue] page:self.page limit:20 requestSucced:^(id  _Nonnull responseObject) {
        [self.tabView.mj_header endRefreshing];
        [self.tabView.mj_footer endRefreshing];
        if (self.page == 1) {
            [self.secondLevelModelArrM removeAllObjects];
        }
        [SVProgressHUD dismiss];
        NSArray *arr = responseObject[@"result"];
        for (NSDictionary *dic in arr) {
            BookListModel *model = [[BookListModel alloc] initWithDict:dic];
            model.IDString = dic[@"id"];
            [self.secondLevelModelArrM addObject:model];
        }
        [self.tabView reloadData];
        
    } requestfailure:^(NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    [_tabView reloadData];
}
-(UITableView *)tabView
{
    if (!_tabView) {
        _tabView = [[UITableView alloc] init];
        _tabView.delegate = self;
        _tabView.dataSource = self;
//        _tabView.separatorStyle = UITableViewCellSeparatorStyleNone;
        _tabView.backgroundColor = RGB(253, 249, 237);
    }
    return _tabView;
}
#pragma mark - UITableView Delagete DataSource
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.secondLevelModelArrM.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:chapterCell];
    
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:chapterCell];
    }
    cell.backgroundColor = RGB(253, 249, 237);
    BookListModel *model = self.secondLevelModelArrM[indexPath.row];
    cell.textLabel.text = model.name;
    cell.textLabel.textColor = [UIColor blackColor];
//    if (indexPath.row == _readModel.record.chapter) {
//        [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
//    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  44.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.delegate respondsToSelector:@selector(catalog:didSelectChapter:page:)]) {
        [self.delegate catalog:nil didSelectChapter:indexPath.row page:0];
    }
    BookListModel *model = self.secondLevelModelArrM[indexPath.row];
    if ([self.delegate respondsToSelector:@selector(catalog:didSelectChapter:bookListModel:)]) {
        [self.delegate catalog:nil didSelectChapter:self.bookClassifyModel bookListModel:model];
    }
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    _tabView.frame = CGRectMake(0, 0, ViewSize(self.view).width, ViewSize(self.view).height);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
