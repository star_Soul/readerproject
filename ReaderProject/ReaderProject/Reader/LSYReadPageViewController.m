//
//  LSYReadPageViewController.m
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYReadPageViewController.h"
#import "LSYReadViewController.h"
#import "LSYChapterModel.h"
#import "LSYMenuView.h"
#import "LSYCatalogViewController.h"
#import "UIImage+ImageEffects.h"
#import "LSYNoteModel.h"
#import "LSYMarkModel.h"
#import <objc/runtime.h>
#import "BookChapterModel.h"
#import "NumModel.h"
#import "BookmarkModel.h"
#import "BookListModelManager.h"
#import <UMAnalytics/MobClick.h>
#import "RecommendModelManager.h"
#define AnimationDelay 0.3
#define catalogViewWidth 0.7
@interface LSYReadPageViewController ()<UIPageViewControllerDelegate,UIPageViewControllerDataSource,LSYMenuViewDelegate,UIGestureRecognizerDelegate,LSYCatalogViewControllerDelegate,LSYReadViewControllerDelegate>
{
    NSUInteger _chapter;    //当前显示的章节
    NSUInteger _page;       //当前显示的页数
    NSUInteger _chapterChange;  //将要变化的章节
    NSUInteger _pageChange;     //将要变化的页数
    BOOL _isTransition;     //是否开始翻页
}
@property (nonatomic,strong) UIPageViewController *pageViewController;
@property (nonatomic,getter=isShowBar) BOOL showBar; //是否显示状态栏
@property (nonatomic,strong) LSYMenuView *menuView; //菜单栏
@property (nonatomic,strong) LSYCatalogViewController *catalogVC;   //侧边栏
@property (nonatomic,strong) UIView * catalogView;  //侧边栏背景
@property (nonatomic,strong) LSYReadViewController *readView;   //当前阅读视图

@property(nonatomic,assign)NSInteger currentPage;



@property(nonatomic,strong)NumModel *currentNumModel;

@property(nonatomic,strong)BookChapterModel *currentBookChapterModel;
@property(nonatomic,strong)BookChapterModel *currentLastBookChapterModel;
@property(nonatomic,strong)BookChapterModel *currentNextBookChapterModel;
@end

@implementation LSYReadPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addChildViewController:self.pageViewController];
    self.view.backgroundColor = RGB(253, 249, 237);
//    _chapter = _model.record.chapter;;
//    _page = _model.record.page;
    [self.view addGestureRecognizer:({
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showToolMenu)];
        tap.delegate = self;
        tap;
    })];
    [self.view addSubview:self.menuView];
    [self.menuView.bottomSettingView.firstButton addTarget:self action:@selector(showLeft) forControlEvents:UIControlEventTouchUpInside];
    [self.menuView.bottomSettingView.secondButton addTarget:self action:@selector(lightMode:) forControlEvents:UIControlEventTouchUpInside];
    self.menuView.topView.titleLabel.text = self.currentClassifyModel.name;
    [self addChildViewController:self.catalogVC];
    [self.view addSubview:self.catalogView];
    [self.catalogView addSubview:self.catalogVC.view];
    //添加笔记
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addNotes:) name:LSYNoteNotification object:nil];
    
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    self.currentPage = 1;
    [self requestData];
//    self.menuView.hidden = YES;
    self.menuView.topView.hidden =
    self.menuView.bottomSettingView.hidden = YES;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self showToolMenu];
        [self.menuView hiddenAnimation:YES];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.menuView.topView.hidden =
            self.menuView.bottomSettingView.hidden = NO;
        });

    });
    if (self.currentClassifyModel.isRecommendModel) {
        self.currentClassifyModel.isread = @"1";
        [[RecommendModelManager shareManager] updateWithModel:self.currentClassifyModel IDString:self.currentClassifyModel.IDString name:self.currentClassifyModel.name isRead:@"1"];
    }
    [MobClick setAutoPageEnabled:YES];
    if ([UserInfoManager manager].loginUser.authorization) {
        
            [HttpManager fuckAddBookListHttpManagerRequestURL:[NSString stringWithFormat:@"http://www.ljsb.top:5000/common/isread?analysis=%@&",[UserInfoManager manager].loginUser.authorization] dic:@{@"bookId":self.currentClassifyModel.IDString} Succed:^(id  _Nonnull responseObject) {
                
            } requestfailure:^(NSError * _Nonnull error) {
                
            }];
    }
    
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.hidden = NO;
}
-(void)requestData{
    [HttpManager fuckGeSearchtHttpManagerRequestBookId:self.currentClassifyModel.IDString num:[NSString stringWithFormat:@"%ld",(long)[self.currentBookListModel.num integerValue]] Succed:^(id  _Nonnull responseObject) {
        self.currentBookChapterModel = [[BookChapterModel alloc] initWithDict:responseObject[@"result"]];
        if ([self.currentBookListModel.num integerValue] == 1) {
            self.currentBookChapterModel.content = [NSString stringWithFormat:@"\n%@\n\n\n%@",self.currentBookChapterModel.title,self.currentBookChapterModel.content];
        }else{
            self.currentBookChapterModel.content = [NSString stringWithFormat:@"\n%@\n\n\n%@",self.currentBookChapterModel.title,self.currentBookChapterModel.content];
        }
        self.currentBookChapterModel.page = 0;
        
        self.currentBookChapterModel.pageArr = [[NSArray alloc] initWithArray:[self paginateWithBounds:CGRectMake(LeftSpacing,TopSpacing, self.view.frame.size.width-LeftSpacing-RightSpacing, self.view.frame.size.height-TopSpacing-BottomSpacing) contentString:self.currentBookChapterModel.content]];
        self.currentBookChapterModel.num = [self.currentBookListModel.num integerValue];
        [_pageViewController setViewControllers:@[[self readViewWithChapter:1 page:self.currentBookChapterModel.page]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
        [self requestNextModel];
        [self requestLastModel];
    } requestfailure:^(NSError * _Nonnull error) {
        
    }];
}
-(void)requestNextModel{
    [HttpManager fuckGeSearchtHttpManagerRequestBookId:self.currentClassifyModel.IDString num:[NSString stringWithFormat:@"%ld",self.currentBookChapterModel.num + 1] Succed:^(id  _Nonnull responseObject) {
        NumModel *currentNumModel = [[NumModel alloc] initWithDict:responseObject[@"result"]];
        if (currentNumModel.content.length > 0) {
            self.currentNextBookChapterModel = [[BookChapterModel alloc] init];
            self.currentNextBookChapterModel.content = currentNumModel.content;
            if (self.currentBookChapterModel.num + 1 == 1) {
                self.currentNextBookChapterModel.content = [NSString stringWithFormat:@"\n%@\n\n\n%@",currentNumModel.title,currentNumModel.content];
            }else{
                self.currentNextBookChapterModel.content = [NSString stringWithFormat:@"\n%@\n\n\n%@",currentNumModel.title,currentNumModel.content];
            }
            self.currentNextBookChapterModel.page = 0;
            self.currentNextBookChapterModel.pageArr = [[NSArray alloc] initWithArray:[self paginateWithBounds:CGRectMake(LeftSpacing,TopSpacing, self.view.frame.size.width-LeftSpacing-RightSpacing, self.view.frame.size.height-TopSpacing-BottomSpacing) contentString:self.currentNextBookChapterModel.content]];
            self.currentNextBookChapterModel.num = self.currentBookChapterModel.num + 1;
        }
        
    } requestfailure:^(NSError * _Nonnull error) {
        
    }];
}
-(void)requestLastModel{
    [HttpManager fuckGeSearchtHttpManagerRequestBookId:self.currentClassifyModel.IDString num:[NSString stringWithFormat:@"%ld",self.currentBookChapterModel.num - 1] Succed:^(id  _Nonnull responseObject) {
        NumModel *currentNumModel = [[NumModel alloc] initWithDict:responseObject[@"result"]];
        if (currentNumModel.content.length > 0) {
            self.currentLastBookChapterModel = [[BookChapterModel alloc] init];
            self.currentLastBookChapterModel.content = currentNumModel.content;
            if (self.currentBookChapterModel.num - 1 == 1) {
                self.currentLastBookChapterModel.content = [NSString stringWithFormat:@"\n%@\n\n\n%@",currentNumModel.title,currentNumModel.content];
            }else{
                self.currentLastBookChapterModel.content = [NSString stringWithFormat:@"\n%@\n\n\n%@",currentNumModel.title,currentNumModel.content];
            }
            self.currentLastBookChapterModel.page = 0;
            self.currentLastBookChapterModel.pageArr = [[NSArray alloc] initWithArray:[self paginateWithBounds:CGRectMake(LeftSpacing,TopSpacing, self.view.frame.size.width-LeftSpacing-RightSpacing, self.view.frame.size.height-TopSpacing-BottomSpacing) contentString:self.currentLastBookChapterModel.content]];
            self.currentLastBookChapterModel.num = self.currentBookChapterModel.num  - 1;
        }
        
    } requestfailure:^(NSError * _Nonnull error) {
        
    }];
}
-(void)showLeft{
    [_menuView hiddenAnimation:NO];
    [self catalogShowState:YES];
}
-(void)lightMode:(UIButton *)sender{
    sender.selected = !sender.isSelected;
    if (sender.selected) {
        [[NSNotificationCenter defaultCenter] postNotificationName:LSYThemeNotification object:UIColorFromRGB(0x171813)];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:LSYThemeNotification object:RGB(253, 249, 237)];
    }
}
#pragma mark - 添加书签
-(void)addNotes:(NSNotification *)no
{
//    LSYNoteModel *noteModel = no.object;
//    LSYChapterModel *chapterModel = _model.record.chapterModel;
//    noteModel.location += [chapterModel.pageArray[_model.record.page] integerValue];
//    noteModel.chapter = _model.record.chapter;
//    noteModel.recordModel = [_model.record copy];
//    [[_model mutableArrayValueForKey:@"notes"] addObject:noteModel];    //这样写才能KVO数组变化
//    [LSYReadUtilites showAlertTitle:nil content:@"保存笔记成功"];
    
}

-(BOOL)prefersStatusBarHidden
{
    return !_showBar;
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
#pragma mark - 显示底部工具栏
-(void)showToolMenu
{
    BOOL isMarked = FALSE;
    
//    LSYRecordModel *recordModel = _model.record; //上次看的记录
//    LSYChapterModel *chapterModel = recordModel.chapterModel;
    
//    NSUInteger startIndex = [chapterModel.pageArray[recordModel.page] integerValue];
    
//    NSUInteger endIndex = NSUIntegerMax;
//    NSUInteger chapter = recordModel.chapter;
    
//    if (recordModel.page < chapterModel.pageCount - 1) {
//        endIndex = [chapterModel.pageArray[recordModel.page + 1] integerValue];
//    }
    
//    for (int i = 0; i < _model.marks.count; i++) {
//        LSYMarkModel *markModel = _model.marks[i];
//        if (markModel.chapter == chapter && markModel.location >= startIndex && markModel.location < endIndex) {
//            isMarked = YES;
//            break;
//        }
//    }
    
    isMarked?(_menuView.topView.state=1): (_menuView.topView.state=0);
    [self.menuView showAnimation:YES];
}

#pragma mark - init 底部工具栏
-(LSYMenuView *)menuView
{
    if (!_menuView) {
        _menuView = [[LSYMenuView alloc] init];
        _menuView.hidden = YES;
        _menuView.delegate = self;
        
//        _menuView.recordModel = _model.record;
    }
    return _menuView;
}

#pragma mark - 分页控制器
-(UIPageViewController *)pageViewController
{
    if (!_pageViewController) {
        _pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
        _pageViewController.delegate = self;
        _pageViewController.dataSource = self;
        [self.view addSubview:_pageViewController.view];
    }
    return _pageViewController;
}
#pragma mark - 左侧目录
-(LSYCatalogViewController *)catalogVC
{
    if (!_catalogVC) {
        _catalogVC = [[LSYCatalogViewController alloc] init];
        _catalogVC.bookClassifyModel = self.currentClassifyModel;
        _catalogVC.catalogDelegate = self;
    }
    return _catalogVC;
}
-(UIView *)catalogView
{
    if (!_catalogView) {
        _catalogView = [[UIView alloc] init];
        _catalogView.backgroundColor = [UIColor clearColor];
        _catalogView.hidden = YES;
        [_catalogView addGestureRecognizer:({
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenCatalog)];
            tap.delegate = self;
            tap;
        })];
    }
    return _catalogView;
}
#pragma mark - 左侧栏点击响应
#pragma mark - CatalogViewController Delegate
-(void)catalog:(LSYCatalogViewController *)catalog didSelectChapter:(NSUInteger)chapter page:(NSUInteger)page
{
     [_pageViewController setViewControllers:@[[self readViewWithChapter:chapter page:page]] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
    [self updateReadModelWithChapter:chapter page:page];
    [self hiddenCatalog];
    
}
-(void)catalog:(LSYCatalogViewController *)catalog didSelectChapter:(ClassifyModel *)classfiyModel bookListModel:(BookListModel *)bookListModel{
    self.currentBookListModel = bookListModel;
    self.currentPage = 1;
    [self requestData];
}
-(void)catalog:(LSYCatalogViewController *)catalog didSelectChapter:(BookmarkModel *)bookMarkModel{
    self.currentClassifyModel.IDString = bookMarkModel.IDString;
    self.currentBookListModel.num = [NSString stringWithFormat:@"%ld",bookMarkModel.num];
    self.currentPage = bookMarkModel.page;
    [self requestData];
    [self hiddenCatalog];
}
#pragma mark -  UIGestureRecognizer Delegate
//解决TabView与Tap手势冲突
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]) {
        return NO;
    }
    return  YES;
}
#pragma mark - Privite Method
-(void)catalogShowState:(BOOL)show
{
    show?({
        _catalogView.hidden = !show;
        [UIView animateWithDuration:AnimationDelay animations:^{
            _catalogView.frame = CGRectMake(0, 0,ViewSize(self.view).width, ViewSize(self.view).height);
            
        } completion:^(BOOL finished) {
            [_catalogView insertSubview:[[UIImageView alloc] initWithImage:[self blurredSnapshot]] atIndex:0];
            
        }];
    }):({
        if ([_catalogView.subviews.firstObject isKindOfClass:[UIImageView class]]) {
            [_catalogView.subviews.firstObject removeFromSuperview];
        }
        [UIView animateWithDuration:AnimationDelay animations:^{
             _catalogView.frame = CGRectMake(-ViewSize(self.view).width, 0, ViewSize(self.view).width, ViewSize(self.view).height);
        } completion:^(BOOL finished) {
            _catalogView.hidden = !show;
        }];
    });
}
-(void)hiddenCatalog
{
    [self catalogShowState:NO];
}
- (UIImage *)blurredSnapshot {
    
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(CGRectGetWidth(self.view.frame)*catalogViewWidth, CGRectGetHeight(self.view.frame)), NO, 1.0f);
    [self.view drawViewHierarchyInRect:CGRectMake(0, 0, CGRectGetWidth(self.view.frame)*catalogViewWidth, CGRectGetHeight(self.view.frame)) afterScreenUpdates:NO];
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIImage *blurredSnapshotImage = [snapshotImage applyLightEffect];
    UIGraphicsEndImageContext();
    return blurredSnapshotImage;
}
#pragma mark - Menu View Delegate
-(void)menuViewDidHidden:(LSYMenuView *)menu
{
     _showBar = NO;
    [self setNeedsStatusBarAppearanceUpdate];
}
-(void)menuViewDidAppear:(LSYMenuView *)menu
{
    _showBar = YES;
    [self setNeedsStatusBarAppearanceUpdate];
    
}
-(void)menuViewInvokeCatalog:(LSYBottomMenuView *)bottomMenu
{
    [_menuView hiddenAnimation:NO];
    [self catalogShowState:YES];
    
}

-(void)menuViewJumpChapter:(NSUInteger)chapter page:(NSUInteger)page
{
    [_pageViewController setViewControllers:@[[self readViewWithChapter:chapter page:page]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self updateReadModelWithChapter:chapter page:page];
}
-(void)menuViewFontSize:(LSYBottomMenuView *)bottomMenu
{

//    [_model.record.chapterModel updateFont];
    
//    for (int i = 0; i < _model.chapters.count; i++) {
//        [_model.chapters[i] updateFont];
//    }
    
    [_pageViewController setViewControllers:@[[self readViewWithChapter:1 page:(self.currentBookChapterModel.page>self.currentBookChapterModel.pageArr.count-1)?self.currentBookChapterModel.pageArr.count-1:self.currentBookChapterModel.page]] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    [self updateReadModelWithChapter:1 page:(self.currentBookChapterModel.page>self.currentBookChapterModel.pageArr.count-1)?self.currentBookChapterModel.pageArr.count-1:self.currentBookChapterModel.page];
    self.currentBookChapterModel.pageArr = [[NSArray alloc] initWithArray:[self paginateWithBounds:CGRectMake(LeftSpacing,TopSpacing, self.view.frame.size.width-LeftSpacing-RightSpacing, self.view.frame.size.height-TopSpacing-BottomSpacing) contentString:self.currentBookChapterModel.content]];
    
}
#pragma mark - 添加书签
-(void)menuViewMark:(LSYTopMenuView *)topMenu
{

    BOOL isMarked = FALSE;
//    ClassifyModel  *bookModel = self.currentClassifyModel;
//    BookListModel *listModel = self.currentBookListModel;
    BookmarkModel *model = [[BookmarkModel alloc] init];
    model.IDString = self.currentClassifyModel.IDString;
    model.num = self.currentBookChapterModel.num;
    model.chapterId = self.currentBookChapterModel.chapterId;
    model.content = self.currentBookChapterModel.content;
    model.created = self.currentBookChapterModel.created;
    model.number = self.currentBookChapterModel.number;
    model.state = self.currentBookChapterModel.state;
    model.title = self.currentBookChapterModel.title;
    model.updated = self.currentBookChapterModel.updated;
    model.words = self.currentBookChapterModel.words;
    model.page = self.currentBookChapterModel.page;
    model.pageArr = self.currentBookChapterModel.pageArr;
    [[BookListModelManager shareManager] insertDataWithModel:model];
//    LSYRecordModel *recordModel = _model.record;
//    LSYChapterModel *chapterModel = recordModel.chapterModel;
    
//    NSUInteger startIndex = [chapterModel.pageArray[recordModel.page] integerValue];
    
//    NSUInteger endIndex = NSUIntegerMax;
//
//    if (recordModel.page < chapterModel.pageCount - 1) {
//        endIndex = [chapterModel.pageArray[recordModel.page + 1] integerValue];
//    }
    
//    NSMutableArray *markedArray = [[NSMutableArray alloc] init];
//    for (int i = 0; i < _model.marks.count; i++) {
//        LSYMarkModel *markModel = _model.marks[i];
//        if (markModel.location >= startIndex && markModel.location <= endIndex) {
//            isMarked = YES;
//            [markedArray addObject:markModel];
//        }
//    }
    
//    if (isMarked) {
//        [[_model mutableArrayValueForKey:@"marks"] removeObjectsInArray:markedArray];
//    } else {
//        LSYRecordModel *recordModel = _model.record;
//        LSYMarkModel *markModel = [[LSYMarkModel alloc] init];
//        markModel.date = [NSDate date];
//        markModel.location = [recordModel.chapterModel.pageArray[recordModel.page] integerValue];
//        markModel.length = 0;
//        markModel.chapter = recordModel.chapter;
//        markModel.recordModel = [_model.record copy];
//        [[_model mutableArrayValueForKey:@"marks"] addObject:markModel];
//        //[_model.marksRecord setObject:markModel forKey:key];
//    }
    
    _menuView.topView.state = !isMarked;
}
#pragma mark -左侧栏点击响应
#pragma mark - Create Read View Controller

-(LSYReadViewController *)readViewWithChapter:(NSUInteger)chapter page:(NSUInteger)page{

    
//    if (_model.record.chapter != chapter) {
//        [self updateReadModelWithChapter:chapter page:page];
//        [_model.record.chapterModel updateFont];
//    }
    _readView = [[LSYReadViewController alloc] init];
//    _readView.recordModel = _model.record;
//    _readView.content = self.currentBookChapterModel.content;
    if ([self paginateWithBounds:CGRectMake(LeftSpacing,TopSpacing, self.view.frame.size.width-LeftSpacing-RightSpacing, self.view.frame.size.height-TopSpacing-BottomSpacing) page:self.currentBookChapterModel.page].length <= 0) {
        _readView.content = self.currentBookChapterModel.content;
    }else{
        _readView.content = [self paginateWithBounds:CGRectMake(LeftSpacing,TopSpacing, self.view.frame.size.width-LeftSpacing-RightSpacing, self.view.frame.size.height-TopSpacing-BottomSpacing) page:self.currentBookChapterModel.page];
    }
    
    _readView.delegate = self;
    NSLog(@"_readGreate");
    
    return _readView;
}
#pragma mark - 翻页效果
-(void)updateReadModelWithChapter:(NSUInteger)chapter page:(NSUInteger)page
{
    _chapter = chapter;
    _page = page;
}
#pragma mark - Read View Controller Delegate
-(void)readViewEndEdit:(LSYReadViewController *)readView
{
    for (UIGestureRecognizer *ges in self.pageViewController.view.gestureRecognizers) {
        if ([ges isKindOfClass:[UIPanGestureRecognizer class]]) {
            ges.enabled = YES;
            break;
        }
    }
}
-(void)readViewEditeding:(LSYReadViewController *)readView
{
    for (UIGestureRecognizer *ges in self.pageViewController.view.gestureRecognizers) {
        if ([ges isKindOfClass:[UIPanGestureRecognizer class]]) {
            ges.enabled = NO;
            break;
        }
    }
}
//向上翻页
#pragma mark -PageViewController DataSource
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    if (self.currentBookChapterModel.page == 0) {
        if (self.currentLastBookChapterModel.content.length > 0) {
            self.currentNextBookChapterModel = self.currentBookChapterModel;
            self.currentBookChapterModel = self.currentLastBookChapterModel;
            [self requestLastModel];
            self.currentBookChapterModel.page = self.currentBookChapterModel.pageArr.count - 1;
        }
//        [self requestLastNum];
       return [self readViewWithChapter:_chapterChange page:self.currentBookChapterModel.page];
    }
    self.currentBookChapterModel.page = self.currentBookChapterModel.page - 1;
    return [self readViewWithChapter:_chapterChange page:self.currentBookChapterModel.page];
    
}
//向后翻页
#pragma mark - 翻页效果
- (nullable UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    if (self.currentBookChapterModel.page + 1 >= self.currentBookChapterModel.pageArr.count) {
//        [self requestNextNum];
        if (self.currentNextBookChapterModel.content.length > 0) {
            self.currentLastBookChapterModel = self.currentBookChapterModel;
            self.currentBookChapterModel = self.currentNextBookChapterModel;
            self.currentBookChapterModel.page = 0;
            [self requestNextModel];
        }
       return [self readViewWithChapter:_chapterChange page:self.currentBookChapterModel.page];
    }
    
        self.currentBookChapterModel.page = self.currentBookChapterModel.page + 1;
        return [self readViewWithChapter:_chapterChange page:self.currentBookChapterModel.page];
}
#pragma mark -PageViewController Delegate
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    if (!completed) {
        LSYReadViewController *readView = previousViewControllers.firstObject;
        _readView = readView;
//        _page = readView.recordModel.page;
//        _chapter = readView.recordModel.chapter;
    }
    else{
        [self updateReadModelWithChapter:_chapter page:_page];
    }
}
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers
{
    _chapter = _chapterChange;
    _page = _pageChange;
}
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];

    
    if (@available(iOS 11.0, *)){
        _pageViewController.view.frame = self.view.frame;
        _menuView.frame = self.view.frame;
        _catalogView.frame = CGRectMake(-ViewSize(self.view).width, 0, ViewSize(self.view).width, ViewSize(self.view).height);
        _catalogVC.view.frame = CGRectMake(0, 0, ViewSize(self.view).width * catalogViewWidth, ViewSize(self.view).height);
    } else {
        _pageViewController.view.frame = self.view.frame;
        _menuView.frame = self.view.frame;
        _catalogView.frame = CGRectMake(-ViewSize(self.view).width, 0, ViewSize(self.view).width, ViewSize(self.view).height);
        _catalogVC.view.frame = CGRectMake(0, 0, ViewSize(self.view).width * catalogViewWidth, ViewSize(self.view).height);
    }
    
    [_catalogVC reload];
}

-(NSString *)paginateWithBounds:(CGRect)bounds page:(NSInteger)page
{
    NSUInteger local = [self.currentBookChapterModel.pageArr[page] integerValue];
        NSUInteger length;
        if (page<self.currentBookChapterModel.pageArr.count-1) {
    //        length = _pages[index+1] -_pages[index];
            length=  [self.currentBookChapterModel.pageArr[page+1] integerValue] - [self.currentBookChapterModel.pageArr[page] integerValue];
        }
        else{
    //        length = _content.length-_pages[index];
            length = self.currentBookChapterModel.content.length - [self.currentBookChapterModel.pageArr[page] integerValue];
        }
        return [self.currentBookChapterModel.content substringWithRange:NSMakeRange(local, length)];
}
-(NSString *)stringOfPage:(NSUInteger)index
{
//    NSUInteger local = _pages[index];
    NSUInteger local = [self.currentBookChapterModel.pageArr[index] integerValue];
    NSUInteger length;
    if (index<self.currentBookChapterModel.pageArr.count-1) {
//        length = _pages[index+1] -_pages[index];
        length=  [self.currentBookChapterModel.pageArr[index+1] integerValue] - [self.currentBookChapterModel.pageArr[index] integerValue];
    }
    else{
//        length = _content.length-_pages[index];
        length = self.currentBookChapterModel.content.length - [self.currentBookChapterModel.pageArr[index] integerValue];
    }
    return [self.currentBookChapterModel.content substringWithRange:NSMakeRange(local, length)];
}
-(NSArray *)paginateWithBounds:(CGRect)bounds contentString:(NSString *)contentString
{
//    _pages.clear();
    NSMutableArray *_pageArray = [[NSMutableArray alloc] init];
    [_pageArray removeAllObjects];
    NSMutableAttributedString *attrString = [[NSMutableAttributedString  alloc] initWithString:contentString];
    NSDictionary *attribute = [LSYReadParser parserAttribute:[LSYReadConfig shareInstance]];
    [attrString setAttributes:attribute range:NSMakeRange(0, attrString.length)];
    
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString((__bridge CFAttributedStringRef) attrString);
    CGPathRef path = CGPathCreateWithRect(bounds, NULL);
    int currentOffset = 0;
    int currentInnerOffset = 0;
    BOOL hasMorePages = YES;
    // 防止死循环，如果在同一个位置获取CTFrame超过2次，则跳出循环
    int preventDeadLoopSign = currentOffset;
    int samePlaceRepeatCount = 0;
    
    while (hasMorePages) {
        if (preventDeadLoopSign == currentOffset) {
            
            ++samePlaceRepeatCount;
            
        } else {
            
            samePlaceRepeatCount = 0;
        }
        
        if (samePlaceRepeatCount > 1) {
            // 退出循环前检查一下最后一页是否已经加上
//            if (_pages.size() == 0) {
//
//                _pages.push_back(currentOffset);
//
//            }
            if (_pageArray.count == 0) {
                [_pageArray addObject:@(currentOffset)];
            }
            else {
                
//                NSUInteger lastOffset = _pages.back();
                NSUInteger lastOffset = [[_pageArray lastObject] integerValue];
                
                if (lastOffset != currentOffset) {
                    
//                    _pages.push_back(currentOffset);
                    [_pageArray addObject:@(currentOffset)];
                }
            }
            break;
        }
        
//        _pages.push_back(currentOffset);
        [_pageArray addObject:@(currentOffset)];
        
        CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(currentInnerOffset, 0), path, NULL);
        CFRange range = CTFrameGetVisibleStringRange(frame);
        
        if ((range.location + range.length) != attrString.length) {
            
            currentOffset += range.length;
            currentInnerOffset += range.length;
            
        } else {
            // 已经分完，提示跳出循环
            hasMorePages = NO;
        }
        if (frame) CFRelease(frame);
    }
    
    CGPathRelease(path);
    CFRelease(frameSetter);
    _pageArray = [[NSMutableArray alloc] initWithArray:[self justPageCount:_pageArray contentString:contentString] ];
    return _pageArray;
}
-(NSMutableArray *)justPageCount:(NSMutableArray *)pageArr contentString:(NSString *)contentString{
    NSInteger lastObj = [[pageArr lastObject] integerValue];
    if (lastObj > contentString.length) {
        [pageArr removeLastObject];
        pageArr = [[NSMutableArray alloc]initWithArray:[self justPageCount:pageArr contentString:contentString]];
    }else if (lastObj < contentString.length){
        [pageArr addObject: [NSString stringWithFormat:@"%ld",contentString.length]];
        return pageArr;
    }
    return pageArr;
}
@end
