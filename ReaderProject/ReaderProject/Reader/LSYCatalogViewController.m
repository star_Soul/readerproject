//
//  LSYCatalogViewController.m
//  LSYReader
//
//  Created by okwei on 16/6/2.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import "LSYCatalogViewController.h"
#import "LSYChapterVC.h"
#import "LSYNoteVC.h"
#import "LSYMarkVC.h"
#import <UMAnalytics/MobClick.h>
@interface LSYCatalogViewController ()<LSYViewPagerVCDelegate,LSYViewPagerVCDataSource,LSYCatalogViewControllerDelegate>
@property (nonatomic,strong) NSArray *titleArray;
@property (nonatomic,strong) NSArray *VCArray;
@end

@implementation LSYCatalogViewController
-(NSArray *)titleArray{
    if (!_titleArray) {
        _titleArray = [[NSArray alloc] initWithObjects:@"目录",nil];
//        _titleArray = [[NSArray alloc] initWithObjects:@"目录",@"笔记",@"书签", nil];
    }
    return _titleArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB(253, 249, 237);
    _VCArray = [[NSArray alloc] initWithObjects:({
        LSYChapterVC *chapterVC = [[LSYChapterVC alloc]init];
        chapterVC.bookClassifyModel = _bookClassifyModel;
        chapterVC.delegate = self;
        chapterVC;
    }),({
        LSYNoteVC *noteVC = [[LSYNoteVC alloc] init];
        noteVC.bookClassifyModel = _bookClassifyModel;
        noteVC.delegate = self;
        noteVC;
    }),({
        LSYMarkVC *markVC =[[LSYMarkVC alloc] init];
        markVC.bookClassifyModel = _bookClassifyModel;
        markVC.delegate = self;
        markVC;
    }), nil];
    self.forbidGesture = YES;
    self.delegate = self;
    self.dataSource = self;
    [MobClick setAutoPageEnabled:YES];
}

-(NSInteger)numberOfViewControllersInViewPager:(LSYViewPagerVC *)viewPager
{
    return self.titleArray.count;
}
-(UIViewController *)viewPager:(LSYViewPagerVC *)viewPager indexOfViewControllers:(NSInteger)index
{
    return _VCArray[index];
}
-(NSString *)viewPager:(LSYViewPagerVC *)viewPager titleWithIndexOfViewControllers:(NSInteger)index
{
    return self.titleArray[index];
}
-(CGFloat)heightForTitleOfViewPager:(LSYViewPagerVC *)viewPager
{
    return 40.0f;
}
-(void)catalog:(LSYCatalogViewController *)catalog didSelectChapter:(NSUInteger)chapter page:(NSUInteger)page
{
    if ([self.catalogDelegate respondsToSelector:@selector(catalog:didSelectChapter:page:)]) {
        [self.catalogDelegate catalog:self didSelectChapter:chapter page:page];
    }
    
}
-(void)catalog:(LSYCatalogViewController *)catalog didSelectChapter:(ClassifyModel *)classfiyModel bookListModel:(BookListModel *)bookListModel{
    if ([self.catalogDelegate respondsToSelector:@selector(catalog:didSelectChapter:bookListModel:)]) {
        [self.catalogDelegate catalog:self didSelectChapter:classfiyModel bookListModel:bookListModel];
    }
}
-(void)catalog:(LSYCatalogViewController *)catalog didSelectChapter:(BookmarkModel *)bookMarkModel{
    if ([ self.catalogDelegate respondsToSelector:@selector(catalog:didSelectChapter:)]) {
        [self.catalogDelegate catalog:self didSelectChapter:bookMarkModel];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
