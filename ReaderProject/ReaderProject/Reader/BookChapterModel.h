//
//  BookChapterModel.h
//  ReaderProject
//
//  Created by 李星星 on 2019/9/25.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BookChapterModel : BaseModel
@property(nonatomic,strong)NSString *chapterId;//"chapterId": 45537,
@property(nonatomic,strong)NSString *content;//"content": "放学铃声响起，静海市云龙高中，数千名学生欢呼着涌出校园，显然比起枯燥 的校园生活来，校外的生活更精彩一些！\t\r可是高三七班的教室中，却一片愁云惨淡！十多名 男男女女正围绕在一名名身穿粉色运动服的少女身前……\t\r少女有着一头乌黑的长发，扎成马尾绑在脑后，一张精美的瓜子脸，嘴唇微薄，鼻子微挺，眼睛不是太大，但却非常的亮，那是一种只有在婴儿眼中才能够看到的清澈光芒——明亮而纯净！\t\r她坐在自己的座位上，看不出身高啊，但发育的很好，即便外面套着一件粉红色的运动装，依旧难以掩盖那挺拔的胸脯……\t\r她叫谭笑笑，是高三七班的班长，之所以放学之后还有这么多人留在这里，是因为班上的一名叫赵孟的同学保护班里的一名叫莉莉的女同学得罪了校园霸王之一的张龙，如今张龙正带着人在校门口守候，扬言要给他一点教训。\t\r莉莉是谭笑笑的好友，赵孟是为了她才得罪了张龙，而自己本身又身为一班之长，自然不可能眼睁睁的看着自己的同学被欺负！\t\r可惜高三七班并不是铁板一块，至少很多人并不愿意为了一个赵孟得罪有着市政委老爸的张龙，也因此放学之后，只有十几个留下来！\t\r“笑笑，罗军那混蛋说今天他老爹生曰，来不了……”\t\r“笑笑，李凯那家伙说他老妈感冒了，正赶去医院，也来不了……”\t\r“笑笑，李东明手机关机……”\t\r……\t\r一个又一个男生站在谭笑笑的身前，满脸愤慨的说道！\t\r他们在学校也有些地位，平曰里也结交了一些其他班的学生，以前在外面打架斗殴的时候都是一起上，谁知道这一次遇上了麻烦，竟然一个人都找不到……\t\r“没用的，张龙已经放出话来，谁帮我们，就是和他为敌，他们是不会得罪张龙的……”看到叽叽喳喳的众人，谭笑笑轻声叹息了一声……\t\r因为张龙老爸的原因，就算是学校里的老师，也不愿得罪他，况且这件事本来就是学生之间的事情，要是告诉了老师，以后他们也没脸在学校继续混下去了！\t\r“那我们怎么办？难道真的要把赵孟交出去不成？”又一名男生不满的说道！\t\r这人叫花小蝶，说实话，他心里倒是希望将赵孟交出去，张龙何许人也，校园霸王之一，老爸是市政委，没有人愿意得罪他？要不是自己在追求谭笑笑，他才不愿趟这趟浑水！\t\r其实不仅是他，除了赵孟外，班上的其他男生同样是看在谭笑笑的面子上才留下来的！",
@property(nonatomic,strong)NSString *created;//"created": 1489230093,
@property(nonatomic,strong)NSString *number;//"number": null,
@property(nonatomic,strong)NSString *state;//"state": 0,
@property(nonatomic,strong)NSString *title;//"title": "第一章 我可以帮你们",
@property(nonatomic,strong)NSString *updated;//"updated": 1489230093,
@property(nonatomic,strong)NSString *words;//"words": 927

@property(nonatomic,assign)NSInteger page;
@property(nonatomic,assign)NSInteger num;
@property(nonatomic,strong)NSArray  *pageArr;
@end

NS_ASSUME_NONNULL_END
