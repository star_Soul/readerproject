//
//  LSYReadPageViewController.h
//  LSYReader
//
//  Created by Labanotation on 16/5/30.
//  Copyright © 2016年 okwei. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "LSYReadModel.h"
#import "BookListModel.h"
#import "ClassifyModel.h"
@interface LSYReadPageViewController : UIViewController
//@property (nonatomic,strong) NSURL *resourceURL;
//@property (nonatomic,strong) LSYReadModel *model;
@property(nonatomic,strong)BookListModel *currentBookListModel; //章节
@property(nonatomic,strong)ClassifyModel *currentClassifyModel; //小说详情
//+(void)loadURL:(NSURL *)url;
@end
