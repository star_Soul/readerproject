//
//  bookInfoListController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "bookInfoListController.h"
#import "BookListModel.h"
#import <MJRefresh/MJRefresh.h>
#import <SVProgressHUD.h>
#import "LSYReadPageViewController.h"
#import <UMAnalytics/MobClick.h>
@interface bookInfoListController ()<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property(nonatomic,assign)NSInteger page;

@property(nonatomic,strong)NSMutableArray *dataSourceArrM;
@end

@implementation bookInfoListController
-(NSMutableArray *)dataSourceArrM{
    if (!_dataSourceArrM) {
        _dataSourceArrM = [ [NSMutableArray alloc] init];
    }
    return _dataSourceArrM;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = self.currentClassfiyModel.name;
    self.listTableView.backgroundColor = [UIColor whiteColor];
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.showsVerticalScrollIndicator = NO;
    //    self.listTableView.separatorColor = color_bg;
    self.listTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.listTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    self.listTableView.rowHeight = 50;
    self.listTableView.bounces = YES;
    self.listTableView.tableFooterView = [UIView new];
    self.listTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        self.page = 1;
        [self requestData];
    }];
    self.listTableView.mj_footer  = [MJRefreshAutoFooter footerWithRefreshingBlock:^{
        self.page ++;
        [self requestData];
    }];
    self.page = 1;
    [self requestData];
    [MobClick setAutoPageEnabled:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)requestData{
    [SVProgressHUD show];
    [SVProgressHUD dismissWithDelay:3.0];
    
    [HttpManager fuckGetHttpManagerType:@"chapter" Type1:@"book" IDString:[self.currentClassfiyModel.IDString intValue] page:self.page limit:20 requestSucced:^(id  _Nonnull responseObject) {
        [self.listTableView.mj_header endRefreshing];
        [self.listTableView.mj_footer endRefreshing];
        if (self.page == 1) {
            [self.dataSourceArrM removeAllObjects];
        }
        [SVProgressHUD dismiss];
        NSArray *arr = responseObject[@"result"];
        for (NSDictionary *dic in arr) {
            BookListModel *model = [[BookListModel alloc] initWithDict:dic];
            model.IDString = dic[@"id"];
            [self.dataSourceArrM addObject:model];
        }
        [self.listTableView reloadData];
        
    } requestfailure:^(NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    LSYReadPageViewController *pageView = [[LSYReadPageViewController alloc] init];
    BookListModel *model = self.dataSourceArrM[indexPath.row];
    pageView.currentBookListModel = model;
    pageView.currentClassifyModel = self.currentClassfiyModel;
//    NSURL *fileURL = [[NSBundle mainBundle] URLForResource:@"mdjyml"withExtension:@"txt"];
//    pageView.resourceURL = fileURL;    //文件位置
//
//    dispatch_async(dispatch_get_global_queue(0, 0), ^{
//
//        pageView.model = [LSYReadModel getLocalModelWithURL:fileURL];
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//
//            [self presentViewController:pageView animated:YES completion:nil];
//        });
//    });
    pageView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:pageView animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSourceArrM.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    BookListModel *model = self.dataSourceArrM[indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@",model.name];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

@end
