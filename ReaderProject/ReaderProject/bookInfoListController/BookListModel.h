//
//  BookListModel.h
//  ReaderProject
//
//  Created by 李星星 on 2019/9/23.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BaseModel.h"
#import "ClassifyModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface BookListModel : BaseModel
@property(nonatomic,strong)NSString *created;//"created": 1489230093,
@property(nonatomic,strong)NSString *IDString;//"id": 45537,
@property(nonatomic,strong)NSString *name;//"name": "第一章 我可以帮你们",
@property(nonatomic,strong)NSString *novelId;//"novelId": 11,
@property(nonatomic,strong)NSString *number;//"number": null,
@property(nonatomic,strong)NSString *state;//"state": 1,
@property(nonatomic,strong)NSString *updated;//"updated": 1489230093,
@property(nonatomic,strong)NSString *words;//"words": null
@property(nonatomic,strong)NSString *num;//"num": 1

@property(nonatomic,strong)ClassifyModel *currentClassifyModel;
@end

NS_ASSUME_NONNULL_END
