//
//  bookInfoListController.h
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "BaseViewController.h"
#import "ClassifyModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface bookInfoListController : BaseViewController
@property(nonatomic,strong)ClassifyModel *currentClassfiyModel;
@end

NS_ASSUME_NONNULL_END
