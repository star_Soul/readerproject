//
//  ChooseSexController.m
//  ReaderProject
//
//  Created by 李星星 on 2019/9/29.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "ChooseSexController.h"
#import "ClassifyModel.h"
#import "RecommendModelManager.h"
#import "BaseTabbarController.h"
#import <UMAnalytics/MobClick.h>
@interface ChooseSexController ()
@property (retain, nonatomic) IBOutlet UIButton *manButton;
@property (retain, nonatomic) IBOutlet UIButton *femanButton;
@property (retain, nonatomic) IBOutlet UIButton *otherButton;
@property(nonatomic,strong)NSString *gender;
@end

@implementation ChooseSexController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"选择性别";
    [self  manButtonClick:self.manButton];
    [MobClick setAutoPageEnabled:YES];
}
- (IBAction)manButtonClick:(UIButton *)sender {
    self.manButton.selected = YES;
    self.femanButton.selected = NO;
    self.otherButton.selected = NO;
    self.gender = @"1";
//    [self changeSex:@"1"];
}
- (IBAction)femanButtonClick:(UIButton *)sender {
    self.manButton.selected = NO;
    self.femanButton.selected = YES;
    self.otherButton.selected = NO;
    self.gender = @"0";
//    [self changeSex:@"0"];
}
- (IBAction)otherButtonClick:(UIButton *)sender {
    self.manButton.selected = NO;
    self.femanButton.selected = NO;
    self.otherButton.selected = YES;
}
-(IBAction)changeSex{
    [HttpManager fuckAddBookListHttpManagerRequestURL:@"http://www.ljsb.top:5000/front/recommend?" dic:@{@"gender":self.gender} Succed:^(id  _Nonnull responseObject) {
        NSArray *arr = responseObject[@"result"];
        for (NSDictionary *dic in arr) {
            ClassifyModel *model = [[ClassifyModel alloc] initWithDict:dic];
            model.IDString = dic[@"id"];
            model.isRecommendModel = @"yesdfd";
            [[RecommendModelManager shareManager] insertDataWithModel:model];
        }
        [UIApplication sharedApplication].delegate.window.rootViewController = [[BaseTabbarController alloc] init];
    } requestfailure:^(NSError * _Nonnull error) {
        
    }];
//    if ([UserInfoManager manager].loginUser.authorization) {
//        [HttpManager fuckAddBookListHttpManagerRequestURL:[NSString stringWithFormat:@"http://www.ljsb.top:5000/common/resetgender?analysis=%@&",[UserInfoManager manager].loginUser.authorization] dic:@{@"analysis":[UserInfoManager manager].loginUser.authorization,@"gender":gender} Succed:^(id  _Nonnull responseObject) {
//
//        } requestfailure:^(NSError * _Nonnull error) {
//
//        }];
//    }else{
//        [MBProgressHUD showError:@"请先登录"];
//    }
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)dealloc {
    [_manButton release];
    [_femanButton release];
    [_otherButton release];
    [super dealloc];
}
@end
