//
//  readHistoryListController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "readHistoryListController.h"
#import "SearchTableViewCell.h"
#import <Masonry/Masonry.h>
#import "InfoController.h"
#import "ReadHistoryManager.h"
#import <SVProgressHUD.h>
#import "BaseTabbarController.h"
#import <UMAnalytics/MobClick.h>
@interface readHistoryListController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property(nonatomic,strong)UIButton *rightButton;

@property(nonatomic,strong)NSMutableArray *dataSourceArrM;
@end

@implementation readHistoryListController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"浏览历史";
    self.listTableView.backgroundColor = [UIColor whiteColor];
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.showsVerticalScrollIndicator = NO;
    //    self.listTableView.separatorColor = color_bg;
    self.listTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SearchTableViewCell class])];
    self.listTableView.rowHeight = 50;
    self.listTableView.bounces = YES;
    self.listTableView.tableFooterView = [UIView new];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn addTarget:self action:@selector(rightBarButtonItemClick:) forControlEvents:UIControlEventTouchUpInside];

    [rightBtn setTitle:@"清空" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.rightButton = rightBtn;
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        
    }];
    [self requestData];
    [MobClick setAutoPageEnabled:YES];
}
-(void)requestData{
    [SVProgressHUD show];
    self.dataSourceArrM = [[NSMutableArray alloc] initWithArray:[[ReadHistoryManager shareManager] fetchAllUsers]];
    if (self.dataSourceArrM.count == 0) {
        self.listTableView.hidden = YES;
    }else{
        [self.listTableView reloadData];
    }
    [SVProgressHUD dismiss];
}
-(void)rightBarButtonItemClick:(UIButton *)sender{
    
    [[ReadHistoryManager shareManager] deleteData];
    [self requestData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSourceArrM.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchTableViewCell class])];
    cell.currentClassifyModel = self.dataSourceArrM[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    InfoController *ctrl = [[InfoController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    ctrl.currentClassfiyModel = self.dataSourceArrM[indexPath.row];
    [self.navigationController pushViewController:ctrl animated:YES];
}
- (IBAction)findButtonClick:(UIButton *)sender {
    BaseTabbarController *ctrl = (BaseTabbarController *)[UIApplication sharedApplication].delegate.window.rootViewController;
    ctrl.selectedIndex = 1;
    
}

@end
