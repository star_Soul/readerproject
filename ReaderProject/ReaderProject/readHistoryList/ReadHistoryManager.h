//
//  ReadHistoryManager.h
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ClassifyModel.h"
#import "FMDatabase.h"//第三方开源库,封装了ios中sqlite3数据的操作
NS_ASSUME_NONNULL_BEGIN

@interface ReadHistoryManager : NSObject
@property(nonatomic,strong)FMDatabase *dataBase;
@property(nonatomic,strong)NSString *dbPath;
//开发中一般，暴露一个类方法，来使其他对象通过此方法获取到单例
+ (ReadHistoryManager *)shareManager;
//插入一条数据
- (void)insertDataWithModel:(ClassifyModel *)model;
//根据name和IDString来删除一条数据
-(void)deleteDataWithName:(NSString *)name IDString:(NSString *)IDString;
-(void)deleteData;
//获取全部数据
- (NSArray *)fetchAllUsers;
-(BOOL)isDataExistsWithName:(NSString *)name IDString:(NSString *)IDString;
@end

NS_ASSUME_NONNULL_END
