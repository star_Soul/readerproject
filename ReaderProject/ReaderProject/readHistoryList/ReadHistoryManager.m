//
//  ReadHistoryManager.m
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "ReadHistoryManager.h"

@implementation ReadHistoryManager
{
    //FMDatabase 没有考虑到多个线程同时操作数据库的情况
    NSLock *_lock;
    
}
//被static修饰，只会初始化一次， 并且会一直持有初始化的值
static ReadHistoryManager *manager = nil;
+ (ReadHistoryManager *)shareManager{
    //防止多个线程同时调用shareManager方法
    //@synchronized 一次只能允许一个线程访问关键字中的代码
    @synchronized(self){
        if (manager == nil) {
            manager = [[self alloc] init];
        }
    }
    return manager;
}
//重写init 进行一些必要的初始化操作
- (id)init{
    self = [super init];
    if (self) {
        _lock = [[NSLock alloc] init];
        //拼接数据库的路径
        //NSHomeDirectory() 程序的沙盒根目录
        _dbPath = [NSHomeDirectory() stringByAppendingFormat:@"/Documents/FatWeightModelInfo.db"];
        NSLog(@"%@",_dbPath);
        //创建fmdb对象，并将路径传递过去
        _dataBase = [[FMDatabase alloc] initWithPath:_dbPath];
        //open dbPath中没有数据库文件，会创建并打开数据库；有文件，则直接打开
        //[_dataBase close];
        if ([_dataBase open]) {
            //创建表 blob 二进制对象类型
            NSString *createSql = @"create table if not exists FatWeightModelInfo(id integer primary key autoincrement,author text,authorId  text,cover text,created text,enabled text,extras text,IDString text,label text,tag text,name text,state text,summary text,updated text,words text,countchapter text)";
            //executeUpdate 增、删、改，创建表 的sql全用此方法
            //返回值为执行的结果 yes no
            BOOL isSuccessed  = [_dataBase executeUpdate:createSql];
            if (!isSuccessed) {
                //打印失败的信息
                NSLog(@"create error:%@",_dataBase.lastErrorMessage);
            }
        }
    }
    return self;
}
-(void)insertDataWithModel:(ClassifyModel *)model{
    [_lock lock];
    //    UIImage *image = model.headImage;
    //将图片转化成NSData
    //UIImagePNGRepresentation 将png格式的图片转化成NSData
    //    NSData *data = UIImagePNGRepresentation(image);
    //UIImageJPEGRepresentation(<#UIImage *image#>, CGFloat compressionQuality)
    //sqlite中 用?作为占位符
    NSString *insertSql = @"insert into FatWeightModelInfo(author,authorId,cover,created,enabled,extras,IDString,label,tag,name,state,summary,updated,words,countchapter) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    //executeUpdate 要求后面跟的参数必须是NSObject类型,否则会抛出EXC_BAD_ACCESS错误,fmdb会在将数据写入之前对数据进行自动转化
    BOOL isSuccessd =[_dataBase executeUpdate:insertSql,model.author,model.authorId,model.cover,model.created,model.enabled,model.extras,model.IDString,model.label,model.tag,model.name,model.state,model.summary,model.updated,model.words,model.countchapter];
    if (!isSuccessd) {
        NSLog(@"insert error:%@",_dataBase.lastErrorMessage);
    }
    [_lock unlock];
}
-(void)deleteDataWithName:(NSString *)name IDString:(NSString *)IDString{
    [_lock lock];
    NSString *deleteSql =[NSString stringWithFormat:@"delete from FatWeightModelInfo where name = '%@' and IDString = '%@'",name,IDString] ;
    BOOL isSuccessed =[_dataBase executeUpdate:deleteSql,time];
    if (!isSuccessed) {
        NSLog(@"delete error:%@",_dataBase.lastErrorMessage);
    }
    [_lock unlock];
}
-(void)deleteData{
    [_lock lock];
    NSString *deleteSql =[NSString stringWithFormat:@"delete from FatWeightModelInfo"] ;
    BOOL isSuccessed =[_dataBase executeUpdate:deleteSql,time];
    if (!isSuccessed) {
        NSLog(@"delete error:%@",_dataBase.lastErrorMessage);
    }
    [_lock unlock];
}
-(NSArray *)fetchAllUsers{
    [_lock lock];
    NSString *seleteSql =[NSString stringWithFormat:@"select * from FatWeightModelInfo"];
    //查询的sql语句用executeQuery
    //FMResultSet 查询结果的集合类
    FMResultSet *set =[_dataBase executeQuery:seleteSql];
    //next 从第一条数据开始，一直能取到最后一条，能取到当前的数据返回YES
    NSMutableArray *array = [ [NSMutableArray alloc] init];
    while ([set next]) {
        //根据字段名称，获取字段的值
        ClassifyModel *model = [[ClassifyModel alloc] init];
        //stringForColumn 获取字符串的值
        model.author = [set stringForColumn:@"author"];
        model.authorId = [set stringForColumn:@"authorId"] ;
        model.cover = [set stringForColumn:@"cover"];
        model.created = [set stringForColumn:@"created"];
        model.enabled = [set stringForColumn:@"enabled"];
        model.extras = [set stringForColumn:@"extras"];
        model.IDString = [set stringForColumn:@"IDString"];
        model.label = [set stringForColumn:@"label"];
        model.tag = [set stringForColumn:@"tag"];
        model.name = [set stringForColumn:@"name"];
        model.state = [set stringForColumn:@"state"];
        model.summary = [set stringForColumn:@"summary"];
        model.updated = [set stringForColumn:@"updated"];
        model.words = [set stringForColumn:@"words"];
        model.countchapter = [set stringForColumn:@"countchapter"];
        
        //获取NSData值
        [array addObject:model];
    }
    [_lock unlock];
    return array;
}
-(BOOL)isDataExistsWithName:(NSString *)name IDString:(NSString *)IDString{
    NSString *selectSql = @"select * from FatWeightModelInfo where name = '%@' and IDString = '%@'";
    FMResultSet *set = [_dataBase executeQuery:selectSql,name,IDString];
    return [set next];
}
@end
