//
//  FeedBackController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "FeedBackController.h"
#import "LxGridViewFlowLayout.h"
#import "TZTestCell.h"
#import "TZImagePickerHelper.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <UMAnalytics/MobClick.h>
#define WeakPointer(weakSelf) __weak __typeof(&*self)weakSelf = self
#define MAX_COUNT 4
@interface FeedBackController ()<UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate,UITextFieldDelegate>{
    CGFloat _itemWH;
    CGFloat _margin;
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UILabel *placeTextLabel;
@property (weak, nonatomic) IBOutlet UITextField *phoneTF;


@property (nonatomic, strong) LxGridViewFlowLayout *layout;
@property (nonatomic, strong) TZImagePickerHelper *helper;
@property (nonatomic, strong) NSMutableArray *imagesURL;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *colllectionNLCHight;

@property (weak, nonatomic) IBOutlet UIButton *commitButton;
@property (weak, nonatomic) IBOutlet UIButton *callServiceButton;
@property (nonatomic, strong) NSMutableString *imageUrlCouts;
@property (nonatomic, assign) NSInteger imageCout;

@end

@implementation FeedBackController
-(NSMutableArray *)imagesURL{
    if (!_imagesURL) {
        _imagesURL = [ [NSMutableArray alloc] init];
    }
    return _imagesURL;
}
- (LxGridViewFlowLayout *)layout
{
    if (!_layout) {
        _layout = [[LxGridViewFlowLayout alloc] init];
        _margin = 10;
        _itemWH = ((SCREEN_WIDTH-30-20) - 2*_margin-2*_margin)/4;
        _layout.itemSize = CGSizeMake(_itemWH, _itemWH);
        _layout.minimumInteritemSpacing = _margin;
        _layout.minimumLineSpacing = _margin;
    }
    return _layout;
}
- (TZImagePickerHelper *)helper
{
    if (!_helper) {
        _helper = [[TZImagePickerHelper alloc] init];
        WeakPointer(weakSelf);
        _helper.finish = ^(NSArray *array){
            [weakSelf.imagesURL addObjectsFromArray:array];
            weakSelf.layout.itemCount = weakSelf.imagesURL.count;
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf.collectionView reloadData];
                if (weakSelf.imagesURL.count%4 == 0) {
                    weakSelf.colllectionNLCHight.constant = _collectionView.contentSize.height+((SCREEN_WIDTH-30-20) - 2*_margin-2*_margin)/4+30;
                }else{
                    weakSelf.colllectionNLCHight.constant = _collectionView.contentSize.height+30;
                }
            });
        };
    }
    return _helper;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"意见反馈";
    self.navigationController.navigationBarHidden = NO;
    self.textView.delegate = self;
    self.phoneTF.delegate = self;
        UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [rightButton addTarget:self action:@selector(commitBtnClick) forControlEvents:UIControlEventTouchUpInside];
        [rightButton setTitle:@"提交" forState:UIControlStateNormal];
        rightButton.frame = CGRectMake(0, 0, 44, 44);
        [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightButton];
    
    self.scrollView.delegate = self;
    self.collectionView.collectionViewLayout = self.layout;
    _collectionView.alwaysBounceVertical = YES;
    _collectionView.backgroundColor = [UIColor whiteColor];
    _collectionView.contentInset = UIEdgeInsetsMake(10, 10, 10, 10);
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.bounces  = NO;
    [_collectionView registerClass:[TZTestCell class] forCellWithReuseIdentifier:@"TZTestCell"];
    self.colllectionNLCHight.constant = ((SCREEN_WIDTH-30-20) - 2*_margin-2*_margin)/4+30;
    self.commitButton.layer.cornerRadius = 5;
    self.commitButton.clipsToBounds = YES;
    [self.commitButton addTarget:self action:@selector(commitBtnClick) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view from its nib.
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    [back setImage:[UIImage imageNamed:@"返回-1"] forState:UIControlStateNormal];
    [back setTitle:@"用户反馈" forState:UIControlStateNormal];
    [back setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    back.titleLabel.adjustsFontSizeToFitWidth = YES;
    back.frame = CGRectMake(0, 0, 100, 30);
    back.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    back.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    back.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    [back addTarget:self action:@selector(backBtnClick) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:back];
    
    NSMutableAttributedString *stringM = [[NSMutableAttributedString  alloc] initWithString:@"客服电话：15695946686"];
    [stringM addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 4)];
    [stringM addAttribute:NSForegroundColorAttributeName value:DEFAULT_T_ORANGE range:NSMakeRange(5, stringM.length - 5)];
    [self.callServiceButton setAttributedTitle:stringM forState:UIControlStateNormal];
    [self.callServiceButton setBackgroundColor:[UIColor clearColor]];
    [self.callServiceButton addTarget:self action:@selector(opTelePhone) forControlEvents:UIControlEventTouchUpInside];
    [MobClick setAutoPageEnabled:YES];
}
-(void)backBtnClick{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)opTelePhone
{
    NSString *callPhone = [NSString stringWithFormat:@"telprompt://%@", @"15695946686"];
    /// 解决iOS10及其以上系统弹出拨号框延迟的问题
    /// 方案一
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        /// 10及其以上系统
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callPhone] options:@{} completionHandler:nil];
    } else {
        /// 10以下系统
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:callPhone]];
    }
}
- (void)commitBtnClick {
    
    [self.textView endEditing:YES];
    [self.phoneTF endEditing:YES];
    if (self.textView.text.length == 0) {
        [MBProgressHUD showError:NSLocalizedString(@"请输入问题描述", @"")];return;
    }
    if (self.imagesURL.count == 0) {
        [MBProgressHUD showError:NSLocalizedString(@"请最少选择一张图片", @"")];return;
    }
    if (self.phoneTF.text.length != 11) {
        [MBProgressHUD showError:NSLocalizedString(@"手机号不正确", @"")];return;
    }
    
    [SVProgressHUD show];
    
    // 将第一个网络请求任务添加到组中
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [SVProgressHUD dismiss];
        [MBProgressHUD showError:@"反馈成功"];
        [self.navigationController popViewControllerAnimated:YES];
    });
    self.imageCout = 0;
    self.imageUrlCouts = [NSMutableString string];
    for (int i=0; i<self.imagesURL.count;i++) {
        NSMutableArray *imagesArrM = [ [NSMutableArray alloc] init];
        NSMutableArray *filesArrM = [ [NSMutableArray alloc] init];
        NSMutableArray *namesArrM = [ [NSMutableArray alloc] init];
        [imagesArrM addObject:[UIImage imageNamed:self.imagesURL[i]]];
        [filesArrM addObject:@"file"];
        [namesArrM addObject:@"fileName"];
        
        
    }
}

#pragma mark -- UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.imagesURL.count + 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TZTestCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TZTestCell" forIndexPath:indexPath];
    cell.videoImageView.hidden = YES;
    if (indexPath.row == self.imagesURL.count)
    {
        cell.imageView.image = [UIImage imageNamed:@"touxiang-xiangji"];
        cell.deleteBtn.hidden = YES;
    }
    else
    {
        cell.imageView.image = [UIImage imageWithContentsOfFile:self.imagesURL[indexPath.row]];
        cell.deleteBtn.hidden = NO;
    }
    cell.deleteBtn.tag = indexPath.row;
    [cell.deleteBtn addTarget:self action:@selector(deleteBtnClik:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == self.imagesURL.count)
    {
        [self.textView endEditing:YES];
        [self.phoneTF endEditing:YES];
        if ((MAX_COUNT - self.imagesURL.count) <= 0) {
            [MBProgressHUD showError:NSLocalizedString(@"最多添加4张", @"")];
            return;
        }
        [self.helper showImagePickerControllerWithMaxCount:(MAX_COUNT - self.imagesURL.count) WithViewController:self];
    }
    else
    {
        // preview photos or video / 预览照片或者视频
    }
}
#pragma mark -- 内部方法
/**
 删除
 
 @param sender sender
 */
- (void)deleteBtnClik:(UIButton *)sender
{
    [self.imagesURL removeObjectAtIndex:sender.tag];
    self.layout.itemCount = self.imagesURL.count;
    
    [self.collectionView performBatchUpdates:^{
        NSIndexPath *indexPath = [NSIndexPath indexPathForItem:sender.tag inSection:0];
        [self.collectionView deleteItemsAtIndexPaths:@[indexPath]];
    } completion:^(BOOL finished) {
        [self.collectionView reloadData];
        self.colllectionNLCHight.constant = _collectionView.contentSize.height+30;
    }];
}
#pragma mark - scrollViewDelegate
-(void)textViewDidBeginEditing:(UITextView *)textView{
    self.placeTextLabel.hidden = YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if (textView.text.length == 0) {
        self.placeTextLabel.hidden = NO;
    }else{
        self.placeTextLabel.hidden = YES;
    }
}
#pragma mark -UITextView Delegate Method
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    if([text isEqualToString:@"\n"]){
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.returnKeyType == UIReturnKeyDone) {
        return [textField resignFirstResponder];
    }
    return YES;
}

@end

