//
//  BookListLastCollectionViewCell.h
//  ReaderProject
//
//  Created by 李星星 on 2019/10/14.
//  Copyright © 2019 dingye. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BookListLastCollectionViewCell : UICollectionViewCell

@end

NS_ASSUME_NONNULL_END
