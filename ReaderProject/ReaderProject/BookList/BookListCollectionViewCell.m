//
//  BookListCollectionViewCell.m
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "BookListCollectionViewCell.h"
#import <UIImageView+WebCache.h>
@implementation BookListCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setCurrentClassfiyModel:(ClassifyModel *)currentClassfiyModel{
    _currentClassfiyModel = currentClassfiyModel;
    self.nameLabel.text = currentClassfiyModel.name;
    [self.bookImageView sd_setImageWithURL:[NSURL URLWithString:currentClassfiyModel.cover]];
    if ([self.currentClassfiyModel.isread isEqualToString:@"1"]) {
        self.isReadLabel.text = @"已读";
    }else{
        self.isReadLabel.text = @"未读";
    }
}
@end
