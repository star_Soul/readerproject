//
//  BookListCollectionViewCell.h
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClassifyModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface BookListCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bookImageView;
@property (weak, nonatomic) IBOutlet UIImageView *isSelectedImageView;
@property (weak, nonatomic) IBOutlet UILabel *isReadLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;

@property(nonatomic,strong)ClassifyModel *currentClassfiyModel;
@end

NS_ASSUME_NONNULL_END
