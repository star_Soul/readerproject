//
//  BookListController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "BookListController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "BookListCollectionViewCell.h"
#import <Masonry/Masonry.h>
#import "SearchViewController.h"
#import "InfoController.h"
#import "ClassifyModel.h"
#import <SVProgressHUD.h>
#import "BaseTabbarController.h"
#import "RecommendModelManager.h"
#import "BookListLastCollectionViewCell.h"
#import <UMAnalytics/MobClick.h>
@interface BookListController ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
@property(nonatomic,assign)BOOL isEdit;
@property (strong, nonatomic) UICollectionView *BookListCollectionView;

@property (nonatomic, strong) NSMutableArray *secondLevelModelArrM;

@property (nonatomic, strong) NSMutableArray *selectedModelArrM;

//@property(nonatomic,strong)NSMutableIndexSet *muIndexSet;

@property(nonatomic,strong)UIButton *rightButton;

@property(nonatomic,strong)UIButton *leftButton;
@end

@implementation BookListController
-(UICollectionView *)BookListCollectionView{
    if (!_BookListCollectionView) {
        CHTCollectionViewWaterfallLayout *flowLayout = [[CHTCollectionViewWaterfallLayout alloc] init];
        flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 0, 10);
        flowLayout.minimumColumnSpacing = 10;
        flowLayout.minimumInteritemSpacing = 10;
        flowLayout.columnCount = 3;
        //        self.flowLayout = flowLayout;
        _BookListCollectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        [_BookListCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([BookListCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([BookListCollectionViewCell class])];
        [_BookListCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([BookListLastCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([BookListLastCollectionViewCell class])];
        _BookListCollectionView.delegate = self;
        _BookListCollectionView.dataSource = self;
        _BookListCollectionView.scrollEnabled = YES;
        _BookListCollectionView.layer.cornerRadius = 5;
        _BookListCollectionView.clipsToBounds = YES;
        
        _BookListCollectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
        _BookListCollectionView.backgroundColor = [UIColor whiteColor];
        _BookListCollectionView.showsVerticalScrollIndicator = NO;
        _BookListCollectionView.showsHorizontalScrollIndicator = NO;
        //注册Cell
        
        _BookListCollectionView.layer.shadowOffset = CGSizeMake(5, 5);
        _BookListCollectionView.layer.shadowColor = [UIColor blackColor].CGColor;
        _BookListCollectionView.layer.shadowRadius = 5;
        _BookListCollectionView.layer.shadowOpacity = 0;
    }
    return _BookListCollectionView;
}
-(NSMutableArray *)secondLevelModelArrM{
    if (!_secondLevelModelArrM) {
        _secondLevelModelArrM = [ [NSMutableArray alloc] init];
    }
    return _secondLevelModelArrM;
}
-(NSMutableArray *)selectedModelArrM{
    if (!_selectedModelArrM) {
        _selectedModelArrM = [ [NSMutableArray alloc] init];
    }
    return _selectedModelArrM;
}
//-(NSMutableIndexSet *)muIndexSet{
//    if (!_muIndexSet) {
//        _muIndexSet = [[NSMutableIndexSet alloc] init];
//    }
//    return _muIndexSet;
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"我的书架";
    self.isEdit = NO;
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn addTarget:self action:@selector(rightBarButtonItemClick:) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.rightButton = rightBtn;
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        
    }];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftBtn addTarget:self action:@selector(leftBarButtonItemClick:) forControlEvents:UIControlEventTouchUpInside];
    [leftBtn setImage:[UIImage imageNamed:@"编辑"] forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:leftBtn];
    self.leftButton = leftBtn;
    [self.leftButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        
    }];
    
    [self.view addSubview:self.BookListCollectionView];
    [self.BookListCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.top.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    [MobClick setAutoPageEnabled:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self requestData];
}
-(void)rightBarButtonItemClick:(UIButton *)sender{

    if ([sender.titleLabel.text isEqualToString:@"删除"]) {
        UIAlertController *ctrl = [UIAlertController alertControllerWithTitle:@"主人，您还没看呢，确认舍弃书宝宝吗？" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"我再想想" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [action1 setValue:DEFAULT_T_ORANGE forKey:@"titleTextColor"];
        
        UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"狠心离去" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [SVProgressHUD show];
            
//            NSArray *arr = [self.selectedModelArrM objectsAtIndexes:self.muIndexSet];
            NSMutableArray *booklistModelArrM = [[NSMutableArray alloc] init];
//            NSMutableArray *recommandModelArrM = [[NSMutableArray alloc] init];
            for (ClassifyModel *model in self.selectedModelArrM) {
                if (model.isRecommendModel) {
                    [[RecommendModelManager shareManager] deleteDataWithName:model.name IDString:model.IDString];
                }else{
                    [booklistModelArrM addObject:model];
                }
            }
            
            NSMutableString *stringM = [[NSMutableString alloc] init];
            for (int i = 0; i < booklistModelArrM.count ; i ++) {
                ClassifyModel *model = booklistModelArrM[i];
                if (i == self.selectedModelArrM.count - 1) {
                    [stringM appendFormat:@"%@", [NSString stringWithFormat:@"%@",model.IDString]];
                }else{
                   [stringM appendFormat:@"%@", [NSString stringWithFormat:@"%@,",model.IDString]];
                }
            }
            if ([UserInfoManager manager].loginUser.authorization) {
                 [HttpManager fuckCancelBooktHttpManagerRequestDic:@{@"bookIds":stringM} Succed:^(id  _Nonnull responseObject) {
                                if ([[NSString filterDataToString:responseObject[@"retCode"]] isEqual:@"200"]) {
                //                    [self.secondLevelModelArrM removeObjectsAtIndexes:self.muIndexSet];
                //                    [self.BookListCollectionView reloadData];
                                    [self requestData];
                                    [self.selectedModelArrM removeAllObjects];
                                }else{
                                    [MBProgressHUD showError:responseObject[@"msg"]];
                                }
                                [SVProgressHUD dismiss];
                            } requestfailure:^(NSError * _Nonnull error) {
                                
                            }];
            }else{
                [self requestData];
                [self.selectedModelArrM removeAllObjects];
                [SVProgressHUD dismiss];
            }
           
        }];
        [action2 setValue:[UIColor blackColor] forKey:@"titleTextColor"];
        [ctrl addAction:action1];
        [ctrl addAction:action2];
        
        [self presentViewController:ctrl animated:YES completion:nil];
    }else{
        SearchViewController *ctrl = [[SearchViewController alloc] init];
        ctrl.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:ctrl animated:YES];
    }
}
-(void)leftBarButtonItemClick:(UIButton *)sender{
    sender.selected = !sender.isSelected;
    if (sender.selected) {
        self.isEdit = YES;
        [self.leftButton setTitle:@"取消" forState:UIControlStateNormal];
        [self.rightButton setTitle:@"删除" forState:UIControlStateNormal];
        [self.rightButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [self.leftButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    }else{
        self.isEdit = NO;
        [self.leftButton setTitle:@"" forState:UIControlStateNormal];
        [self.rightButton setTitle:@"" forState:UIControlStateNormal];
        [self.rightButton setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
        [self.leftButton setImage:[UIImage imageNamed:@"编辑"] forState:UIControlStateNormal];
    }
    [self.BookListCollectionView reloadData];
}
-(void)requestData{
    self.BookListCollectionView.hidden = YES;
    [SVProgressHUD show];
    [SVProgressHUD dismissWithDelay:3.0];
    if ([UserInfoManager manager].loginUser.authorization) {
        [HttpManager fuckAddBookListHttpManagerRequestURL:@"http://www.ljsb.top:5000/common/getcollect?" dic:@{@"analysis":[UserInfoManager manager].loginUser.authorization} Succed:^(id  _Nonnull responseObject) {
            [SVProgressHUD dismiss];
            NSArray *arr = responseObject[@"result"];
            [self.secondLevelModelArrM removeAllObjects];
            for (NSDictionary *dic in arr) {
                ClassifyModel *model = [[ClassifyModel alloc] initWithDict:dic];
                model.IDString = dic[@"id"];
                [self.secondLevelModelArrM addObject: model];
            }
            [self requestRecommandModel];
            if (self.secondLevelModelArrM.count > 0) {
                self.BookListCollectionView.hidden = NO;
                [self.BookListCollectionView reloadData];
            }
            
        } requestfailure:^(NSError * _Nonnull error) {
            
        }];
    }else{
//        self.BookListCollectionView.hidden = YES;
        [self.secondLevelModelArrM removeAllObjects];
        [self requestRecommandModel];
        if (self.secondLevelModelArrM.count > 0) {
            self.BookListCollectionView.hidden = NO;
            [self.BookListCollectionView reloadData];
        }
        if (![[NSUserDefaults standardUserDefaults] objectForKey:@"first"]) {
            [[NSUserDefaults standardUserDefaults] setObject:@"dfsadf" forKey:@"first"];
            [MBProgressHUD showError:@"请先登录"];
            [SVProgressHUD dismiss];
        }else{
            
        }
    }
}
-(void)requestRecommandModel{
    NSArray *arr = [[RecommendModelManager shareManager] fetchAllUsers];
    [self.secondLevelModelArrM addObjectsFromArray:arr];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark  设置CollectionView的组数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark  设置CollectionView每组所包含的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
//    return 9;
    return self.secondLevelModelArrM.count + 1;
}

#pragma mark  设置CollectionCell的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    static NSString *identify = @"TRRightCollectionViewCell";
    if (indexPath.item == self.secondLevelModelArrM.count) {
        BookListLastCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([BookListLastCollectionViewCell class]) forIndexPath:indexPath];
        return cell;
    }
    BookListCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([BookListCollectionViewCell class]) forIndexPath:indexPath];
    ClassifyModel *model = self.secondLevelModelArrM[indexPath.item];
    cell.currentClassfiyModel = model;
    if (self.isEdit) {
        cell.isSelectedImageView.hidden = NO;
        cell.isSelectedImageView.image = [UIImage imageNamed:@"未选中"];
    }else{
        cell.isSelectedImageView.hidden = YES;
    }
    
    
    return cell;
}

#pragma mark  定义每个UICollectionView的大小

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{

    return CGSizeMake((SCREEN_WIDTH - 40) / 3.0,(SCREEN_HEIGHT - 40 - 88 - 64) / 3.0);
}

#pragma mark  点击CollectionView触发事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    self.selectIndex = indexPath.item + 99;
    if (indexPath.item == self.secondLevelModelArrM.count) {
        BaseTabbarController *ctrl = (BaseTabbarController *)[UIApplication sharedApplication].delegate.window.rootViewController;
        ctrl.selectedIndex = 1;
        return;
    }
    BookListCollectionViewCell *cell = (BookListCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self.view endEditing:YES];
    ClassifyModel *model = self.secondLevelModelArrM[indexPath.item];
    
    if (self.isEdit) {
        if ([cell.isSelectedImageView.image isEqual:[UIImage imageNamed:@"选中"]]) {
            cell.isSelectedImageView.image = [UIImage imageNamed:@"未选中"];
//            [self.muIndexSet removeIndexes:[NSIndexSet indexSetWithIndex:indexPath.item]];
            for (ClassifyModel *conentModel in self.selectedModelArrM) {
                if ([model.name isEqualToString:conentModel.name] && [[NSString filterDataToString:model.IDString] isEqualToString:[NSString filterDataToString:conentModel.IDString]]) {
                    [self.selectedModelArrM removeObject:conentModel];
                    break;
                }
            }
        }else{
            cell.isSelectedImageView.image = [UIImage imageNamed:@"选中"];
//            [self.muIndexSet addIndexes:[NSIndexSet indexSetWithIndex:indexPath.item]];
            [self.selectedModelArrM addObject:model];
        }
        
    }else{
        InfoController *ctrl = [[InfoController alloc] init];
        ctrl.hidesBottomBarWhenPushed = YES;
        ctrl.currentClassfiyModel = self.secondLevelModelArrM[indexPath.item];
        [self.navigationController pushViewController:ctrl animated:YES];
    }
}

#pragma mark  设置CollectionViewCell是否可以被点击

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}
- (IBAction)findButtonClick:(UIButton *)sender {
    BaseTabbarController *ctrl = (BaseTabbarController *)[UIApplication sharedApplication].delegate.window.rootViewController;
    ctrl.selectedIndex = 1;
}



@end
