//
//  AppDelegate.m
//  ReaderProject
//
//  Created by dy on 2019/9/19.
//  Copyright © 2019 dingye. All rights reserved.
//友盟的账号：lds666,密码：lds123456

#import "AppDelegate.h"
#import "BaseTabbarController.h"
#import "ChooseSexController.h"
#import <UMCommon/UMCommon.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName :[UIColor whiteColor]}];
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"矩形 6"] forBarMetrics:UIBarMetricsDefault];
    
//    self.window.rootViewController = [[BaseTabbarController alloc] init];
    
    NSString *isFirst = [[NSUserDefaults standardUserDefaults] objectForKey:ISFIRSTSTRING];
    if (!isFirst) {
        self.window.rootViewController = [[ChooseSexController alloc] init];
        [[NSUserDefaults standardUserDefaults] setObject:@"dfsadf" forKey:ISFIRSTSTRING];
    }else{
        self.window.rootViewController = [[BaseTabbarController alloc] init];
    }
    
//    [WXApi registerApp:WX_APP_KEY
//    universalLink:WX_RedirectURI];
    [UMConfigure initWithAppkey:@"5da6736f0cafb2da26000d32" channel:@"App Store"];

    
    return YES;
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return  [WXApi handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    return [WXApi handleOpenURL:url delegate:self];
}
- (BOOL)application:(UIApplication *)application
continueUserActivity:(NSUserActivity *)userActivity
restorationHandler:(void(^)(NSArray<id<UIUserActivityRestoring>> * __nullable
restorableObjects))restorationHandler {
return [WXApi handleOpenUniversalLink:userActivity
delegate:self];
}
//是微信终端向第三方程序发起请求，要求第三方程序响应。第三方程序响应完后必须调用 sendRsp 返回。在调用 sendRsp 返回时，会切回到微信终端程序界面。
-(void)onReq:(BaseReq *)req{
    
}
//如果第三方程序向微信发送了 sendReq 的请求，那么 onResp 会被回调。sendReq 请求调用后，会切到微信终端程序界面。
-(void)onResp:(BaseResp *)resp{
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
