//
//  InfoController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "InfoController.h"
#import "bookInfoListController.h"
#import <UIImageView+WebCache.h>
#import "LSYReadPageViewController.h"
#import "ReadHistoryManager.h"
#import <SVProgressHUD.h>
#import <UMAnalytics/MobClick.h>
@interface InfoController ()
@property (weak, nonatomic) IBOutlet UILabel *topTypeLabel;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIImageView *bookImageView;
@property (weak, nonatomic) IBOutlet UILabel *bookNamelabel;
@property (weak, nonatomic) IBOutlet UILabel *bookAtuthorLabel;
@property (weak, nonatomic) IBOutlet UILabel *tyoeLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UIButton *infoButton;
@property (weak, nonatomic) IBOutlet UILabel *allCountLabel;
@property (weak, nonatomic) IBOutlet UIButton *infoListButton;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *startButton;

@property(nonatomic,strong)NSMutableArray *dataSourceArrM;
@end

@implementation InfoController
-(NSMutableArray *)dataSourceArrM{
    if (!_dataSourceArrM) {
        _dataSourceArrM = [[NSMutableArray alloc] init];
    }
    return _dataSourceArrM;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self.backButton addTarget:self action:@selector(backButtonclick) forControlEvents:UIControlEventTouchUpInside];
    [self.infoListButton addTarget:self action:@selector(gotobookListController) forControlEvents:UIControlEventTouchUpInside];
    
    [self.bookImageView sd_setImageWithURL:[NSURL URLWithString:self.currentClassfiyModel.cover]];
    self.bookNamelabel.text = self.currentClassfiyModel.name;
    self.bookAtuthorLabel.text = self.currentClassfiyModel.author;
    self.topTypeLabel.text = self.currentClassfiyModel.label;
    self.infoLabel.text = self.currentClassfiyModel.summary;
    self.allCountLabel.text = [NSString stringWithFormat:@"共%@章",self.currentClassfiyModel.countchapter];
    NSArray *arr = [[ReadHistoryManager shareManager] fetchAllUsers];
    BOOL isExist = NO;
    for (ClassifyModel *model in arr) {
        if ([model.name isEqualToString:self.currentClassfiyModel.name] && [[NSString filterDataToString:model.IDString] isEqualToString:[NSString filterDataToString:self.currentClassfiyModel.IDString]]) {
            isExist = YES;
        }
    }
    if (!isExist) {
        [[ReadHistoryManager shareManager] insertDataWithModel:self.currentClassfiyModel];
    }
    [self requestData];
    [MobClick setAutoPageEnabled:YES];
}
-(void)requestData{
    [HttpManager fuckGetHttpManagerType:@"chapter" Type1:@"book" IDString:[self.currentClassfiyModel.IDString intValue] page:1 limit:10 requestSucced:^(id  _Nonnull responseObject) {
        [self.dataSourceArrM removeAllObjects];
        NSArray *arr = responseObject[@"result"];
        for (NSDictionary *dic in arr) {
            BookListModel *model = [[BookListModel alloc] initWithDict:dic];
            model.IDString = dic[@"id"];
            [self.dataSourceArrM addObject:model];
        }
        [SVProgressHUD dismiss];
    } requestfailure:^(NSError * _Nonnull error) {
    }];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
-(void)backButtonclick{
    [self.navigationController popViewControllerAnimated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)gotobookListController{
    bookInfoListController *ctrl = [[bookInfoListController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    ctrl.currentClassfiyModel = self.currentClassfiyModel;
    [self.navigationController pushViewController:ctrl animated:YES];
}
- (IBAction)addButton:(UIButton *)sender {
    if ([UserInfoManager manager].loginUser) {
        [SVProgressHUD show];
        [SVProgressHUD dismissWithDelay:3.0];
        
        [HttpManager fuckAddBookListHttpManagerRequestURL:[NSString stringWithFormat:@"http://www.ljsb.top:5000/common/collect?analysis=%@&",[UserInfoManager manager].loginUser.authorization] dic:@{@"bookId":self.currentClassfiyModel.IDString} Succed:^(id  _Nonnull responseObject) {
            [MBProgressHUD showError:@"添加成功"];
            [SVProgressHUD dismiss];
            
        } requestfailure:^(NSError * _Nonnull error) {
            [MBProgressHUD showError:@"添加错误，请重试"];
            [SVProgressHUD dismiss];
        }];
    }else{
        [MBProgressHUD showError:@"请先登录"];
    }
}
- (IBAction)startButton:(UIButton *)sender {
    if (self.dataSourceArrM.count >= 1) {
        BookListModel *model = [self.dataSourceArrM firstObject];
        LSYReadPageViewController *pageView = [[LSYReadPageViewController alloc] init];
        pageView.currentBookListModel = model;
        pageView.currentClassifyModel = self.currentClassfiyModel;
        [self.navigationController pushViewController:pageView animated:YES];
    }else{
        [SVProgressHUD show];
        [self requestData];
    }
}
@end
