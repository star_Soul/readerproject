//
//  MineTableViewCell.m
//  ReaderProject
//
//  Created by 李星星 on 2019/10/14.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "MineTableViewCell.h"

@implementation MineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [_leftImageView release];
    [_titleLabel release];
    [_rightLabel release];
    [super dealloc];
}
@end
