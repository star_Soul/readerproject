//
//  MineController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "MineController.h"
#import "MineHeaderView.h"
#import "readHistoryListController.h"
#import "FeedBackController.h"
#import "LoginController.h"
#import "UserAgreePopView.h"
#import "ChooseSexController.h"
#import "WXApi.h"
#import <SDWebImage/SDImageCache.h>
#import <SVProgressHUD.h>
#import "MineTableViewCell.h"
#import <UMAnalytics/MobClick.h>
@interface MineController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *listTableView;

@property(nonatomic,strong)MineHeaderView  *headerView;

@property (nonatomic, strong) NSArray *titleArr;

@property (nonatomic, strong) NSArray *imageArr;

@property(nonatomic,strong)UIView *backGroundView;
@property(nonatomic,strong)UserAgreePopView *userAgreePopView;
@end

@implementation MineController
-(NSArray *)titleArr{
    if (!_titleArr) {
//        _titleArr = [[NSArray alloc] initWithObjects:@"浏览记录",@"意见反馈",@"分享好友",@"当前版本",@"清空缓存",@"用户协议", nil];
        _titleArr = [[NSArray alloc] initWithObjects:@"浏览记录",@"意见反馈",@"当前版本",@"清空缓存",@"用户协议", nil];
    }
    return _titleArr;
}
-(NSArray *)imageArr{
    if (!_imageArr) {
//        _imageArr = [[NSArray alloc] initWithObjects:@"浏览记录",@"意见反馈",@"分享好友",@"当前版本",@"清空缓存",@"用户协议", nil];
        _imageArr = [[NSArray alloc] initWithObjects:@"浏览记录",@"意见反馈",@"当前版本",@"清空缓存",@"用户协议", nil];
    }
    return _imageArr;
}
-(UIView *)backGroundView{
    if (!_backGroundView) {
        _backGroundView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - 60)];
        _backGroundView.backgroundColor = [UIColor clearColor];
        
        [self.view addSubview:_backGroundView];
        
        UIView *grayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 60)];
        if (SCREEN_HEIGHT > 800) {
            _backGroundView.frame = CGRectMake(0, SCREEN_HEIGHT, SCREEN_WIDTH, SCREEN_HEIGHT - 88);
            grayView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - 88);
        }
        grayView.backgroundColor = [UIColor grayColor];
        grayView.alpha = 0.5;
        [_backGroundView addSubview:grayView];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backGroundViewTap)];
        [grayView addGestureRecognizer:tap];
        [_backGroundView addSubview:self.userAgreePopView];
    }
    return _backGroundView;
}
-(UserAgreePopView *)userAgreePopView{
    if (!_userAgreePopView) {
        _userAgreePopView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([UserAgreePopView class]) owner:self options:nil].lastObject;
        _userAgreePopView.frame = CGRectMake(30, 50, SCREEN_WIDTH - 30 * 2, 500);
        _userAgreePopView.layer.cornerRadius = 5.0;
        _userAgreePopView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        _userAgreePopView.layer.shadowOffset = CGSizeMake(-5,5);
        _userAgreePopView.layer.shadowOpacity = 1;
        _userAgreePopView.layer.shadowRadius = 5;
        _userAgreePopView.layer.cornerRadius = 5;
        _userAgreePopView.layer.masksToBounds = YES;
        _userAgreePopView.clipsToBounds = NO;
        _userAgreePopView.UserAgreeLabel.text = @"本协议为您与「有你小说 APP」(下称本 APP)版权所有者之间所订立的契约，具有合同的法律效力，请您仔细阅读。\n一、本协议内容、生效、变更。本协议内容包括协议正文及所有本 APP 已经发布的或将来可能发布的各类规则。所有规则为本协议不可分割的组成部分，与协议正文具有同等法律效力。如您对协议有任何疑问，应向本 APP 咨询。只要您使用本 APP 平台服务，则本协议即对您产生约束，届时您不应以未阅读本协议的内容或者未获得本 APP对您问询的解答等理由，主张本协议无效，或要求撤销本协议。您确认：本协议条款是处理双方权利义务的契约，始终有效，法律另有强制性规定或双方另有特别约定的，依其规定。您承诺接受并遵守本协议的约定。如果您不同意本协议的约定，您应立即停止注册程序或停止使用本 APP平台服务。本 APP 有权根据需要不定期地制订、修改本协议及/或各类规则，并在本 APP 平台公示，不再另行单独通知用户。变更后的协议和规则一经在 APP 公布，立即生效。如您不同意相关变更，应当立即停止使用本 APP 平台服务。您继续使用本 APP 平台服务的，即表明您接受修订后的协议和规则。\n二、本 APP 平台所刊载的所有资料及图表仅供参考使用。用户明确同意其使用本 APP 平台网络服务所存在的风险将完全由其自己承担；因其使用本 APP 平台网络服务而产生的一切后果也由其自己承担，本 APP 平台对用户不承担任何责任。\n三、本 APP 平台的用户在参加本 APP 平台举办的各种活动时，我们将在您的同意及确认下，通过注册表格等形式要求您提供一些个人资料，如：您的姓名、性别、年龄、出生日期、身份证号、家庭住址、教育程度、公司情况、所属行业等。我们在未经您同意的情况下，绝对不会将您的任何资料以任何方式泄露给任何第三方。\n四、当政府司法机关依照法定程序要求本 APP 平台披露个人资料时，我们将根据执法单位之要求或为公共安全之目的提供个人资料。在此情况下之任何披露，本 APP 平台均得免责。\n五、 任何由于黑客攻击、计算机病毒侵入或发作、因政府管制而造成的暂时性关闭等影响网络正常经营的不可抗力而造成的个人资料泄露、丢失、被盗用或被窜改等，本 APP 平台均得免责。\n六、由于与本 APP 平台链接的其它网站所造成之个人资料泄露及由此而导致的任何法律争议和后果，本 APP 平台均得免责。\n七、本 APP 平台如因系统维护或升级而需暂停服务时，将事先公告。若因线路及非本网站控制范围外的硬件故障或其它不可抗力而导致暂停服务，于暂停服务期间造成的一切不便与损失，本 APP 平台不负任何责任。\n八、本 APP 平台使用者因为违反本声明的规定而触犯中华人民共和国法律的，一切后果自己负责，本 APP 平台不承担任何责任。\n九、凡以任何方式登陆本 APP 平台或直接、间接使用本 APP 平台资料者，视为自愿接受本 APP 平台声明的约束。\n十、本声明未涉及的问题参见国家有关法律法规，当本声明与国家法律法规冲突时，以国家法律法规为准。\n十一、本 APP 平台不担保网络服务一定能满足用户的要求，也不担保网络服务不会中断，对网络服务的及时性、安全性、准确性也都不作担保。\n十二、本 APP 平台不保证为向用户提供便利而设置的外部链接的准确性和完整性，同时，对于该等外部链接指向的不由本 APP 平台实际控制的任何网页或平台上的内容，本 APP 平台不承担任何责任。\n十三、对于因不可抗力或本 APP 平台不能控制的原因造成的网络服务中断或其它缺陷，本 APP 平台不承担任何责任，但将尽力减少因此而给用户造成的损失和影响。\n十四、本 APP 平台所有页面的版式、图片版权均为本 APP 平台所有，未经授权，不得用于除本 APP 平台之外的任何站点。\n十五、本 APP 平台之声明以及其修改权、更新权及最终解释权均属本 APP 平台所有。";
//        [self.view addSubview:_userAgreePopView];
    }
   return  _userAgreePopView;
}
-(void)backGroundViewTap{

    [UIView animateWithDuration:0.3 animations:^{
        self.backGroundView.transform = CGAffineTransformIdentity;
    }];
}
-(void)showPopView{
    self.backGroundView.transform = CGAffineTransformIdentity;
    [UIView animateWithDuration:0.3 animations:^{
        self.backGroundView.transform = CGAffineTransformMakeTranslation(0, -SCREEN_HEIGHT);
    } completion:^(BOOL finished) {

    }];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"我的";
    self.back.hidden = YES;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 260)];
    self.headerView = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([MineHeaderView class]) owner:self options:nil].lastObject;
    self.headerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, 260);
    self.headerView.userImageButton.layer.cornerRadius = 80 / 2.0;
    self.headerView.userImageButton.clipsToBounds = YES;

    [self.headerView.userImageButton addTarget:self action:@selector(chooseSexController) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView.userNameButton addTarget:self action:@selector(personalInfoController) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:self.headerView];
    self.listTableView.tableHeaderView = view;
    
    self.listTableView.backgroundColor = [UIColor whiteColor];
    self.listTableView.delegate = self;
    self.listTableView.dataSource = self;
    self.listTableView.showsVerticalScrollIndicator = NO;
    //    self.listTableView.separatorColor = color_bg;
    self.listTableView.separatorInset = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.listTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    [self.listTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MineTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([MineTableViewCell class])];
    self.listTableView.rowHeight = 50;
    self.listTableView.bounces = YES;
    self.listTableView.tableFooterView = [UIView new];
    if (SCREEN_HEIGHT < 800) {
        self.headerView.titleLableHeight.constant = 88;
    }
    [MobClick setAutoPageEnabled:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self requestData];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
-(void)requestData{
    if ([UserInfoManager manager].loginUser.authorization) {
        [self.headerView.userNameButton setTitle:[UserInfoManager manager].loginUser.phone forState:UIControlStateNormal];
        self.headerView.userNameButton.userInteractionEnabled = NO;
        self.headerView.userImageButton.userInteractionEnabled = YES;
    }else{
        [self.headerView.userNameButton setTitle:@"登录/注册" forState:UIControlStateNormal];
        self.headerView.userNameButton.userInteractionEnabled = YES;
        self.headerView.userImageButton.userInteractionEnabled = NO;
    }
    [self.listTableView reloadData];
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return  self.titleArr.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MineTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([MineTableViewCell class])];
   
    cell.titleLabel.text = self.titleArr[indexPath.row];
    cell.leftImageView.image = [UIImage imageNamed:self.imageArr[indexPath.row]];
    cell.selectionStyle = UITableViewCellSeparatorStyleNone;
//    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.rightLabel.hidden = YES;
    if ([self.titleArr[indexPath.row] isEqualToString:@"当前版本"]) {
        cell.detailTextLabel.text = @"V1.0.0";
        cell.accessoryType = UITableViewCellAccessoryNone;
    }else if ([self.titleArr[indexPath.row] isEqualToString:@"清空缓存"]) {
        cell.rightLabel.hidden = NO;
        cell.rightLabel.text = @"当前缓存：0.0";
        NSString * currentCacheSize = [NSString stringWithFormat:@"%@",[self fileSizeWithInteger:[[SDImageCache sharedImageCache] totalDiskSize]]];
        cell.rightLabel.text = [NSString stringWithFormat:@"当前缓存：%@",currentCacheSize];
    }else{
        cell.detailTextLabel.text = @"";
    }
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 60;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if ([UserInfoManager manager].loginUser.authorization.length > 0) {
        return 80;
    }else if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 80;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] init];
    if ([UserInfoManager manager].loginUser.authorization.length > 0) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:@"退出登录" forState:UIControlStateNormal];
        [button setBackgroundColor:DEFAULT_T_ORANGE];
        button.frame = CGRectMake(50, 20, SCREEN_WIDTH - 50 * 2, 40);
        button.layer.cornerRadius = 40 / 2.0;
        button.clipsToBounds = YES;
        [button addTarget:self action:@selector(exitButtonClick) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:button];
    }else if (section == 0) {
        return view;
    }
    
    return view;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([self.titleArr[indexPath.row] isEqualToString:@"浏览记录"]) {
        readHistoryListController *ctrl = [[readHistoryListController alloc] init];
        ctrl.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:ctrl animated:YES];
    }else if ([self.titleArr[indexPath.row] isEqualToString:@"意见反馈"]) {
        FeedBackController *ctrl = [[FeedBackController alloc] init];
        ctrl.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:ctrl animated:YES];
    }else if ([self.titleArr[indexPath.row] isEqualToString:@"分享好友"]) {
//        GlassesMyCouponController *ctrl = [[GlassesMyCouponController alloc] init];
//        ctrl.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:ctrl animated:YES];
        SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
        req.bText = YES;
        req.text = @"分享的内容";
        req.scene = WXSceneSession;
        [WXApi sendReq:req];
    }else if ([self.titleArr[indexPath.row] isEqualToString:@"清空缓存"]) {
//        GlassesFeedBackController *ctrl = [[GlassesFeedBackController alloc] init];
//        ctrl.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:ctrl animated:YES];
        [SVProgressHUD show];
        [SVProgressHUD dismissWithDelay:2.0];
        [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
            [SVProgressHUD dismiss];
            [MBProgressHUD showError:@"清除成功！"];
            [self.listTableView reloadData];
        }];
    }else if ([self.titleArr[indexPath.row] isEqualToString:@"用户协议"]) {
//        [self userAgreePopView];
//        [self.userAgreePopView.agreeButton addTarget:self action:@selector(popViewDismiss) forControlEvents:UIControlEventTouchUpInside];
        [self showPopView];
        [self.userAgreePopView.agreeButton addTarget:self action:@selector(backGroundViewTap) forControlEvents:UIControlEventTouchUpInside];
    }
}
// 根据数据计算出大小
- (NSString *) fileSizeWithInteger:(NSInteger)size{
    // 1K = 1024dB, 1M = 1024K,1G = 1024M
    if (size < 1024) {// 小于1k
        return [NSString stringWithFormat:@"%ldB",(long)size];
    }else if (size < 1024 * 1024){// 小于1m
        CGFloat aFloat = size/1024;
        return [NSString stringWithFormat:@"%.0fK",aFloat];
    }else if (size < 1024 * 1024 * 1024){// 小于1G
        CGFloat aFloat = size/(1024 * 1024);
        return [NSString stringWithFormat:@"%.1fM",aFloat];
    }else{
        CGFloat aFloat = size/(1024*1024*1024);
        return [NSString stringWithFormat:@"%.1fG",aFloat];
    }
}
#pragma mark - 用户事件
-(void)jump2AlbmWithCell{
//    [[JCPickImage shareManage]addImageWithControl:self];
//
//    [JCPickImage shareManage].myblock = ^(UIImage *image) {
//        if (image == nil) return;
//
//        [[SDImageCache sharedImageCache] storeImage:image forKey:DEFAULT_ICON_KEY completion:^{
//            [self.headerView.userImageButton setBackgroundImage:image forState:UIControlStateNormal];
//        }];
//    };
}
-(void)personalInfoController{
    LoginController *ctrl = [[LoginController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:ctrl animated:YES];
    
}
-(void)chooseSexController{
//    ChooseSexController *ctrl = [[ChooseSexController alloc] init];
//    ctrl.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:ctrl animated:YES];
}
-(void)popViewDismiss{
    [_userAgreePopView removeFromSuperview];
    _userAgreePopView = nil;
}
-(void)exitButtonClick{
    [[UserInfoManager manager] clearUserInfo];
    [self requestData];
    [self.listTableView reloadData];
    self.headerView.userImageButton.userInteractionEnabled = NO;
}

@end
