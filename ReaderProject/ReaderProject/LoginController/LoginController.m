//
//  LoginController.m
//  ReaderProject
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "LoginController.h"
#import "RegisterController.h"
#import <SVProgressHUD.h>
#import <objc/runtime.h>
#import "ChangePwdController.h"
#import <UMAnalytics/MobClick.h>
@interface LoginController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;
@property (weak, nonatomic) IBOutlet UIButton *QQLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *WXLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *WBLoginButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPwdButton;

@end

@implementation LoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"登录";
    self.loginButton.layer.cornerRadius =
    self.registerButton.layer.cornerRadius = 20;
    self.loginButton.clipsToBounds =
    self.registerButton.clipsToBounds = YES;
    self.registerButton.layer.borderColor = DEFAULT_T_ORANGE.CGColor;
    self.registerButton.layer.borderWidth = 1.0;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    [self.view addGestureRecognizer:tap];
    self.view.backgroundColor = [UIColor whiteColor];
    self.phoneTextField.clearButtonMode = UITextFieldViewModeAlways;
    
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(self.phoneTextField, ivar);
    placeholderLabel.textColor = UIColorFromRGB(0x808080);
    self.phoneTextField.textColor = UIColorFromRGB(0x808080);

    UILabel *pwdplaceholderLabel = object_getIvar(self.pwdTextField, ivar);
    pwdplaceholderLabel.textColor = UIColorFromRGB(0x808080);
    self.pwdTextField.textColor = UIColorFromRGB(0x808080);
    [MobClick setAutoPageEnabled:YES];
}
-(void)tapClick{
    [self.view endEditing:YES];
}
- (IBAction)forgotPwdButtonClick:(UIButton *)sender {
    ChangePwdController *ctrl = [[ChangePwdController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:ctrl animated:YES];
}
- (IBAction)loginButtonClick:(UIButton *)sender {
    if (self.phoneTextField.text.length != 11) {
        [MBProgressHUD showError:@"请输入正确的手机号"];
        return;
    }else if (self.pwdTextField.text.length < 6){
        [MBProgressHUD showError:@"密码长度至少6位"];
        return;
    }else{
        [SVProgressHUD show];
        [SVProgressHUD dismissWithDelay:3.0];
        [HttpManager fuckHttpManagerRequestUrl:@"http://www.ljsb.top:5000/common/login" dic:@{@"username":self.phoneTextField.text,@"password":self.pwdTextField.text} Succed:^(id  _Nonnull responseObject) {
            [SVProgressHUD dismiss];
            PBUserInfo *info = [[PBUserInfo alloc] init];
            info.phone = self.phoneTextField.text;
            info.userPassword =  self.pwdTextField.text;
            info.sex = 1;
            info.authorization = responseObject[@"result"][@"analysis"];
            [[UserInfoManager manager] save: info];
            
            [self.navigationController popToRootViewControllerAnimated:YES];
        } requestfailure:^(NSError * _Nonnull error) {
            [SVProgressHUD dismiss];
        }];
    }
}
- (IBAction)registerButtonClick:(UIButton *)sender {
    RegisterController *ctrl = [[RegisterController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:ctrl animated:YES];
}
- (IBAction)qqButtonClick:(UIButton *)sender {
}
- (IBAction)wxButtonClick:(UIButton *)sender {
}
- (IBAction)wbButtonClick:(UIButton *)sender {
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
