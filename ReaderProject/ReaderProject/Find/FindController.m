//
//  FindController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "FindController.h"
#import "SDCycleScrollView.h"
#import "SearchTableViewCell.h"
#import <Masonry/Masonry.h>
#import "SearchViewController.h"
#import "InfoController.h"
#import "MonthlyModel.h"
#import "ClassifyModel.h"
#import <SVProgressHUD.h>
#import "TypeController.h"
#import "BannberModel.h"
#import <UMAnalytics/MobClick.h>
@interface FindController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,SDCycleScrollViewDelegate>
@property (nonatomic, strong) SDCycleScrollView *bannerHeaderView;

@property (nonatomic, strong) UITableView *rightTableView;

@property(nonatomic,strong)NSMutableArray *dataSourceTitleArrM;
@property(nonatomic,strong)NSMutableArray *dataSourceInfoArrM;
@property(nonatomic,strong)NSMutableArray *bannerArrM;

@property(nonatomic,strong)UIButton *rightButton;
@end

@implementation FindController
-(NSMutableArray *)dataSourceTitleArrM{
    if (!_dataSourceTitleArrM) {
        _dataSourceTitleArrM = [[NSMutableArray alloc] init];
    }
    return _dataSourceTitleArrM;
}
-(NSMutableArray *)dataSourceInfoArrM{
    if (!_dataSourceInfoArrM) {
        _dataSourceInfoArrM = [[NSMutableArray alloc] init];
    }
    return _dataSourceInfoArrM;
}
-(NSMutableArray *)bannerArrM{
    if (!_bannerArrM) {
        _bannerArrM = [[NSMutableArray alloc] init];
    }
    return _bannerArrM;
}
-(UITableView *)rightTableView{
    if (!_rightTableView) {
        _rightTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.tableFooterView = [UIView new];
        [_rightTableView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SearchTableViewCell class])];
    }
    return _rightTableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"发现";
    
    CGFloat heightForBannerHeaderView = 210;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, heightForBannerHeaderView)];
    self.bannerHeaderView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, heightForBannerHeaderView) delegate:self placeholderImage:[UIImage imageNamed:@"4"]];
    self.bannerHeaderView.localizationImageNamesGroup = @[@"3",@"4"];
    self.bannerHeaderView.tag = 1000;
    self.bannerHeaderView.layer.cornerRadius = 5.0;
    self.bannerHeaderView.clipsToBounds = YES;
    [view addSubview:self.bannerHeaderView];
    
    self.rightTableView.tableHeaderView = view;
//    self.rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.rightTableView.delegate = self;
    self.rightTableView.dataSource = self;
    self.rightTableView.tableFooterView = [UIView new];
    [self.view addSubview:self.rightTableView];
    [self.rightTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.top.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
    
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn addTarget:self action:@selector(rightBarButtonItemClick:) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setImage:[UIImage imageNamed:@"搜索"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];
    self.rightButton = rightBtn;
    [self.rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(60);
        
    }];
    [self requestData];
    [self requestBannber];
    [MobClick setAutoPageEnabled:YES];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.dataSourceTitleArrM.count <= 0) {
        [self requestData];
    }
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)requestData{
    [SVProgressHUD show];
    [HttpManager fuckGeSearchtHttpManagerRequestSucced:^(id  _Nonnull responseObject) {
        [self.dataSourceTitleArrM removeAllObjects];
        [self.dataSourceInfoArrM removeAllObjects];
        NSArray *arr = responseObject[@"result"];
        for (NSDictionary *dic in arr) {
            MonthlyModel *model = [[MonthlyModel alloc] initWithDict:dic];
            model.IDString = dic[@"id"];
            
            [self.dataSourceTitleArrM addObject:model];
        }
        
        for (MonthlyModel *model in self.dataSourceTitleArrM) {
            [self requestInfoData:model];
        }
        
    } requestfailure:^(NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
    }];
}
-(void)requestInfoData:(MonthlyModel *)model{
    [HttpManager fuckGetHttpManagerType:@"monthnov" Type1:@"monthly" IDString:[model.IDString intValue] page:1 limit:20 requestSucced:^(id  _Nonnull responseObject) {
        NSArray *arr = responseObject[@"result"];
        NSMutableArray *arrM = [ [NSMutableArray alloc] init];
        for (NSDictionary *dic in arr) {
            ClassifyModel *model = [[ClassifyModel alloc] initWithDict:dic];
            model.IDString = dic[@"id"];
            [arrM addObject:model];
        }
        [self.dataSourceInfoArrM addObject:arrM];
        if (self.dataSourceInfoArrM.count == self.dataSourceTitleArrM.count) {
            [self.rightTableView reloadData];
            [SVProgressHUD dismiss];
        }
    } requestfailure:^(NSError * _Nonnull error) {
        
    }];

}
-(void)requestBannber{
    [HttpManager fuckAddBookListHttpManagerRequestURL:@"http://www.ljsb.top:5000/front/banner" dic:nil Succed:^(id  _Nonnull responseObject) {
        NSArray *result = responseObject[@"result"];
        [self.bannerArrM removeAllObjects];
        NSMutableArray *imageArrM = [[NSMutableArray alloc] init];
        for (NSDictionary *dic in result) {
            BannberModel *model = [[BannberModel alloc] initWithDict:dic];
            [imageArrM addObject:model.imgurl];
            [self.bannerArrM addObject:model];
        }
        self.bannerHeaderView.localizationImageNamesGroup = imageArrM;
        
    } requestfailure:^(NSError * _Nonnull error) {
        
    }];
}
-(void)rightBarButtonItemClick:(UIButton *)sender{

        SearchViewController *ctrl = [[SearchViewController alloc] init];
        ctrl.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:ctrl animated:YES];
}
-(void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    BannberModel *model = self.bannerArrM[index];
    [SVProgressHUD show];
    [HttpManager fuckAddBookListHttpManagerRequestURL:@"http://www.ljsb.top:5000/front/book?" dic:@{@"bookId":model.bookId} Succed:^(id  _Nonnull responseObject) {
        ClassifyModel *model = [[ClassifyModel alloc] initWithDict:responseObject[@"result"]];
        model.IDString = responseObject[@"result"][@"id"];
        [SVProgressHUD dismiss];
        InfoController *ctrl = [[InfoController alloc] init];
        ctrl.hidesBottomBarWhenPushed = YES;
        ctrl.currentClassfiyModel = model;
        [self.navigationController pushViewController:ctrl animated:YES];
    } requestfailure:^(NSError * _Nonnull error) {
        
    }];
    
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceTitleArrM.count;
//    return 3;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *arr = self.dataSourceInfoArrM[section];
    return arr.count;
//    return 3;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchTableViewCell class]) forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
 NSArray *arr = self.dataSourceInfoArrM[indexPath.section];
    cell.currentClassifyModel = arr[indexPath.row];
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 140;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{

    return 50;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
        UIView *view = [UIView new];
        view.backgroundColor = [UIColor whiteColor];

    UIImageView *imageView = [[UIImageView alloc] init];
    imageView.image = [UIImage imageNamed:@"风云榜"];
    if (section == 0) {
        imageView.image = [UIImage imageNamed:@"风云榜"];
    }else if (section == 1){
        imageView.image = [UIImage imageNamed:@"收藏榜"];
    }else if (section == 2){
        imageView.image = [UIImage imageNamed:@"新书榜"];
    }
    imageView.frame = CGRectMake(20, 15, 25, 25);
    [view addSubview:imageView];
        UILabel *label = [[UILabel alloc] init];
        label.frame = CGRectMake(CGRectGetMaxX(imageView.frame) + 15, 15, SCREEN_WIDTH, 25);
        label.textColor = [UIColor blackColor];
//        label.textAlignment = nste;
        label.font = FONTSIZE(18);
    MonthlyModel *model = self.dataSourceTitleArrM[section];
    label.text = model.monthly;
//        if (section == 0) {
//            label.text = @"风云榜";
//        }else if (section == 1){
//            label.text = @"收藏榜";
//        }else if (section == 2){
//            label.text = @"新书榜";
//        }
        [view addSubview:label];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"更多" forState:UIControlStateNormal];
    [button setTitleColor:DEFAULT_T_ORANGE forState:UIControlStateNormal];
    button.frame = CGRectMake(SCREEN_WIDTH - 20 - 60, 15, 60, 25);
    button.tag = section;
    [button addTarget:self action:@selector(moreButton:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:button];
    return view;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    return [UIView new];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    InfoController *ctrl = [[InfoController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    NSArray *arr = self.dataSourceInfoArrM[indexPath.section];
    ctrl.currentClassfiyModel = arr[indexPath.row];
    [self.navigationController pushViewController:ctrl animated:YES];
}
-(void)moreButton:(UIButton *)sender{
    NSInteger tag = sender.tag;
    MonthlyModel *model = self.dataSourceTitleArrM[tag];
    TypeController *ctlr = [[TypeController alloc] init];
    ctlr.hidesBottomBarWhenPushed = YES;
    ctlr.currentMonthlyModel = model;
    ctlr.isFind = YES;
    [self.navigationController pushViewController:ctlr animated:YES];
}
@end
