//
//  MonthlyModel.h
//  ReaderProject
//
//  Created by 李星星 on 2019/9/23.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface MonthlyModel : BaseModel
@property(nonatomic,strong)NSString *IDString;//"id": 1,
@property(nonatomic,strong)NSString *monthly;//"monthly": "推荐榜"
@end

NS_ASSUME_NONNULL_END
