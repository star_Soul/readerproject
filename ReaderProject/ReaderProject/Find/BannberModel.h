//
//  BannberModel.h
//  ReaderProject
//
//  Created by 李星星 on 2019/10/11.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface BannberModel : BaseModel
@property(nonatomic,strong)NSString *bookId;//"bookId": 74,
@property(nonatomic,strong)NSString *imgurl;//"imgurl": "https://cdn.cnbj0.fds.api.mi-img.com/atlas/b1fb2c6c919dc463bc2ebae880f4c873?thumb=1&w=240&h=320",
@property(nonatomic,strong)NSString *rank;//"rank": 1
@end

NS_ASSUME_NONNULL_END
