//
//  UserInfoManager.h
//  ReaderProject
//
//  Created by 李星星 on 2019/9/29.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BaseModel.h"
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,CertifyState)
{
    CertifyStateUnCertify,
    CertifyStateFinish,
    CertifyStateFail,
    CertifyStateLoging,
};
typedef NS_ENUM(NSInteger,LoginType)
{
    LoginTypeVer,
    LoginTypeRegister,
    LoginTypePassword,
    LoginTypeForgetPassword,
};
@interface SystemConfiguration : BaseModel
@property (nonatomic, assign) CGFloat accountAmount;//用户账户金额
@property (nonatomic, copy) NSString *content;//推荐规则内容"
@property (nonatomic, copy) NSString *customerPhone;//客服电话
@property (nonatomic, copy) NSString *customerQQ;//客服QQ
@property (nonatomic, copy) NSString *orderBook;//快速下单说明
@property (nonatomic, copy) NSString *param;//注册协议和商家申请协议
@property (nonatomic, copy) NSString *recommenProportion;//推荐提成比例
@end

@interface UserRoleEntity : BaseModel
@property (nonatomic, copy) NSString *userRoleId; //角色编号
@property (nonatomic, copy) NSString *userRoleNo; //角色业务标识
@property (nonatomic, copy) NSString *userId; //用户编号
@property (nonatomic, copy) NSString *roleType; //角色类型:1.教练 2.穿线师 3.补货员 4.仓管
@property (nonatomic, copy) NSString *address; //详细地址
@property (nonatomic, copy) NSString *commissionRatioint; //分成比例【1-99】
@property (nonatomic, copy) NSString *chargingStandard; //收费标准:xxx/元【只针对教练】
@property (nonatomic, copy) NSString *cabinetId; //机柜编号【只针对于教练】
@property (nonatomic, copy) NSString *status; //状态:1.审核中 2.通过 3.驳回【只针对于教练】
@property (nonatomic, copy) NSString *refuseReason; //驳回理由【只针对于教练】
@property (nonatomic, copy) NSString *idCardPicFrontImg; //身份证正面
@property (nonatomic, copy) NSString *idCardPicBackImg; //身份证反面
@property (nonatomic, copy) NSString *certificateImg; //资质证书
@property (nonatomic, copy) NSString *isFlag; //角色状态:1.正常 2.禁用
@property (nonatomic, assign) long ctime; //创建时间
@property (nonatomic, assign) long utime; //教练审核通过时间
@end

@interface BankNumber : BaseModel
@property (nonatomic, copy) NSString *cashRequestId;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *realName;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, copy) NSString *identityID;
@property (nonatomic, copy) NSString *bankNumber;
@property (nonatomic, copy) NSString *     bankName;
@property (nonatomic, copy) NSString *branchName;
@property (nonatomic, copy) NSString *nayReason;
@property (nonatomic, assign) NSInteger status;
@property (nonatomic, assign) NSInteger cashRequesType;
@property (nonatomic, assign) long ctime;
@property (nonatomic, assign) long etime;
@end

@interface PBUserInfo : BaseModel
@property (nonatomic, assign) NSInteger id;
@property (nonatomic, copy) NSString *userId; //用户ID
@property (nonatomic, copy) NSString *nickname; //昵称
@property (nonatomic, copy) NSString *authorization;
@property (nonatomic, copy) NSString *phone; //手机号码
@property (nonatomic, copy) NSString *gender; //性别:1.男 2.女
@property (nonatomic, copy) NSString *ballAge; //球龄
@property (nonatomic, copy) NSString *ballLevel; //网球等级
@property (nonatomic, copy) NSString *isVip; //会员:1.否 2.是
@property (nonatomic, copy) NSString *vipUseTime; //VIP的期限
@property (nonatomic, copy) NSString *vipEndTime; //VIP过期时间
@property (nonatomic, copy) NSString *openId; //微信授权ID
@property (nonatomic, copy) NSString *isFlag; //用户状态:1.正常 2.禁用
@property (nonatomic, assign) long ctime; //创建时间
@property (nonatomic, copy) NSString *avatarUrl; //头像
@property (nonatomic, strong) UserRoleEntity *userRoleEntity; //角色对象


@property (nonatomic, copy) NSString *email;
@property (nonatomic, assign) NSInteger sex;
@property (nonatomic, assign) NSInteger isHasRedpackage;
@property (nonatomic, copy) NSString *qqNumber;
@property (nonatomic, copy) NSString *usersPhone;
@property (nonatomic, copy) NSString *userAccount;
@property (nonatomic, assign) NSString *uid;
@property (nonatomic, copy) NSString *userPassword;
@property (nonatomic, copy) NSString *telephone;
@property (nonatomic, copy) NSString *usersPic;
@property (nonatomic, assign) NSInteger usersType;
@property (nonatomic, copy) NSString *wechatNumber;
@property (nonatomic, strong) BankNumber *bankNumber;
@property (nonatomic, assign) CGFloat __latitude;
@property (nonatomic, assign) CGFloat __longitude;
@end
@interface UserInfoManager : BaseModel
@property (nonatomic, strong, readonly) PBUserInfo *loginUser;
@property (nonatomic, strong) SystemConfiguration *sysConfigura;
@property (nonatomic, assign) CGFloat latitude;
@property (nonatomic, assign) CGFloat longitude;

+ (UserInfoManager *)manager;

- (void)save:(PBUserInfo *)info;
- (void)saveSys:(SystemConfiguration *)configura;
- (void)clearUserInfo;
@end

