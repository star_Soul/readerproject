//
//  HttpManager.m
//  ReaderProject
//
//  Created by 李星星 on 2019/9/23.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "HttpManager.h"
#import <AFNetworking.h>
#import <SVProgressHUD/SVProgressHUD.h>
#define SVPDELAY 1
#define kDefaultTimeoutInterval 3
@implementation HttpManager
+(void)fuckGetHttpManagerType:(NSString *)type Type1:(NSString *)Type1 IDString:(NSInteger )IDString requestSucced:(Success)Succed requestfailure:(Failure)failure
{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/jpeg",@"image/png", nil];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSString *urlStr = [NSString stringWithFormat:@"http://www.ljsb.top:5000/front/%@?%@=%ld", type, Type1,IDString];
    
    [manager GET:urlStr parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //此处要判断状态  status  判断是否请求成功
        //—->  此处跳转到请求成功的返回。在此处进行结果的判断。网络失败等原因
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            // 取得http状态码
            NSInteger code = [httpResponse statusCode];
            if (code == 200) {
                Succed(responseObject);
                
            } else if (code == 400 || code == 401 || code == 403) {
                [SVProgressHUD showInfoWithStatus:@"请求不合法"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 404) {
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 500 || code == 505) {
                [SVProgressHUD showInfoWithStatus:@"服务器异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }else{
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
        failure(error);
    }];
}
+(void)fuckGetHttpManagerType:(NSString *)type Type1:(NSString *)Type1 IDString:(NSInteger)IDString page:(NSInteger)page limit:(NSInteger)limit requestSucced:(Success)Succed requestfailure:(Failure)failure{
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/jpeg",@"image/png", nil];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSString *urlStr = [NSString stringWithFormat:@"http://www.ljsb.top:5000/front/%@?%@=%ld&page=%ld&limit=%ld", type, Type1,IDString,page,limit];
    
    [manager GET:urlStr parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //此处要判断状态  status  判断是否请求成功
        //—->  此处跳转到请求成功的返回。在此处进行结果的判断。网络失败等原因
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            // 取得http状态码
            NSInteger code = [httpResponse statusCode];
            if (code == 200) {
                Succed(responseObject);
                
            } else if (code == 400 || code == 401 || code == 403) {
                [SVProgressHUD showInfoWithStatus:@"请求不合法"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 404) {
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 500 || code == 505) {
                [SVProgressHUD showInfoWithStatus:@"服务器异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }else{
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
        failure(error);
    }];
}
+(void)fuckGeSearchtHttpManagerSearchText:(NSString *)SearchText page:(NSInteger )page limit:(NSInteger )limit requestSucced:(Success)Succed requestfailure:(Failure)failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/jpeg",@"image/png", nil];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSString *urlStr = [NSString stringWithFormat:@"http://www.ljsb.top:5000/front/search?keyword=%@&page=%ld&limit=%ld",SearchText,page,limit];
   NSString *result = [urlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [manager GET:result parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //此处要判断状态  status  判断是否请求成功
        //—->  此处跳转到请求成功的返回。在此处进行结果的判断。网络失败等原因
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            // 取得http状态码
            NSInteger code = [httpResponse statusCode];
            if (code == 200) {
                Succed(responseObject);
                
            } else if (code == 400 || code == 401 || code == 403) {
                [SVProgressHUD showInfoWithStatus:@"请求不合法"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 404) {
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 500 || code == 505) {
                [SVProgressHUD showInfoWithStatus:@"服务器异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }else{
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
        failure(error);
    }];
}
+(void)fuckGeSearchtHttpManagerRequestSucced:(Success)Succed requestfailure:(Failure)failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/jpeg",@"image/png", nil];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSString *urlStr = [NSString stringWithFormat:@"http://www.ljsb.top:5000/front/monthly"];
    
    [manager GET:urlStr parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //此处要判断状态  status  判断是否请求成功
        //—->  此处跳转到请求成功的返回。在此处进行结果的判断。网络失败等原因
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            // 取得http状态码
            NSInteger code = [httpResponse statusCode];
            if (code == 200) {
                Succed(responseObject);
                
            } else if (code == 400 || code == 401 || code == 403) {
                [SVProgressHUD showInfoWithStatus:@"请求不合法"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 404) {
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 500 || code == 505) {
                [SVProgressHUD showInfoWithStatus:@"服务器异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }else{
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
        failure(error);
    }];
}
//根据章节id获取内容
+(void)fuckGeSearchtHttpManagerRequestBookId:(NSString *)BookId chapter:(NSString *)chapter Succed:(Success)Succed requestfailure:(Failure)failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval;
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/jpeg",@"image/png", nil];
    
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSString *urlStr = [NSString stringWithFormat:@"http://www.ljsb.top:5000/front/content?book=%@&chapter=%@",BookId,chapter];
    
    [manager GET:urlStr parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        //此处要判断状态  status  判断是否请求成功
        //—->  此处跳转到请求成功的返回。在此处进行结果的判断。网络失败等原因
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
        if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
            // 取得http状态码
            NSInteger code = [httpResponse statusCode];
            if (code == 200) {
                Succed(responseObject);
                
            } else if (code == 400 || code == 401 || code == 403) {
                [SVProgressHUD showInfoWithStatus:@"请求不合法"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 404) {
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            } else if (code == 500 || code == 505) {
                [SVProgressHUD showInfoWithStatus:@"服务器异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }else{
                [SVProgressHUD showInfoWithStatus:@"未知异常"];
                [SVProgressHUD dismissWithDelay:SVPDELAY];
            }
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error.localizedDescription);
        
        failure(error);
    }];
}
+(void)fuckGeSearchtHttpManagerRequestBookId:(NSString *)BookId num:(NSString *)num Succed:(Success)Succed requestfailure:(Failure)failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
      manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval;
      manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/jpeg",@"image/png", nil];
      
      manager.requestSerializer = [AFHTTPRequestSerializer serializer];
      NSString *urlStr = [NSString stringWithFormat:@"http://www.ljsb.top:5000/front/contentv?book=%@&num=%@",BookId,num];
      
      [manager GET:urlStr parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
          
      } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
          
          //此处要判断状态  status  判断是否请求成功
          //—->  此处跳转到请求成功的返回。在此处进行结果的判断。网络失败等原因
          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
          if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
              // 取得http状态码
              NSInteger code = [httpResponse statusCode];
              if (code == 200) {
                  Succed(responseObject);
                  
              } else if (code == 400 || code == 401 || code == 403) {
                  [SVProgressHUD showInfoWithStatus:@"请求不合法"];
                  [SVProgressHUD dismissWithDelay:SVPDELAY];
              } else if (code == 404) {
                  [SVProgressHUD showInfoWithStatus:@"未知异常"];
                  [SVProgressHUD dismissWithDelay:SVPDELAY];
              } else if (code == 500 || code == 505) {
                  [SVProgressHUD showInfoWithStatus:@"服务器异常"];
                  [SVProgressHUD dismissWithDelay:SVPDELAY];
              }else{
                  [SVProgressHUD showInfoWithStatus:@"未知异常"];
                  [SVProgressHUD dismissWithDelay:SVPDELAY];
              }
          }
          
      } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
          NSLog(@"%@", error.localizedDescription);
          
          failure(error);
      }];
}
+(void)fuckAddBookListHttpManagerRequestURL:(NSString *)Url dic:(NSDictionary*)dic Succed:(Success)Succed requestfailure:(Failure)failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
       manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval;
       manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/jpeg",@"image/png", nil];
       
       manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSMutableString *stringM = [[NSMutableString alloc] init];
    NSArray *keyArr = dic.allKeys;
    for (NSString *key in keyArr) {
        [stringM appendFormat:@"%@", [NSString stringWithFormat:@"%@=%@",key,dic[key]]];
    }
       NSString *urlStr = [NSString stringWithFormat:@"%@%@",Url,stringM];
       
       [manager GET:urlStr parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
           
       } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           
           //此处要判断状态  status  判断是否请求成功
           //—->  此处跳转到请求成功的返回。在此处进行结果的判断。网络失败等原因
           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
           if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
               // 取得http状态码
               NSInteger code = [httpResponse statusCode];
               if (code == 200) {
                   Succed(responseObject);
                   
               } else if (code == 400 || code == 401 || code == 403) {
                   [SVProgressHUD showInfoWithStatus:@"请求不合法"];
                   [SVProgressHUD dismissWithDelay:SVPDELAY];
               } else if (code == 404) {
                   [SVProgressHUD showInfoWithStatus:@"未知异常"];
                   [SVProgressHUD dismissWithDelay:SVPDELAY];
               } else if (code == 500 || code == 505) {
                   [SVProgressHUD showInfoWithStatus:@"服务器异常"];
                   [SVProgressHUD dismissWithDelay:SVPDELAY];
               }else{
                   [SVProgressHUD showInfoWithStatus:@"未知异常"];
                   [SVProgressHUD dismissWithDelay:SVPDELAY];
               }
           }
           
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           NSLog(@"%@", error.localizedDescription);
           
           failure(error);
       }];
}
+(void)fuckCancelBooktHttpManagerRequestDic:(NSDictionary*)dic Succed:(Success)Succed requestfailure:(Failure)failure{
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
       manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval;
       manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/jpeg",@"image/png", nil];
       
       manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    NSMutableString *stringM = [[NSMutableString alloc] init];
    NSArray *keyArr = dic.allKeys;
    for (NSString *key in keyArr) {
        [stringM appendFormat:@"%@", [NSString stringWithFormat:@"%@=%@",key,dic[key]]];
    }
       NSString *urlStr = [NSString stringWithFormat:@"%@%@&%@",@"http://www.ljsb.top:5000/common/uncollectmany?analysis=",[UserInfoManager manager].loginUser.authorization,stringM];
       
       [manager GET:urlStr parameters:nil progress:^(NSProgress * _Nonnull downloadProgress) {
           
       } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
           
           //此处要判断状态  status  判断是否请求成功
           //—->  此处跳转到请求成功的返回。在此处进行结果的判断。网络失败等原因
           NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
           if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
               // 取得http状态码
               NSInteger code = [httpResponse statusCode];
               if (code == 200) {
                   Succed(responseObject);
                   
               } else if (code == 400 || code == 401 || code == 403) {
                   [SVProgressHUD showInfoWithStatus:@"请求不合法"];
                   [SVProgressHUD dismissWithDelay:SVPDELAY];
               } else if (code == 404) {
                   [SVProgressHUD showInfoWithStatus:@"未知异常"];
                   [SVProgressHUD dismissWithDelay:SVPDELAY];
               } else if (code == 500 || code == 505) {
                   [SVProgressHUD showInfoWithStatus:@"服务器异常"];
                   [SVProgressHUD dismissWithDelay:SVPDELAY];
               }else{
                   [SVProgressHUD showInfoWithStatus:@"未知异常"];
                   [SVProgressHUD dismissWithDelay:SVPDELAY];
               }
           }
           
       } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
           NSLog(@"%@", error.localizedDescription);
           
           failure(error);
       }];
}
+(void)fuckHttpManagerRequestUrl:(NSString *)Url dic:(NSDictionary*)dic  Succed:(Success)Succed requestfailure:(Failure)failure{
  //    DLog(@"reuestURL  ------%@ params == %@",url,dic);
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    //    AFJSONRequestSerializer *serializer = ;
    //    [serializer setStringEncoding:NSUTF8StringEncoding];
        
        manager.requestSerializer=[AFHTTPRequestSerializer serializer];
        manager.requestSerializer.timeoutInterval = kDefaultTimeoutInterval;
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/html", @"text/json", @"text/plain", @"text/javascript", @"text/xml", @"image/jpeg",@"image/png", nil];
        
        NSString * headurl = [Url substringWithRange:NSMakeRange(0, 4)];
        
        NSString *urlStr = [NSString stringWithFormat:@"%@", Url];
        
        if ([headurl isEqualToString:@"http"]) {
            urlStr = Url;
        }
        
        [manager POST:urlStr parameters:dic progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            //此处要判断状态  status  判断是否请求成功
            //—->  此处跳转到请求成功的返回。在此处进行结果的判断。网络失败等原因
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse*)task.response;
            if ([httpResponse respondsToSelector:@selector(allHeaderFields)]) {
                // 取得http状态码
                
                NSInteger code = [httpResponse statusCode];
                if (code == 200) {
//                    NSInteger status = [responseObject[@"code"] integerValue];
                    
//                    if (status == 1)
//                    {
                        Succed(responseObject);
//                    }
//                    else if (status == -9)
//                    {
//                        SCLoginViewController *vc = [[SCLoginViewController alloc]init];
//                        [UIApplication sharedApplication].delegate.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:vc];;

//                    }
//                    else
//                    {
//                        NSString *message = responseObject[@"msg"];
//                        failure(nil);
//                        [SVProgressHUD showInfoWithStatus:message];
//                        [SVProgressHUD dismissWithDelay:SVPDELAY];
//                    }
                    
                } else if (code == 400 || code == 401 || code == 403) {
                    NSString *message = responseObject[@"msg"];
                    [SVProgressHUD showInfoWithStatus:message];
                    [SVProgressHUD dismissWithDelay:SVPDELAY];
                } else if (code == 404) {
                    [SVProgressHUD showInfoWithStatus:@"未知异常"];
                    [SVProgressHUD dismissWithDelay:SVPDELAY];
                } else if (code == 500 || code == 505) {
                    [SVProgressHUD showInfoWithStatus:@"服务器异常"];
                    [SVProgressHUD dismissWithDelay:SVPDELAY];
                }
            }
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@", error.localizedDescription);
            
            failure(error);
            
        }];
}
@end
