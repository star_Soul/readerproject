//
//  HttpManager.h
//  ReaderProject
//
//  Created by 李星星 on 2019/9/23.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BaseModel.h"

NS_ASSUME_NONNULL_BEGIN
typedef void(^Success)(id responseObject);
typedef void(^Failure)(NSError *error);
@interface HttpManager : BaseModel
@property(nonatomic , copy) Success requestSuccess; //请求成功
@property(nonatomic , copy) Failure requestFailure; //请求失败

+(void)fuckGetHttpManagerType:(NSString *)type Type1:(NSString *)Type1 IDString:(NSInteger )IDString requestSucced:(Success)Succed requestfailure:(Failure)failure;

+(void)fuckGetHttpManagerType:(NSString *)type Type1:(NSString *)Type1 IDString:(NSInteger )IDString page:(NSInteger )page limit:(NSInteger )limit requestSucced:(Success)Succed requestfailure:(Failure)failure;

+(void)fuckGeSearchtHttpManagerSearchText:(NSString *)SearchText page:(NSInteger )page limit:(NSInteger )limit requestSucced:(Success)Succed requestfailure:(Failure)failure;

+(void)fuckGeSearchtHttpManagerRequestSucced:(Success)Succed requestfailure:(Failure)failure;

//根据章节id获取内容
+(void)fuckGeSearchtHttpManagerRequestBookId:(NSString *)BookId chapter:(NSString *)chapter Succed:(Success)Succed requestfailure:(Failure)failure;

//根据num获取内容
+(void)fuckGeSearchtHttpManagerRequestBookId:(NSString *)BookId num:(NSString *)num Succed:(Success)Succed requestfailure:(Failure)failure;

//用户get接口
+(void)fuckAddBookListHttpManagerRequestURL:(NSString *)Url dic:(NSDictionary*)dic Succed:(Success)Succed requestfailure:(Failure)failure;

//用户get接口
+(void)fuckCancelBooktHttpManagerRequestDic:(NSDictionary*)dic Succed:(Success)Succed requestfailure:(Failure)failure;

//用户接口
+(void)fuckHttpManagerRequestUrl:(NSString *)Url dic:(NSDictionary*)dic  Succed:(Success)Succed requestfailure:(Failure)failure;


@end

NS_ASSUME_NONNULL_END
