//
//  UserInfoManager.m
//  ReaderProject
//
//  Created by 李星星 on 2019/9/29.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "UserInfoManager.h"
#define LOGIN_USER_INFO @"login_user_info"
@implementation SystemConfiguration

@end
@implementation UserRoleEntity

@end

@implementation BankNumber

@end
@implementation PBUserInfo
- (void)objReflectFromDic:(NSDictionary *)jsonDic
{
    [super objReflectFromDic:jsonDic];
   
}

@end
@implementation UserInfoManager
{
    PBUserInfo *userInfo;
    SystemConfiguration *sysConfig;
}
+ (UserInfoManager *)manager
{
    static UserInfoManager *manager;
    static dispatch_once_t pred;
    dispatch_once(&pred, ^{
        manager = [[UserInfoManager alloc]init];
    });
    return manager;
}


- (PBUserInfo *)loginUser {
    if (userInfo != nil) {
        return userInfo;
    }
    NSDictionary *params = [[NSUserDefaults standardUserDefaults] dictionaryForKey:LOGIN_USER_INFO];
    if (params == nil) {
        return nil;
    }
    userInfo = [[PBUserInfo alloc] init];
    [userInfo objReflectFromDic:params];
    return userInfo;
}

- (void)save:(PBUserInfo *)info
{
    if (info)
    {
        NSDictionary *params = [info  dictRepresentation];
        [[NSUserDefaults standardUserDefaults] setValue:params forKey:LOGIN_USER_INFO];
        [[NSUserDefaults standardUserDefaults] synchronize];
        userInfo = info;
        //给老用户模型赋值
        
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:DidUpdateUserInfo object:nil];
    }
}

- (void)clearUserInfo {
    userInfo = nil;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:LOGIN_USER_INFO];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (SystemConfiguration *)sysConfigura {
    if (sysConfig != nil) {
        return sysConfig;
    }
    NSDictionary *params = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"sysConfigura"];
    if (params == nil) {
        return nil;
    }
    sysConfig = [[SystemConfiguration alloc] init];
    [sysConfig objReflectFromDic:params];
    return sysConfig;
}

- (void)saveSys:(SystemConfiguration *)configura
{
    if (configura)
    {
        NSDictionary *params = [configura  dictRepresentation];
        [[NSUserDefaults standardUserDefaults] setValue:params forKey:@"sysConfigura"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        sysConfig = configura;
        //给老用户模型赋值
        
        
        //        [[NSNotificationCenter defaultCenter] postNotificationName:DidUpdateUserInfo object:nil];
    }
}

@end
