//
//  RegisterController.h
//  ReaderProject
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "BaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface RegisterController : BaseViewController
@property(nonatomic,assign)BOOL isChangePwd;
@end

NS_ASSUME_NONNULL_END
