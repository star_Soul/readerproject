//
//  RegisterController.m
//  ReaderProject
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 dingye. All rights reserved.
//

#import "RegisterController.h"
#import <SVProgressHUD.h>
#import "UserAgreePopView.h"
#import <objc/runtime.h>
#import <UMAnalytics/MobClick.h>
@interface RegisterController ()
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;

@property (weak, nonatomic) IBOutlet UIButton *codeButton;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *pwdTextField;
@property (weak, nonatomic) IBOutlet UIButton *registerButton;

@property (weak, nonatomic) IBOutlet UIButton *isAgreeButton;
@property (weak, nonatomic) IBOutlet UIButton *userAgressButton;

@property(nonatomic,strong)NSTimer *timer;
@property(nonatomic,assign)NSInteger count;

@property(nonatomic,strong)UserAgreePopView *userAgreePopView;
@end

@implementation RegisterController
-(UserAgreePopView *)userAgreePopView{
    if (!_userAgreePopView) {
        _userAgreePopView = [[NSBundle mainBundle]loadNibNamed:NSStringFromClass([UserAgreePopView class]) owner:self options:nil].lastObject;
        _userAgreePopView.frame = CGRectMake(30, 100, SCREEN_WIDTH - 30 * 2, 500);
        _userAgreePopView.layer.cornerRadius = 5.0;
        _userAgreePopView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        _userAgreePopView.layer.shadowOffset = CGSizeMake(-5,5);
        _userAgreePopView.layer.shadowOpacity = 1;
        _userAgreePopView.layer.shadowRadius = 5;
        _userAgreePopView.layer.cornerRadius = 5;
        _userAgreePopView.layer.masksToBounds = YES;
        _userAgreePopView.clipsToBounds = NO;
        _userAgreePopView.UserAgreeLabel.text = @"本协议为您与「趣小说 APP」(下称本 APP)版权所有者之间所订立的契约，具有合同的法律效力，请您仔细阅读。\n一、本协议内容、生效、变更。本协议内容包括协议正文及所有本 APP 已经发布的或将来可能发布的各类规则。所有规则为本协议不可分割的组成部分，与协议正文具有同等法律效力。如您对协议有任何疑问，应向本 APP 咨询。只要您使用本 APP 平台服务，则本协议即对您产生约束，届时您不应以未阅读本协议的内容或者未获得本 APP对您问询的解答等理由，主张本协议无效，或要求撤销本协议。您确认：本协议条款是处理双方权利义务的契约，始终有效，法律另有强制性规定或双方另有特别约定的，依其规定。您承诺接受并遵守本协议的约定。如果您不同意本协议的约定，您应立即停止注册程序或停止使用本 APP平台服务。本 APP 有权根据需要不定期地制订、修改本协议及/或各类规则，并在本 APP 平台公示，不再另行单独通知用户。变更后的协议和规则一经在 APP 公布，立即生效。如您不同意相关变更，应当立即停止使用本 APP 平台服务。您继续使用本 APP 平台服务的，即表明您接受修订后的协议和规则。\n二、本 APP 平台所刊载的所有资料及图表仅供参考使用。用户明确同意其使用本 APP 平台网络服务所存在的风险将完全由其自己承担；因其使用本 APP 平台网络服务而产生的一切后果也由其自己承担，本 APP 平台对用户不承担任何责任。\n三、本 APP 平台的用户在参加本 APP 平台举办的各种活动时，我们将在您的同意及确认下，通过注册表格等形式要求您提供一些个人资料，如：您的姓名、性别、年龄、出生日期、身份证号、家庭住址、教育程度、公司情况、所属行业等。我们在未经您同意的情况下，绝对不会将您的任何资料以任何方式泄露给任何第三方。\n四、当政府司法机关依照法定程序要求本 APP 平台披露个人资料时，我们将根据执法单位之要求或为公共安全之目的提供个人资料。在此情况下之任何披露，本 APP 平台均得免责。\n五、 任何由于黑客攻击、计算机病毒侵入或发作、因政府管制而造成的暂时性关闭等影响网络正常经营的不可抗力而造成的个人资料泄露、丢失、被盗用或被窜改等，本 APP 平台均得免责。\n六、由于与本 APP 平台链接的其它网站所造成之个人资料泄露及由此而导致的任何法律争议和后果，本 APP 平台均得免责。\n七、本 APP 平台如因系统维护或升级而需暂停服务时，将事先公告。若因线路及非本网站控制范围外的硬件故障或其它不可抗力而导致暂停服务，于暂停服务期间造成的一切不便与损失，本 APP 平台不负任何责任。\n八、本 APP 平台使用者因为违反本声明的规定而触犯中华人民共和国法律的，一切后果自己负责，本 APP 平台不承担任何责任。\n九、凡以任何方式登陆本 APP 平台或直接、间接使用本 APP 平台资料者，视为自愿接受本 APP 平台声明的约束。\n十、本声明未涉及的问题参见国家有关法律法规，当本声明与国家法律法规冲突时，以国家法律法规为准。\n十一、本 APP 平台不担保网络服务一定能满足用户的要求，也不担保网络服务不会中断，对网络服务的及时性、安全性、准确性也都不作担保。\n十二、本 APP 平台不保证为向用户提供便利而设置的外部链接的准确性和完整性，同时，对于该等外部链接指向的不由本 APP 平台实际控制的任何网页或平台上的内容，本 APP 平台不承担任何责任。\n十三、对于因不可抗力或本 APP 平台不能控制的原因造成的网络服务中断或其它缺陷，本 APP 平台不承担任何责任，但将尽力减少因此而给用户造成的损失和影响。\n十四、本 APP 平台所有页面的版式、图片版权均为本 APP 平台所有，未经授权，不得用于除本 APP 平台之外的任何站点。\n十五、本 APP 平台之声明以及其修改权、更新权及最终解释权均属本 APP 平台所有。";
        [self.view addSubview:_userAgreePopView];
    }
   return  _userAgreePopView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"注册";
    self.registerButton.layer.cornerRadius = 20;
    self.registerButton.clipsToBounds = YES;
    if (self.isChangePwd) {
        self.codeButton.hidden = YES;
        self.isAgreeButton.hidden = self.userAgressButton.hidden = YES;
        self.codeTextField.placeholder = @"请输入密码";
        self.pwdTextField.placeholder = @"请再次输入密码";
        [self.registerButton setTitle:@"确定修改" forState:UIControlStateNormal];
        self.title = @"修改密码";
    }
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick)];
    [self.view addGestureRecognizer:tap];
    self.phoneTextField.clearButtonMode = UITextFieldViewModeAlways;
    [self.userAgressButton addTarget:self action:@selector(popViewShow) forControlEvents:UIControlEventTouchUpInside];
    self.view.backgroundColor = [UIColor whiteColor];
    
    Ivar ivar =  class_getInstanceVariable([UITextField class], "_placeholderLabel");
    UILabel *placeholderLabel = object_getIvar(self.phoneTextField, ivar);
    placeholderLabel.textColor = UIColorFromRGB(0x808080);
    self.phoneTextField.textColor = UIColorFromRGB(0x808080);

    UILabel *pwdplaceholderLabel = object_getIvar(self.pwdTextField, ivar);
    pwdplaceholderLabel.textColor = UIColorFromRGB(0x808080);
    self.pwdTextField.textColor = UIColorFromRGB(0x808080);
    
    UILabel *codeplaceholderLabel = object_getIvar(self.codeTextField, ivar);
    codeplaceholderLabel.textColor = UIColorFromRGB(0x808080);
    self.codeTextField.textColor = UIColorFromRGB(0x808080);
    [MobClick setAutoPageEnabled:YES];
}
-(void)popViewShow{
    [self userAgreePopView];
    [self.userAgreePopView.agreeButton addTarget:self action:@selector(popViewDismiss) forControlEvents:UIControlEventTouchUpInside];
}
-(void)popViewDismiss{
    [_userAgreePopView removeFromSuperview];
    _userAgreePopView = nil;
}
-(void)tapClick{
    [self.view endEditing:YES];
}
- (IBAction)codeButtonClick:(UIButton *)sender {
    if (self.phoneTextField.text.length == 11) {
        [MBProgressHUD showError:@"验证码已发送"];
        [HttpManager fuckHttpManagerRequestUrl:@"http://www.ljsb.top:5000/common/getcode" dic:@{@"phone":self.phoneTextField.text} Succed:^(id  _Nonnull responseObject) {
            self.codeButton.userInteractionEnabled = NO;
            self.count = 0;
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(timerClick) userInfo:nil repeats:YES];
            [[NSRunLoop currentRunLoop] addTimer:self.timer
                                         forMode:NSRunLoopCommonModes];
        } requestfailure:^(NSError * _Nonnull error) {
            
        }];
        
    }else{
        [MBProgressHUD showError:@"请输入正确的手机号"];
        return;
    }
}
- (IBAction)registerButtonClick:(UIButton *)sender {
    if (self.phoneTextField.text.length != 11) {
        [MBProgressHUD showError:@"请输入正确的手机号"];
        return;
    }else if (self.codeTextField.text.length != 6 && !self.isChangePwd){
        [MBProgressHUD showError:@"验证码不正确"];
        return;
    }else if (self.codeTextField.text.length != 6 && self.isChangePwd){
        [MBProgressHUD showError:@"请设置密码长度至少6位"];
        return;
    }else if (self.pwdTextField.text.length < 6){
        [MBProgressHUD showError:@"请设置密码长度至少6位"];
        return;
    }else{
        if (self.isChangePwd) {
            
        }else{
            if (self.userAgressButton.selected) {
                [SVProgressHUD show];
                [SVProgressHUD dismissWithDelay:3.0];
                [HttpManager fuckHttpManagerRequestUrl:@"http://www.ljsb.top:5000/common/register" dic:@{@"phone":self.phoneTextField.text,@"code":self.codeTextField.text,@"password":self.pwdTextField.text,@"gender":@"1"} Succed:^(id  _Nonnull responseObject) {
                    [SVProgressHUD dismiss];
                    PBUserInfo *info = [[PBUserInfo alloc] init];
                    info.phone = self.phoneTextField.text;
                    info.userPassword =  self.pwdTextField.text;
                    info.sex = 1;
                    info.authorization = responseObject[@"result"][@"analysis"];
                    [[UserInfoManager manager] save: info];
                    [MobClick profileSignInWithPUID:info.phone];
                    [MobClick profileSignInWithPUID:@"UserID" provider:@"SJ"];
                    [self.navigationController popToRootViewControllerAnimated:YES];
                } requestfailure:^(NSError * _Nonnull error) {
                    
                }];
            }else{
                [MBProgressHUD showError:@"请先同意用户协议"];
            }
            
        }
    }
}
- (IBAction)isAgressButtonClick:(UIButton *)sender {
    sender.selected = !sender.isSelected;
}
- (IBAction)userAgreeButtonClick:(UIButton *)sender {
    
}
-(void)timerClick{
    self.count ++;
    if (self.count == 60) {
        [self.timer invalidate];
        self.timer = nil;
        self.codeButton.userInteractionEnabled = YES;
        [self.codeButton setTitle:@"发送验证码" forState:UIControlStateNormal];
        return;
    }
    [self.codeButton setTitle:[NSString stringWithFormat:@"%ldS",60 - self.count] forState:UIControlStateNormal];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
