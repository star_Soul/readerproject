//
//  MoreController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "MoreController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "MoreCollectionViewCell.h"
#import "TypeController.h"
#import "ChannelModel.h"
#import <SVProgressHUD.h>
#import <UMAnalytics/MobClick.h>
@interface MoreController ()<UICollectionViewDelegate,UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *listCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *manButton;
@property (weak, nonatomic) IBOutlet UIView *manBottomView;
@property (weak, nonatomic) IBOutlet UIButton *femanButton;
@property (weak, nonatomic) IBOutlet UIView *femanButtonView;


@property(nonatomic,strong)NSMutableArray *dataSourceArrM;
@end

@implementation MoreController
-(NSMutableArray *)dataSourceArrM{
    if (!_dataSourceArrM) {
        _dataSourceArrM = [[NSMutableArray alloc] init];
    }
    return _dataSourceArrM;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.title = @"更多";
    CHTCollectionViewWaterfallLayout *flowLayout = [[CHTCollectionViewWaterfallLayout alloc] init];
    flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 0, 10);
    flowLayout.minimumColumnSpacing = 10;
    flowLayout.minimumInteritemSpacing = 10;
    flowLayout.columnCount = 2;
    //        self.flowLayout = flowLayout;
    [self.listCollectionView setCollectionViewLayout:flowLayout];
    [self.listCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MoreCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([MoreCollectionViewCell class])];
    self.listCollectionView.delegate = self;
    self.listCollectionView.dataSource = self;
    self.listCollectionView.scrollEnabled = YES;
    self.listCollectionView.layer.cornerRadius = 5;
    self.listCollectionView.clipsToBounds = YES;
    
    self.listCollectionView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.listCollectionView.backgroundColor = [UIColor whiteColor];
    self.listCollectionView.showsVerticalScrollIndicator = NO;
    self.listCollectionView.showsHorizontalScrollIndicator = NO;
    //注册Cell
    
    self.listCollectionView.layer.shadowOffset = CGSizeMake(5, 5);
    self.listCollectionView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.listCollectionView.layer.shadowRadius = 5;
    self.listCollectionView.layer.shadowOpacity = 0;
    [self manButtonClick:self.manButton];
    [MobClick setAutoPageEnabled:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
-(void)requestData:(NSInteger )sex{
    [SVProgressHUD show];
    [SVProgressHUD dismissWithDelay:3.0];
    [HttpManager fuckGetHttpManagerType:@"channels" Type1:@"channel" IDString:sex requestSucced:^(id  _Nonnull responseObject) {
        [SVProgressHUD dismiss];
        NSArray *arr = responseObject[@"result"];
        [self.dataSourceArrM removeAllObjects];
        for (NSDictionary *dic in arr) {
            ChannelModel *model = [[ChannelModel alloc] initWithDict:dic];
            model.IDString = dic[@"id"];
            [self.dataSourceArrM addObject:model];
        }
        [self.listCollectionView reloadData];
    } requestfailure:^(NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
    }];
}
- (IBAction)manButtonClick:(UIButton *)sender {
    self.manButton.selected = YES;
    self.femanButton.selected = NO;
    self.manBottomView.hidden = NO;
    self.femanButtonView.hidden = YES;
//    self.dataSourceArrM = [NSMutableArray arrayWithObjects:@"都市",@"玄幻",@"仙侠",@"灵异",@"科幻",@"武侠", nil];
    [self requestData:1];
}
- (IBAction)femanButtonClick:(UIButton *)sender {
    self.femanButton.selected = YES;
    self.manButton.selected = NO;
    self.femanButtonView.hidden = NO;
    self.manBottomView.hidden = YES;
//    self.dataSourceArrM = [NSMutableArray arrayWithObjects:@"现言",@"古言",@"穿越",@"豪门",@"幻言",@"校园", nil];
    [self requestData:0];
}
#pragma mark  设置CollectionView的组数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

#pragma mark  设置CollectionView每组所包含的个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataSourceArrM.count;
    //    return self.secondLevelModelArrM.count;
}

#pragma mark  设置CollectionCell的内容
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    static NSString *identify = @"TRRightCollectionViewCell";
    MoreCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([MoreCollectionViewCell class]) forIndexPath:indexPath];
        ChannelModel *model = self.dataSourceArrM[indexPath.item];
        cell.currentMainModel = model;
//    cell.bookTypeImageView.image = [UIImage imageNamed:self.dataSourceArrM[indexPath.item]];
    
    return cell;
}

#pragma mark  定义每个UICollectionView的大小

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat itemWidth = (SCREEN_WIDTH - 30) / 2.0;
    CGFloat itemHeight = [self getItemHeight:332 imageHeight:200 itemWidth:itemWidth];
    return CGSizeMake(itemWidth,itemHeight);
}
-(CGFloat)getItemHeight:(CGFloat)imageWith imageHeight:(CGFloat)imageHeight itemWidth:(CGFloat)itemWidth{
    CGFloat itemHeight = itemWidth*imageHeight/imageWith;
    return itemHeight;
}

#pragma mark  点击CollectionView触发事件
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    //    self.selectIndex = indexPath.item + 99;

        TypeController *ctrl = [[TypeController alloc] init];
        ctrl.hidesBottomBarWhenPushed = YES;
    ChannelModel *model = self.dataSourceArrM[indexPath.item];
//    if ([model.type isEqualToString:@"都市"]) {
//        ctrl.currentTypeOfFlower = dushi;
//    }else if ([model.type isEqualToString:@"玄幻"]) {
//         ctrl.currentTypeOfFlower = xuanhuan;
//    }else if ([model.type isEqualToString:@"仙侠"]) {
//         ctrl.currentTypeOfFlower = xianxia;
//    }else if ([model.type isEqualToString:@"灵异"]) {
//        ctrl.currentTypeOfFlower = lingyi;
//    }else if ([model.type isEqualToString:@"科幻"]) {
//         ctrl.currentTypeOfFlower = kehuan;
//    }else if ([model.type isEqualToString:@"历史"]) {
//         ctrl.currentTypeOfFlower = wuxia;
//    }else if ([model.type isEqualToString:@"现代言情"]) {
//         ctrl.currentTypeOfFlower = xianyan;
//    }else if ([model.type isEqualToString:@"古代言情"]) {
//         ctrl.currentTypeOfFlower = guyan;
//    }else if ([model.type isEqualToString:@"穿越"]) {
//         ctrl.currentTypeOfFlower = chuanyue;
//    }else if ([model.type isEqualToString:@"豪门"]) {
//         ctrl.currentTypeOfFlower = haomen;
//    }else if ([model.type isEqualToString:@"玄幻言情"]) {
//         ctrl.currentTypeOfFlower = huanyan;
//    }else if ([model.type isEqualToString:@"浪漫青春"]) {
//         ctrl.currentTypeOfFlower = xiaoyuan;
//    }else if ([model.type isEqualToString:@"二次元"]) {
//         ctrl.currentTypeOfFlower = xiaoyuan;
//    }
    ctrl.currentChannelModel = model;
        [self.navigationController pushViewController:ctrl animated:YES];

}

#pragma mark  设置CollectionViewCell是否可以被点击

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForFooterInSection:(NSInteger)section
{
    return CGFLOAT_MIN;
}


@end
