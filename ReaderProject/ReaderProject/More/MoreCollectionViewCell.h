//
//  MoreCollectionViewCell.h
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChannelModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface MoreCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bookTypeImageView;
@property (retain, nonatomic) IBOutlet UILabel *typeLabel;

@property(nonatomic,strong)ChannelModel *currentMainModel;
@end

NS_ASSUME_NONNULL_END
