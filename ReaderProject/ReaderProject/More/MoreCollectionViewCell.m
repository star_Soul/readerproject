//
//  MoreCollectionViewCell.m
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "MoreCollectionViewCell.h"
#import <SDWebImage.h>
@implementation MoreCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setCurrentMainModel:(ChannelModel *)currentMainModel{
    _currentMainModel = currentMainModel;
//    [self.bookTypeImageView setImage:[UIImage imageNamed:currentMainModel.type]];
    [self.bookTypeImageView sd_setImageWithURL:[NSURL URLWithString:currentMainModel.cover]];
    self.typeLabel.text = currentMainModel.type;
}
- (void)dealloc {
    [_typeLabel release];
    [super dealloc];
}
@end
