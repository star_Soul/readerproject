//
//  BaseTabbarController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "BaseTabbarController.h"
#import "BookListController.h"
#import "FindController.h"
#import "MoreController.h"
#import "MineController.h"
@interface BaseTabbarController ()
@property (nonatomic, copy) NSArray *titles;
@property (nonatomic, copy) NSArray *images;
@property (nonatomic, copy) NSArray *selectImages;
@end

@implementation BaseTabbarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if ([[UIDevice currentDevice] systemVersion].floatValue>=7.0) {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]} forState:UIControlStateSelected];
    self.tabBar.translucent = NO;
    self.titles = @[@"书架",@"发现",@"更多",@"我的"];
    self.images = @[@"书架1",@"发现1",@"更多1",@"我的1"];
    self.selectImages = @[@"书架",@"发现",@"更多",@"我的"];
    // 1 添加UITabBarController子控制器
    [self setupAllChildViewController];
    
    // 2 设置tabBar上所有按钮内容
    self.delegate = self;
    [self changeLineOfTabbarColor];
}
- (void)changeLineOfTabbarColor {
    
//    CGRect rect = CGRectMake(0.0f, 0.0f, SCREEN_WIDTH, 0.5);
//    UIGraphicsBeginImageContextWithOptions(rect.size,NO, 0);
//    CGContextRef context =UIGraphicsGetCurrentContext();
//    CGContextSetFillColorWithColor(context, [UIColor blackColor].CGColor);
//    CGContextFillRect(context, rect);
//    UIImage *image =UIGraphicsGetImageFromCurrentImageContext();
//    UIGraphicsEndImageContext();
//    [self.tabBar setShadowImage:image];
//    [self.tabBar setBackgroundImage:[UIImage new]];
}
- (void)changeTabberSelect:(NSNotification *)notify
{
    NSDictionary *info = notify.userInfo;
    NSInteger index = [info[@"index"] integerValue];
    self.selectedIndex = index;
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
#pragma mark - 添加所有的子控制器
- (void)setupAllChildViewController
{
    //    // 首页
    BookListController *homeVc = [[BookListController alloc] init];
    [self addChildViewController:homeVc index:0];
    //分类
    FindController *classVc = [[FindController alloc] init];
    [self addChildViewController:classVc index:1];
    //购物车
    MoreController *shopVc = [[MoreController alloc] init];
    [self addChildViewController:shopVc index:2];
    // 我的
    MineController *mineVc = [[MineController alloc] init];
    [self addChildViewController:mineVc index:3];
    
    
    self.selectedIndex = 0;
    
}

#pragma mark - 设置所有的TabBarButton
- (void)addChildViewController:(UIViewController *)vc index:(NSInteger)index
{
    vc.tabBarItem.title = self.titles[index];
    
    
    UIImage *selectedImage = [UIImage imageNamed:self.selectImages[index]];
    selectedImage = [selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.tabBarItem.selectedImage = selectedImage;
    
    vc.tabBarItem.image = [[UIImage imageNamed:self.images[index]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:DEFAULT_T_ORANGE,NSFontAttributeName:[UIFont systemFontOfSize:10]} forState:UIControlStateSelected];
    [vc.tabBarItem setTitleTextAttributes:@{NSForegroundColorAttributeName:DEFAULT_T_T1,NSFontAttributeName:[UIFont systemFontOfSize:10]} forState:UIControlStateNormal];
    UINavigationController *mineNav = [[UINavigationController alloc]initWithRootViewController:vc];
    vc.tabBarItem.tag  = index;
    [self addChildViewController:mineNav];
    
}
-(void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if (![item.title isEqualToString:@"首页"] && ![item.title isEqualToString:@"分类"]) {
//        if (![NRGGUserInfoManager manager].loginUser.authorization) {
//            [self showPopView];
//        }
    }
}
-(UIViewController *)getCurrentVC {
    
    UIViewController *topRootViewController = [[UIApplication  sharedApplication] keyWindow].rootViewController;
    
    // 在这里加一个这个样式的循环
    while (topRootViewController.presentedViewController)
    {
        // 这里固定写法
        topRootViewController = topRootViewController.presentedViewController;
    }
    
    /*
     *  在此判断返回的视图是不是你的根视图--我的根视图是tabbar
     */
    if ([topRootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *mainTabBarVC = (UITabBarController *)topRootViewController;
        topRootViewController = [mainTabBarVC selectedViewController];
        topRootViewController = [topRootViewController.childViewControllers lastObject];
    }else{
        //导航堆栈
        topRootViewController =  topRootViewController.childViewControllers.lastObject;
    }
    
    return topRootViewController;
    
}
@end
