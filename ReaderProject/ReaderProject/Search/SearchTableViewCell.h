//
//  SearchTableViewCell.h
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClassifyModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface SearchTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *bookImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *authurNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (retain, nonatomic) IBOutlet UIView *bottomView;

@property(nonatomic,strong)ClassifyModel *currentClassifyModel;
@end

NS_ASSUME_NONNULL_END
