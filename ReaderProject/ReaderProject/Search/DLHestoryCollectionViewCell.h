//
//  DLHestoryCollectionViewCell.h
//  XBStore
//
//  Created by 刘松松 on 2018/7/10.
//  Copyright © 2018年 LSS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DLHestoryCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) NSString *isChooseGoodsCount;
@end
