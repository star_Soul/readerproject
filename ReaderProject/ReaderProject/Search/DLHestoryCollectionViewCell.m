//
//  DLHestoryCollectionViewCell.m
//  XBStore
//
//  Created by 刘松松 on 2018/7/10.
//  Copyright © 2018年 LSS. All rights reserved.
//

#import "DLHestoryCollectionViewCell.h"

@implementation DLHestoryCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
-(void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    if ([self.isChooseGoodsCount isEqualToString:@"1"]) {
        if (selected) {
            self.backgroundColor = DEFAULT_T_ORANGE;
            self.titleLabel.textColor = [UIColor whiteColor];
        }else{
            self.backgroundColor = [UIColor clearColor];
            self.titleLabel.textColor = [UIColor blackColor];
        }
    }
}
@end
