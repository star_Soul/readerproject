//
//  SearchTableViewCell.m
//  TextBook
//
//  Created by 李星星 on 2019/9/19.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "SearchTableViewCell.h"
#import <UIImageView+WebCache.h>
@implementation SearchTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
-(void)setCurrentClassifyModel:(ClassifyModel *)currentClassifyModel{
    _currentClassifyModel = currentClassifyModel;
    [self.bookImageView sd_setImageWithURL:[NSURL URLWithString:currentClassifyModel.cover]];
    self.nameLabel.text = currentClassifyModel.name;
    self.infoLabel.text = currentClassifyModel.summary;
    self.authurNameLabel.text = currentClassifyModel.author;
    self.typeLabel.text = currentClassifyModel.label;
}
- (void)dealloc {
    [_bottomView release];
    [super dealloc];
}
@end
