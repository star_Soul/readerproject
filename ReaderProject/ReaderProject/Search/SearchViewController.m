//
//  SearchViewController.m
//  TextBook
//
//  Created by 李星星 on 2019/9/20.
//  Copyright © 2019 李星星. All rights reserved.
//

#import "SearchViewController.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import <Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "SearchTableViewCell.h"
#import "WGCollectionViewLayout.h"
#import "DLHestoryCollectionViewCell.h"
#import "EngineTools.h"
#import "InfoController.h"
#import <MJRefresh.h>
#define HESTORYPATH @"hestroyList"
#import "ClassifyModel.h"
#import <UMAnalytics/MobClick.h>
@interface SearchViewController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,WGCollectionViewLayoutDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UITableView *rightTableView;
@property (nonatomic, strong) NSMutableArray *searchArray;

@property (nonatomic, strong) UICollectionView *hestoryCollectionView;
@property (nonatomic, strong) UIView *coverView;
//@property (nonatomic, copy) NSString *filePath;
@property (nonatomic, strong) NSMutableArray *historyArray;


@property (nonatomic, copy) NSString *searchString;
@property (nonatomic, strong) UISearchBar *searchBar;

@property(nonatomic,assign)NSInteger page;
@property (retain, nonatomic) IBOutlet UIImageView *noneImageView;
@property (retain, nonatomic) IBOutlet UILabel *noneTitleLabel;

//@property(nonatomic,strong)UIButton *deleteButton;
@end

@implementation SearchViewController
-(NSMutableArray *)searchArray{
    if (!_searchArray) {
        _searchArray = [ [NSMutableArray alloc] init];
    }
    return _searchArray;
}
-(UITableView *)rightTableView{
    if (!_rightTableView) {
        _rightTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.tableFooterView = [UIView new];
//        _rightTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
//        _rightTableView.separatorColor = DEFAULT_T_ORANGE;
        [_rightTableView registerNib:[UINib nibWithNibName:NSStringFromClass([SearchTableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([SearchTableViewCell class])];
    }
    return _rightTableView;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any aAJVEitional setup after loading the view.
    
//    NSString *docsdir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
//    self.filePath = [docsdir stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.plist",HESTORYPATH]];
//    NSFileManager *manager = [NSFileManager defaultManager];
//    BOOL exist = [manager fileExistsAtPath:self.filePath];
//    if (exist)
//    {
//        self.historyArray = [NSMutableArray arrayWithContentsOfFile:self.filePath];
//    }
//    else
//    {
//        self.historyArray = [ [NSMutableArray alloc] init];
//    }
    self.noneImageView.hidden =
    self.noneTitleLabel.hidden = YES;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 45)];
    view.backgroundColor = [UIColor clearColor];
    //    self.back.hidden = YES;
    [self.view addSubview:view];
    
    self.searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 35)];
    self.navigationItem.titleView = self.searchBar;
    //    self.searchBar.showsCancelButton = YES;
//    self.searchBar.backgroundImage = [UIImage imageWithColor:DEFAULT_T_ORANGE size:CGSizeMake(SCREEN_WIDTH, 45)];
    self.searchBar.backgroundColor = DEFAULT_T_ORANGE;
    self.searchBar.barTintColor = DEFAULT_T_ORANGE;
    self.searchBar.placeholder = @"点击输入小说名、作者";
    self.searchBar.delegate = self;
    
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightBtn addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    [rightBtn setTitle:@"搜索" forState:UIControlStateNormal];
    rightBtn.titleLabel.font = FONT_MEDIUM(14);
    [rightBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithCustomView:rightBtn];//[[UIBarButtonItem alloc] initWithTitle:@"搜索" style:UIBarButtonItemStylePlain target:self action:@selector(deleteHistoryField)];
    //    [self.view aAJVESubview:self.searchBar];
    UITextField *searchField=[self.searchBar valueForKey:@"searchField"];
    searchField.layer.borderColor = DEFAULT_T_ORANGE.CGColor;
    //    UIImageView * imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cdfvdfb"] highlightedImage:[UIImage imageNamed:@"cdfvdfb"]];
    //    imgView.frame = CGRectMake(0, 0, 40, 40);
    //    imgView.userInteractionEnabled = YES;
    //    searchField.rightView = imgView;
    //    searchField.rightViewMode = UITextFieldViewModeAlways;
    //    searchField.layer.borderWidth = 1;
    searchField.layer.cornerRadius = 35/2.0;
    searchField.layer.masksToBounds = YES;
    searchField.layer.borderColor = DEFAULT_BG_COLORB1.CGColor;
    searchField.layer.borderWidth = 1.0;
    if(@available(iOS 11.0, *)) {
        [[self.searchBar.heightAnchor constraintEqualToConstant:44] setActive:YES];
    }
    
    [self.view addSubview:self.rightTableView];
    [self.rightTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view);
        make.top.equalTo(self.view);
        make.bottom.equalTo(self.view);
        make.right.equalTo(self.view);
    }];
    
    
    self.coverView = [[UIView alloc]init];
    self.coverView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.coverView];
    [self.coverView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(view.mas_bottom);
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    UILabel *title = [[UILabel alloc]init];
    title.textColor = [UIColor blackColor];
    title.font = FONT_MEDIUM(20);
    title.text = @"热门搜索";
    [self.coverView addSubview:title];
    [title mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.coverView).offset(10);
        make.top.equalTo(view.mas_top).offset(20);
        make.height.mas_equalTo(20);
    }];
    
    //    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [self.deleteButton setTitle:@"清除" forState:UIControlStateNormal];
    //    [self.deleteButton setTitleColor:DEFAULT_T_T1 forState:UIControlStateNormal];
    //    self.deleteButton.titleLabel.font = FONTSIZE(14);
    //    [self.deleteButton addTarget:self action:@selector(deleteHistoryField) forControlEvents:UIControlEventTouchUpInside];
    //    [self.deleteButton addTarget:self action:@selector(deleteHistoryField) forControlEvents:UIControlEventTouchUpInside];
    //    [self.coverView addSubview:self.deleteButton];
    //    [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make) {
    //        make.right.equalTo(self.coverView).offset(-20);
    //        make.top.equalTo(view.mas_top).offset(10);
    //        make.height.mas_equalTo(30);
    //        make.width.mas_equalTo(60);
    //    }];
    
    WGCollectionViewLayout *layout = [[WGCollectionViewLayout alloc]init];
    layout.delegate = self;
    
    self.hestoryCollectionView = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];
    layout.itemHeight = 30;
    layout.itemLRSpace = 10;
    layout.maxColumnNum = 5;
    self.hestoryCollectionView.delegate = self;
    self.hestoryCollectionView.dataSource = self;
    [self.hestoryCollectionView registerNib:[UINib nibWithNibName:NSStringFromClass([DLHestoryCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:NSStringFromClass([DLHestoryCollectionViewCell class])];
    [self.hestoryCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    self.hestoryCollectionView.backgroundColor = [UIColor clearColor];
    [self.coverView addSubview:self.hestoryCollectionView];
    [self.hestoryCollectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        //        make.edges.equalTo(self.view);
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(title.mas_bottom).offset(10);
    }];
    self.rightTableView.hidden = YES;
    [self requestHistoryArray];
    [MobClick setAutoPageEnabled:YES];
}

-(void)requestData:(NSString *)searchText{
    [SVProgressHUD show];
    [SVProgressHUD dismissWithDelay:3.0];
    [HttpManager fuckGeSearchtHttpManagerSearchText:searchText page:self.page limit:10 requestSucced:^(id  _Nonnull responseObject) {
        [SVProgressHUD dismiss];
        if (self.page == 1) {
            [self.searchArray removeAllObjects];
        }
        [self.rightTableView.mj_header endRefreshing];
        [self.rightTableView.mj_footer endRefreshing];
        
        NSArray *resultArr = responseObject[@"result"];
        for (NSDictionary *dic in resultArr) {
            ClassifyModel *model = [[ClassifyModel alloc] initWithDict:dic];
            model.IDString = dic[@"id"];
            [self.searchArray addObject:model];
        }
        if (self.searchArray.count == 0) {
            self.rightTableView.hidden = YES;
            self.noneImageView.hidden =
            self.noneTitleLabel.hidden = NO;
        }else{
            self.rightTableView.hidden = NO;
            self.noneImageView.hidden =
            self.noneTitleLabel.hidden = YES;
            [self.rightTableView reloadData];
        }
        
    } requestfailure:^(NSError * _Nonnull error) {
        [SVProgressHUD dismiss];
    }];
    
}
-(void)requestHistoryArray{
    [HttpManager fuckAddBookListHttpManagerRequestURL:@"http://www.ljsb.top:5000/front/topsearch" dic:nil Succed:^(id  _Nonnull responseObject) {
        NSArray *result = responseObject[@"result"];
        self.historyArray = [[NSMutableArray alloc] initWithArray:result];
        [self.hestoryCollectionView reloadData];
    } requestfailure:^(NSError * _Nonnull error) {
        
    }];
}
-(void)deleteHistoryField{
    [self.historyArray removeAllObjects];
//    [self.historyArray writeToFile:self.filePath atomically:YES];
    [self.hestoryCollectionView reloadData];
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}
- (void)searchAction
{
    UITextField *searchField=[self.searchBar valueForKey:@"searchField"];
    [searchField resignFirstResponder];
    [self.view endEditing:YES];
    [self searchBarSearchButtonClicked:self.searchBar];
}
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length == 0)
    {
        self.rightTableView.hidden = YES;
        self.coverView.hidden = NO;
        [self.hestoryCollectionView reloadData];
    }
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.rightTableView.hidden = YES;
    self.coverView.hidden = NO;
    [self.hestoryCollectionView reloadData];
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    UITextField *searchField=[self.searchBar valueForKey:@"searchField"];
    [searchField resignFirstResponder];
    [self.view endEditing:YES];
    if (searchBar.text.length>0)
    {
        BOOL isExist = NO;
        for (int i=0; i<self.historyArray.count; i++)
        {
            NSString *hestory = self.historyArray[i];
            if ([searchBar.text isEqualToString:hestory])
            {
                isExist = YES;
                [self.historyArray exchangeObjectAtIndex:i withObjectAtIndex:0];
                break;
            }
        }
        if (isExist == NO)
        {
            [self.historyArray insertObject:searchBar.text atIndex:0];
        }
        if (self.historyArray.count >10)
        {
            [self.historyArray removeObject:self.historyArray.lastObject];
        }
//        [self.historyArray writeToFile:self.filePath atomically:YES];
        
        self.searchString = searchBar.text;
        self.rightTableView.hidden = NO;
        self.coverView.hidden = YES;
        self.page = 1;
        [self requestData:searchBar.text];
    }
    else
    {
        self.rightTableView.hidden = YES;
        self.coverView.hidden = NO;
        [self.hestoryCollectionView reloadData];
    }
}
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.searchArray.count;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([SearchTableViewCell class])];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    ClassifyModel *model = self.searchArray[indexPath.section];
    cell.currentClassifyModel = model;
    cell.bottomView.hidden = NO;
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 150;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    return [UIView new];
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    InfoController *ctrl = [[InfoController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    ClassifyModel *model = self.searchArray[indexPath.section];
    ctrl.currentClassfiyModel = model;
    [self.navigationController pushViewController:ctrl animated:YES];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.hestoryCollectionView) {
        return self.historyArray.count;
    }
    return self.searchArray.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
        for (UIView *view in header.subviews)
        {
            [view removeFromSuperview];
        }
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, 100, 20)];
        title.textColor = [UIColor blackColor];
        title.font = FONT_MEDIUM(20);
        title.text = @"热门搜索";
        [header addSubview:title];
        return header;
    }
    return nil;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(WGCollectionViewLayout*)collectionViewLayout wideForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat width = [EngineTools widthWithHeight:20 andFont:FONT_MEDIUM(14) string:self.historyArray[indexPath.row]];
    return width +30;
}
#pragma mark - CHTCollectionViewDelegateWaterfallLayout
-(CGSize)collectionView:(UICollectionView *)collectionView
                 layout:(UICollectionViewLayout *)collectionViewLayout
 sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // 用以返回尺寸
    return CGSizeMake((collectionView.width-30)/2.0, 80);
}
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    DLHestoryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([DLHestoryCollectionViewCell class]) forIndexPath:indexPath];
    
    cell.titleLabel.text =self.historyArray[indexPath.row];
    cell.layer.cornerRadius = 15;
    cell.layer.masksToBounds = YES;
    cell.layer.borderWidth = 1;
    cell.layer.borderColor = DEFAULT_T_ORANGE.CGColor;
    cell.backgroundColor = [UIColor whiteColor];
    cell.titleLabel.textColor = DEFAULT_T_ORANGE;
    return cell;
    
    
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    self.searchBar.text = self.historyArray[indexPath.row];
    [self.searchBar becomeFirstResponder];
    
}

- (void)dealloc {
    [_noneImageView release];
    [_noneTitleLabel release];
    [super dealloc];
}
@end
